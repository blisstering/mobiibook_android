/**
 * Validation.java
 *@author Nitin Singh
 */
package com.blisstering.mobiibook.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Helper class containing utility functions for validating fields like email,phone number etc. using regular expressions
 * @author Nitin Singh
 *
 */
public class Validation {

	/**
	 * Regular expression for email id
	 */
	 private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	 
	 /**
	  * Regular expression for phone number
	  */
	 private static final String PHONE_NUMBER_PATTERN="[0-9]*";
	 
	 /**
	  * Regular expression for a string with no digits in it
	  */
	 private static final String NO_DIGITS="\\D*";
	
	 
	 /**
	 * Validate email with regular expression
	 * @param String email
	 * email address
	 * @return true valid email, false invalid email
	 */
	public boolean validateEmail(final String email){
		 Pattern pattern;
		 Matcher matcher;

		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();

	}
	
	 
	 /**
	 * Validate phonenumber with regular expression
	 * @param phonenumber the phonenumber to be validated
	 * @return true valid phonenumber, false invalid phonenumber
	 */
	public boolean containsOnlyDigits(final String phonenumber){
		Pattern pattern;
		Matcher matcher;

		pattern = Pattern.compile(PHONE_NUMBER_PATTERN);
		matcher = pattern.matcher(phonenumber);
		return matcher.matches();
	}
	
	/**
	 * Checks a string if it contains any digits or not
	 * @param string the string to be validated
	 * @return true if string doesnt contains any digits false if string contains any digits
	 */
	public boolean doesNotContainDigits(final String string){
		Pattern pattern;
		Matcher matcher;

		pattern = Pattern.compile(NO_DIGITS);
		matcher = pattern.matcher(string);
		return matcher.matches();
	}
	
}
