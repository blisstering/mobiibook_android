/**
 * SchedulesDataHelper.java
 * @author Nitin Singh
 */
package com.blisstering.mobiibook.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Helper Class for handling all the interactions with the schedules DB
 * @author Nitin Singh
 *
 */
public class SchedulesDataHelper {

	/**
	 * String constant containing the name of the DB of schedules
	 */
	private static final String DATABASE_NAME = "mobiibookschedules.db";
	
	/**
	 * String constant containing the version of the DB
	 */
	private static final int DATABASE_VERSION = 3;
	
	/**
	 * String constant containing the name of the table for schedules
	 */
	private static final String TABLE_NAME = "backups";

	/**
	 * Object reference to the current application context
	 */
	private Context context;

	/**
	 * Object reference for SQLiteDatabase
	 */
	private SQLiteDatabase db;

	/**
	 * Object reference for DB helper 
	 */
	private OpenHelper openHelper;

	//private static final String INSERT = "insert into "	+ TABLE_NAME + "(name) values (?)";

	/**
	 * constructor
	 * @param context current application context
	 */
	public SchedulesDataHelper(Context context) {

		this.context = context;
		openHelper = new OpenHelper(this.context);
		this.db = openHelper.getWritableDatabase();
		//this.insertStmt = this.db.compileStatement(INSERT);
	}

	
	
	/**
	 * closes the db 
	 */
	public void closeDB(){

		openHelper.close();
	}


	
	/**
	 * Function to insert a schedule into the DB
	 * @param schname name of schedule
	 * @param schtime time of schedule
	 * @param schtype type of schedule 
	 * @param schcontent content of schedule
	 * @param schday day of schedule
	 * @param usermailid username of user scheduling the backup
	 */
	public void insert(String schname,String schtime,String schtype,String schcontent,String schday,String usermailid) {

		ContentValues values=new ContentValues();
		values.put("schedule_name",schname);
		values.put("schedule_time",schtime);
		values.put("schedule_type",schtype);
		values.put("schedule_content",schcontent);
		values.put("schedule_day",schday);
		values.put("user_mailid", usermailid);

		db.insert("backups",null,values);

		System.out.println("inserted in db");

		//this.insertStmt.bindString(1, name);
		//return this.insertStmt.executeInsert();
	}

	

	//	public void deleteAll() {
	//
	//		this.db.delete(TABLE_NAME, null, null);
	//	}

	/**
	 * returns everything about the schedule from the DB
	 * @return list of string ,each string containing full details of a schedule
	 */
	public List<String> selectAll() {

		//CRITICAL:::::do not chagne the order of elements in the string array in the read query else changes need to be done in the inflateDetails() method in ResetOnRebootService.java
		List<String> list = new ArrayList<String>();
		Cursor cursor = this.db.query(TABLE_NAME, new String[] { "schedule_id","schedule_name","schedule_time","schedule_type","schedule_content","schedule_day" },null, null, null, null, "schedule_name desc");

		if (cursor.moveToFirst()) {

			do {
				String temp="";


				for (int i=0;i<cursor.getColumnCount();i++){
					temp+=cursor.getString(i)+"\n";
				}

				list.add(temp);
			} while (cursor.moveToNext());
		}

		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}

		return list;
	}
	
	
	/**
	 * Function to query the schedules DB
	 * @param selection string containing the selection clause for the query
	 * @return  list of string ,each string containing full details of a schedule
	 */
	public List<String> query(final String selection) {

		//CRITICAL:::::do not chagne the order of elements in the string array in the read query else changes need to be done in the inflateDetails() method in ResetOnRebootService.java
		List<String> list = new ArrayList<String>();
		Cursor cursor = this.db.query(TABLE_NAME, new String[] { "schedule_id","schedule_name","schedule_time","schedule_type","schedule_content","schedule_day" },"user_mailid="+"'"+selection+"'", null, null, null, "schedule_name desc");

		if (cursor.moveToFirst()) {

			do {
				String temp="";


				for (int i=0;i<cursor.getColumnCount();i++){
					temp+=cursor.getString(i)+"\n";
				}

				list.add(temp);
			} while (cursor.moveToNext());
		}

		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}

		return list;
	}


	/**
	 * function returns the alarm_id of the latest entry in the db
	 * @return the ID of the last schedule in the DB
	 */
	public int lastEntryID(){
//		int count=0,temp=1;

		Cursor cursor = this.db.query(TABLE_NAME, new String[] { "schedule_id"},null, null, null, null,null);

//		if(cursor.moveToFirst()){
//			count=cursor.getCount();
//
//			while(temp<count){
//				temp++;
//				cursor.moveToNext();
//			}
			
		if(cursor.moveToLast()){

			final int lastid=cursor.getInt(0);
			cursor.close();
			return lastid;
		}

		else{
			cursor.close();
			return -1;
		}
	}


	/**
	 * function to delete an enrty from the DB using its id
	 * @param schedule_id id of the schedule to be deleted
	 * @return
	 */
	public boolean deleteEntryUsingID(final int schedule_id){

		int rowsaffected=this.db.delete(TABLE_NAME,("schedule_id="+schedule_id+"").trim(), null);

		if(rowsaffected==0)
			return false;

		else return true;

	}


	/**
	 * Helper class for schedule DB
	 * @author Nitin
	 *
	 */
	private static class OpenHelper extends SQLiteOpenHelper {

		OpenHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}


		/**
		 * Creates the data base table 
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL("CREATE TABLE " + TABLE_NAME + "(schedule_id INTEGER PRIMARY KEY AUTOINCREMENT, schedule_name TEXT,schedule_time TEXT,schedule_type TEXT,schedule_content TEXT,schedule_day TEXT,user_mailid TEXT)");
		}


		/**
		 * upgrades the data base table by dropping the last table and creating a new blank one by calling onCreate()
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			Log.w("Example", "Upgrading database, this will drop tables and recreate.");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
			onCreate(db);
		}
	}
}

