/**
 * BackupContentsThread.java
 * @author Prateek Jain
 */
package com.blisstering.mobiibook.backup;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.text.format.DateUtils;
import android.widget.Toast;

import com.blisstering.mobiibook.activity.WebServiceCall;
import com.blisstering.mobiibook.utils.MyMimeTypeMap;
import com.blisstering.mobiibook.utils.Session;

/**
 * Class responsible for running the backup process in the background 
 * @author Prateek Jain
 */
public class BackupContentsThread{

	/**
	 * The JSONArray of files that needs to be backed up to the server
	 */
	JSONArray jsonFiles = null;

	/**
	 * The number of files that are to backed up to the server
	 */
	int fileNumber = 0;

	/**
	 * Name of the backup
	 */
	String backUpName;

	/**
	 * Type of the backup
	 */
	String backUpType;

	/**
	 * The MIME types of the content to be backed up
	 */
	String contenttype;

	/**
	 * The JSONArray of files returned by backup.start WS, these are the files among the files requested for backup that are not present or presnt in modified form  on the server and actually needs to be backed up 
	 */
	JSONArray jsonReturnedFiles = null;

	/**
	 * The backup ID of the current backup returned backup.start WS
	 */
	String backUpId;

	/**
	 * The response message returned by the WS backup.start,backup.resume or backup.filetransfer
	 */
	String returnMessage = "";

	/**
	 * The number of bytes to be transfer to the server
	 */
	long bytesToTransfer;

	/**
	 * The number of bytes already transferred to the server.
	 */
	long bytesTransferred = 0;

	/**
	 * Flag to be set when the backup is to  be cancelled
	 */
	boolean backupCancel = false;

	/**
	 * Flag to be set when a network failure occurs
	 */
	boolean networkException = false;

	/**
	 * Flag to be set when no new files are there to be tranferred
	 */
	boolean noFileToTransfer = false;

	/**
	 * HttpPost object that acutally transfers the file to the server
	 */
	HttpPost httppost;

	/**
	 * The types of data items that is to be backed up
	 */
	String contentnametobebackedup;

	/**
	 * Flag to be set depending on whether or not the stored session id in Session.java is null nor not , if null the appliacation is deemed as crashed and the user needs to login again to establish a valid session 
	 */
	boolean crashflag=false;


	/**
	 * Object reference to the context of the application 
	 */
	Context mcontext;

	/**
	 * String constant for the master opening tag for mobiicalendar.calendar XML file
	 */
	private static final String MASETR_OPENING_TAG = "<calendar>";

	/**
	 * String constant for the master closing tag for mobiicalendar.calendar XML file
	 */
	private static final String MASETR_CLOSING_TAG = "</calendar>";

	/**
	 * String constant for the opening tag for individual entries in the XML file for calendar backup
	 */
	private static final String ENTRY_OPENING_TAG= "<entry>";

	/**
	 * String constant for the closing tag for individual entries in the XML file for calendar backup
	 */
	private static final String ENTRY_CLOSING_TAG= "</entry>";

	/**
	 * String constant for the XML opening tick i.e. <
	 */
	private static final String OPENING_TICK= "<";

	/**
	 * String constant for the XML closing tick i.e.>
	 */
	private static final String CLOSING_TICK= ">";

	/**
	 * String constant for the XML opening tick for a closing tag i.e. </ 
	 */
	private static final String OPENING_TICK_FOR_CLOSING_TAG= "</";

	/**
	 * String constant for the opening tag for individual entries in the XML file for messages backup
	 */
	private static final String MESSAGES_MASETR_OPENING_TAG = "<messages>";

	/**
	 * String constant for the closing tag for individual entries in the XML file for messages backup
	 */
	private static final String MESSAGES_MASETR_CLOSING_TAG = "</messages>";



	/**
	 * String containing the full path of the temporary file(mobiicalendar.calendar) generated during backup which contains the calendar events and which is sent to the server 
	 */
	private static final String EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiicalendar.calendar";

	/**
	 * String containing the full path of the temporary file(mobiimessages.messages) generated during backup which contains the text messages and which is sent to the server 
	 */
	private static final String MESSAGES_EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiimessages.messages";

	/**
	 * String containing the full path of the temporary file(contacts.vcf) generated during backup which contains the contacts and which is sent to the server 
	 */
	private static final String SYSTEM_APP_LOCATION="/data/app";

	/**
	 * String containing the full path of the temporary folder(mobiitemps) generated during backup which contains applications and which is sent to the server 
	 */
	private static final String TEMP_EXTERNAL_DIR= Environment.getExternalStorageDirectory().toString()+"/mobiitempapps";



	/**
	 * Constructor of the class
	 * @param String name 
	 * Name of the backup
	 * @param String type
	 * Type of Backup (Manual, Scheduled)
	 * @author Prateek
	 */
	public BackupContentsThread(String name,String type) {
		// TODO Auto-generated constructor stub
		backUpName = name;
		backUpType = type;
	}



	/**
	 * Constructor
	 * @param name
	 * name of backup
	 * @param contenttypetobebackedup
	 * MIME content type of backup
	 * @param type
	 * Type of backpu(Manual,Scheduled)
	 * @param contentname
	 * Data items to be backedup
	 * @param context
	 * Context of the application
	 */
	public BackupContentsThread(String name,String contenttypetobebackedup,String type,String contentname, Context context) {
		// TODO Auto-generated constructor stub
		backUpName = name;
		backUpType = type;
		contenttype=contenttypetobebackedup.trim();
		contentnametobebackedup=contentname;

		mcontext=context;

		System.out.println("backupname"+backUpName+"backuptype"+backUpType+"contenttype"+contenttype+"contentname"+contentnametobebackedup);
	}



	/**
	 * Starts the backup procedure
	 * @return String
	 * Returns the resonse message of the backup process
	 * @author Prateek
	 */
	public String startBackup()
	{
		if(contenttype.contains("calendar")||contenttype.contains("all")){
			System.out.println("ready to create the calendar file>>>>>>>>>>>>>>>>>");
			createCalendarXML();

		}

		if(contenttype.contains("applications")||contenttype.contains("all")){
			System.out.println("ready to copy apk files>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			getApplications();
		}

		if(contenttype.contains("textmessages")||contenttype.contains("all")){
			System.out.println("ready to create the messages file>>>>>>>>>>>>>>>>>");
			createMessagesXML();
		}


		boolean filesAvailableToRead = getAllFiles();
		if(filesAvailableToRead)
		{
			System.out.println("Files are ready");
			System.out.println("The jsonFiles " + jsonFiles.toString());
			startBackupWS();
		}
		else
		{	
			//System.out.println("Cannot read files");
			//HardCoding
			returnMessage = "Files do not have read permissions. Try again";
		}

		if(backupCancel)
			backupCancelWS();

		return returnMessage;
	}



	/**
	 * function that reads the applications from the /data/app folder and puts them in sdcard for transfer
	 */
	public void getApplications(){
		System.out.println("inside the prepare applications function");

		PackageManager pm = mcontext.getPackageManager();
		List<PackageInfo> list =pm.getInstalledPackages(0);



		//create the temp directory on sd card if it doesnt exists
		File tempdir=new File(TEMP_EXTERNAL_DIR);

		if(!tempdir.exists())
			tempdir.mkdir();

		for(PackageInfo pi : list) {

			try{
				ApplicationInfo ai=pm.getApplicationInfo(pi.packageName, 0);

				System.out.println(">>>>>>packages is<<<<<<<<"+ai.publicSourceDir);

				if((ai.flags & ApplicationInfo.FLAG_SYSTEM)!=0){
					System.out.println(">>>>>>packages is system package"+pi.packageName);
					//System.out.println(">>>>>>packages is system package"+ai.publicSourceDir);
				}

				else{	

					//File file=new File(SYSTEM_APP_LOCATION+"/"+pi.packageName+".apk");

					File file=new File(ai.publicSourceDir);

					System.out.println(">>>file name is >>"+file.getName());

					FileInputStream fIn =  new FileInputStream(file);
					BufferedInputStream bin = new BufferedInputStream( fIn );

					File desfile=new File(TEMP_EXTERNAL_DIR+"/"+pm.getApplicationLabel(ai)+".apk");
					desfile.createNewFile();

					System.out.println("destination file>>>>>>"+desfile.getName());

					FileOutputStream fOut =  new FileOutputStream(desfile);
					BufferedOutputStream bout = new BufferedOutputStream( fOut );

					System.out.println("source file name is : >>>>>>>>>>>"+file.getName());
					System.out.println("destination is>>>>"+desfile.getAbsolutePath());

					byte[] buffer=new byte[8192];

					//reading 8kb from the file at a time for optimization
					int temp1=bin.read(buffer,0,8192);

					while(temp1!=-1){
						if(temp1!=-1){
							bout.write(buffer, 0, 8192);
							System.out.println("byte read>>"+temp1);
							temp1=bin.read(buffer,0,8192);

						}
					}
					bout.flush();

					bin.close();
					bout.close();

					System.out.println(">>>>Done");


				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}




	}



	/**
	 * function that reads the messages from the phone and writes them to a xml file on SD CARD, mobiimessages.messages
	 */
	public void createMessagesXML(){
		System.out.println("inside the create messages xml file");

		Uri messagesUri=Uri.parse("content://sms");

		try{
			// create a file on the sdcard to export the
			// database contents to

			File myFile = new File(MESSAGES_EXPORT_FILE_NAME );
			myFile.createNewFile();


			FileOutputStream fOut =  new FileOutputStream(myFile);
			BufferedOutputStream bos = new BufferedOutputStream( fOut );

			bos.write(MESSAGES_MASETR_OPENING_TAG.getBytes());

			ContentResolver messagesContentResolver=mcontext.getContentResolver();

			Cursor messageCursor=messagesContentResolver.query(messagesUri, null, null, null, null);

			while(messageCursor.moveToNext()){
				bos.write(ENTRY_OPENING_TAG.getBytes());

				for(int i=0;i<messageCursor.getColumnCount();i++){

					if(messageCursor.getString(i)!=null){
						bos.write(OPENING_TICK.getBytes());
						bos.write(messageCursor.getColumnName(i).getBytes());
						bos.write(CLOSING_TICK.getBytes());

						bos.write(messageCursor.getString(i).getBytes());

						System.out.println( messageCursor.getColumnName(i)+ ">>>>>>>>>>>>"+messageCursor.getString(i));

						//System.out.println("not nulll yeye"+eventCursor.getString(i));

						bos.write(OPENING_TICK_FOR_CLOSING_TAG.getBytes());
						bos.write(messageCursor.getColumnName(i).getBytes());
						bos.write(CLOSING_TICK.getBytes());
					}
				}

				bos.write(ENTRY_CLOSING_TAG.getBytes());

			}

			bos.write(MESSAGES_MASETR_CLOSING_TAG.getBytes());	
			bos.close();
		}

		catch(NullPointerException e){
			e.printStackTrace();
		}


		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		catch (IOException e)
		{
			e.printStackTrace();
		}
	}



	/**
	 * function that reads the calendar entries from the phone and writes them to a xml file on SD CARD, mobiicalendar.calendar
	 */
	public void createCalendarXML(){
		System.out.println("inside  the create calendar function>>>>>>>>>>>>>>>>>");

		Uri calendarUri,instanceUri;
		if (android.os.Build.VERSION.SDK_INT <= 7 )
		{
			//the old way
			calendarUri = Uri.parse("content://calendar/calendars"); 
			instanceUri   = Uri.parse("content://calendar/instances/when");
		}
		else
		{
			//the new way
			calendarUri = Uri.parse("content://com.android.calendar/calendars");
			instanceUri    = Uri.parse("content://com.android.calendar/instances/when");
		} 


		try
		{
			// create a file on the sdcard to export the
			// database contents to

			File myFile = new File( EXPORT_FILE_NAME );
			myFile.createNewFile();


			FileOutputStream fOut =  new FileOutputStream(myFile);
			BufferedOutputStream bos = new BufferedOutputStream( fOut );

			bos.write(MASETR_OPENING_TAG.getBytes());

			//read all the calendar entries and write them to the xml file
			ContentResolver contentResolver = mcontext.getContentResolver();

			Cursor cursor = contentResolver.query(calendarUri,(new String[] { "_id", "displayName", "selected" }), null, null, null);	

			while (cursor.moveToNext()) {

				System.out.println(">>>>>>>>>>cal name"+cursor.getString(1)+">>>>>>>>>.cal id "+cursor.getString(0));

				if(cursor.getString(0).equalsIgnoreCase("1")){

					final String _id = cursor.getString(0);
					//final String displayName = cursor.getString(1);
					//final Boolean selected = !cursor.getString(2).equals("0");


					Uri.Builder builder;

					builder= instanceUri.buildUpon();

					long now = new Date().getTime();

					ContentUris.appendId(builder, now - DateUtils.YEAR_IN_MILLIS);
					ContentUris.appendId(builder, now + DateUtils.YEAR_IN_MILLIS);

					Cursor eventCursor = contentResolver.query(builder.build(),null, "Calendars._id=" + _id,null,null); 

					System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Printing calendar>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> with id "+_id);
					//System.out.println("cursor count is "+eventCursor.getCount());


					if(eventCursor!=null){
						System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>number of enteries in calendar"+eventCursor.getCount());
						while (eventCursor.moveToNext()) {	

							bos.write(ENTRY_OPENING_TAG.getBytes());

							for(int i=0;i<eventCursor.getColumnCount();i++){

								if(eventCursor.getString(i)!=null){

									bos.write(OPENING_TICK.getBytes());
									bos.write(eventCursor.getColumnName(i).getBytes());
									bos.write(CLOSING_TICK.getBytes());

									bos.write(eventCursor.getString(i).getBytes());

									//System.out.println(eventCursor.getColumnName(i)+ ">>>>>>>>>>>>"+eventCursor.getString(i));

									//System.out.println("not nulll yeye"+eventCursor.getString(i));

									bos.write(OPENING_TICK_FOR_CLOSING_TAG.getBytes());
									bos.write(eventCursor.getColumnName(i).getBytes());
									bos.write(CLOSING_TICK.getBytes());
								}

							}

							bos.write(ENTRY_CLOSING_TAG.getBytes());
						}	
					}
				}
			}

			bos.write(MASETR_CLOSING_TAG.getBytes());
			bos.close();
		}


		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		catch (IOException e)
		{
			e.printStackTrace();
		}

		catch(NullPointerException e){
			e.printStackTrace();
		}
	}



	/**
	 * Returns if backup was stopped due to any network exception or not
	 * @return boolean
	 * @author Prateek
	 */
	public boolean isNetworkException()
	{
		return networkException;
	}



	/**
	 * Returns if backup was stopped due to any application crashes or not
	 * @return boolean
	 * @author Nitin
	 */
	public boolean isCrashflagSet()
	{
		return crashflag;
	}



	/**
	 * Checks if the external storage is available or not
	 * Also, generates the list of files to be backedup
	 * @return boolean
	 * @author Prateek
	 */
	public boolean getAllFiles()
	{	
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable=false;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		if(mExternalStorageAvailable)
		{
			jsonFiles = new JSONArray();

			getAllFilesFromDirectory(Environment.getExternalStorageDirectory());

		}
		else
		{
			System.out.println("The Media is not available to read");
		}
		return mExternalStorageAvailable;

	}



	/**
	 * Get all the files from the particular directory
	 * @param File directory
	 * directory from which file list to be generated
	 * @author Prateek
	 */
	public void getAllFilesFromDirectory(File directory)
	{
		String extension="",mimetype="",mimename="";
		MyMimeTypeMap map=MyMimeTypeMap.getSingleton();


		try{

			File file[] = directory.listFiles();  
			System.out.println("The length of files is " + file.length);



			for(int i=0;i<file.length;i++)
			{


				System.out.println("The file paths " + i + "=" + file[i].getAbsolutePath());
				if(file[i].isDirectory())
				{
					System.out.println(file[i].getName() + " is directory");
					getAllFilesFromDirectory(file[i]);
					//revisit
				}
				else
				{
					extension=file[i].getName().substring((file[i].getName().lastIndexOf("."))+1);

					System.out.println("extension is "+extension);

					//this makes sure that ONLY applications from the directory mobiitempapps are backed up 
					if(extension.equalsIgnoreCase("apk")){

						if(file[i].getAbsoluteFile().toString().contains(TEMP_EXTERNAL_DIR))
							mimetype=(map.getMimeTypeFromExtension(extension)); 

						else mimetype=null;

					}

					else 
						mimetype=(map.getMimeTypeFromExtension(extension)); 

					System.out.println("mime type is "+mimetype);

					if(mimetype!=null)
					{
						mimename=mimetype.substring(0,mimetype.indexOf("/"));

						System.out.println("mime name is "+mimename);

						if(contenttype.contains(mimename)||contenttype.contains("all")){
							JSONArray singleFile = new JSONArray();
							singleFile.put(file[i].getName());
							System.out.println("File Name to be transferred " + file[i].getName());
							singleFile.put(file[i].length() + "");
							singleFile.put(file[i].getAbsolutePath());
							singleFile.put(file[i].lastModified() + "");
							jsonFiles.put(singleFile);
							fileNumber++;
						}
					}
				}
				//transfer(file[i].getAbsolutePath());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * backup.start web service is called from here
	 * @author Prateek
	 */
	public void startBackupWS()
	{
		crashflag=false;
		if(!backupCancel)
		{
			try {
				if(Session.getSessionId()!=null){
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.start", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					//HardCoding for Android Client ID
					data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
					data += "&" + URLEncoder.encode("backup_name", "UTF-8")+"="+URLEncoder.encode(backUpName, "UTF-8");
					data += "&" + URLEncoder.encode("backup_type", "UTF-8")+"="+URLEncoder.encode(backUpType, "UTF-8");
					data += "&" + URLEncoder.encode("files", "UTF-8")+"="+URLEncoder.encode(jsonFiles.toString(), "UTF-8");
					data += "&" + URLEncoder.encode("content_categories", "UTF-8")+"="+URLEncoder.encode(contentnametobebackedup, "UTF-8");


					//Thread wsCall = new WebServiceCall(data);
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Starting Thread"+data);
					//wsCall.start();
					System.out.println("Waiting for thread to die");
					//wsCall.join();
					//WebServiceCall castedThread = (WebServiceCall) wsCall;
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							returnMessage = obj.get("#message").toString();
						}
						else if(obj.has("response_code") && obj.getInt("response_code") != 0)
						{
							returnMessage = obj.getString("response_message");
						}
						else
						{
							System.out.println("@startBackupWS : The object value " + obj.toString());
							startFileTransferProcedure(obj);
						}
					} else {
						networkException = true;
						System.out.println("Failure");
					}
				}

				else
					crashflag=true;
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}



	/**
	 * File transfer procedure starts from here, the list of files to be backed up is passed to it as an JSONObject
	 * and then they are transferred.
	 * @param JSONObject obj
	 * The json object of files
	 * @author Prateek
	 */
	public void startFileTransferProcedure(JSONObject obj)
	{
		try{
			noFileToTransfer = false;
			jsonReturnedFiles = obj.getJSONArray("files");
			backUpId = obj.getString("backup_id");
			if (jsonReturnedFiles.length() < 1) {
				System.out.println("No files to transfer");
				returnMessage = obj.getString("response_message");
			} else {
				if(!(bytesToTransfer > 0))
					bytesToTransfer = 0;
				//calculate the number of bytes to be transferred
				for(int i=0;i<jsonReturnedFiles.length();i++)
				{
					bytesToTransfer += jsonReturnedFiles.getJSONArray(i).getInt(1);
				}
				for(int i=0;i<=jsonReturnedFiles.length();i++)
				{
					if(backupCancel || networkException)
						break;
					if(i==jsonReturnedFiles.length())
						noFileToTransfer = true;
					boolean returnValue = backUpFileTransferWS(i);
					if(!returnValue)
						break;
					//	break;
				}
			}	
		}catch(Exception e)
		{
			System.out.println("Error in startFileTransferProcedure");
			e.printStackTrace();
		}
	}



	/**
	 * backup.filetransfer is called from here
	 * @author Prateek
	 */
	private boolean backUpFileTransferWS(int current) {

		System.out.println("Entering backup.filetransfer service for "+current);
		if(!backupCancel)
		{
			try {
				String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.filetransfer", "UTF-8");
				data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
				data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backUpId, "UTF-8");
				if(current == 0)
					data += "&" + URLEncoder.encode("file_previous", "UTF-8")+"="+URLEncoder.encode("", "UTF-8");
				else
					data += "&" + URLEncoder.encode("file_previous", "UTF-8")+"="+URLEncoder.encode(((JSONArray)jsonReturnedFiles.get(current-1)).toString(), "UTF-8");

				if(current == jsonReturnedFiles.length())
					data += "&" + URLEncoder.encode("file_current", "UTF-8")+"="+URLEncoder.encode("", "UTF-8");
				else
					data += "&" + URLEncoder.encode("file_current", "UTF-8")+"="+URLEncoder.encode(((JSONArray)jsonReturnedFiles.get(current)).toString(), "UTF-8");

				//Thread wsCall = new WebServiceCall(data);
				WebServiceCall wsCall = new WebServiceCall(data);
				System.out.println("Starting Thread");
				//wsCall.start();
				System.out.println("Waiting for thread to die");
				//wsCall.join();
				WebServiceCall castedThread = (WebServiceCall) wsCall;
				boolean threadStatus = castedThread.isThreadSucceeded();
				if (threadStatus) {
					System.out.println("Success");
					System.out.println("The response " + castedThread.getExecutionResponse());
					System.out.println("There is no error");
					//JSONObject obj = castedThread.getExecutionResponse().getJSONObject("#data");
					JSONObject obj = castedThread.getExecutionResponse();
					int resp = -1;

					try{
						JSONArray dataResArray = obj.getJSONArray("#data");
						resp=dataResArray.getInt(0);
						returnMessage = dataResArray.getString(1);
						if(resp != 0)
						{
							return false;
						}
					}
					catch (Exception e) {
						return false;
					}
					if(resp == 0 && !noFileToTransfer)
					{
						JSONArray jsonCurrentFile;
						System.out.println("@startBackupWS : The object value " + obj.toString());
						jsonCurrentFile = (JSONArray)jsonReturnedFiles.get(current);
						transfer(jsonCurrentFile.getString(2),jsonCurrentFile.getString(0));
						System.out.println(jsonCurrentFile.getString(0) + " is transferred");
						bytesTransferred += jsonCurrentFile.getInt(1);
					}
				} else {
					networkException = true;
					System.out.println("Failure");
					return false;
				}

			} catch (Exception e) {
				System.out.println("@startBackupFileTransfer : " + e.getMessage());
				e.printStackTrace();
				return false;
			}
		}
		System.out.println("Exiting backup.filetransfer service for "+current);
		return true;
	}



	/**
	 * To cancel the backup
	 * @author Prateek
	 */
	public void backupCancel()
	{
		backupCancel = true;
		if(httppost != null)
			httppost.abort();
	}



	/**
	 * backup.cancel web service is called from here
	 * @author Prateek
	 */
	public void backupCancelWS()
	{
		try {
			if(backUpId == null)
			{
				returnMessage = "Backup Cancelled";
				return;
			}
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.cancel", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backUpId, "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONArray jArr = wsCall.getExecutionResponse().getJSONArray("#data");
				System.out.println("In backup Cancel " + jArr.getString(1));
				returnMessage = jArr.getString(1);
			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * To resume the backup which was aborted due to some network issues
	 * @return String
	 * Returns the response message
	 * @author Prateek
	 */
	public String resumeBackup()
	{
		networkException = false;
		backupCancel = false;
		if(backUpId != null)
			resumeBackupWS();
		else
			startBackup();
		return returnMessage;
	}



	/**
	 * backup.resume web service is called from here
	 * @author Prateek
	 */
	public void resumeBackupWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.resume", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			//HardCoding for Android Client ID
			data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
			data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backUpId, "UTF-8");
			data += "&" + URLEncoder.encode("files", "UTF-8")+"="+URLEncoder.encode(jsonFiles.toString(), "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					returnMessage = obj.get("#message").toString();
				}
				else
				{
					System.out.println("@startBackupWS : The object value " + obj.toString());
					startFileTransferProcedure(obj);
				}
			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * Transferring files method
	 * NOT USED
	 * @author Prateek
	 */
	public void transfer1(String fileAbsolutePath, String fileName)
	{	

		try{
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			//httpclient.getParams().setParameter("sessid", Session.getSessionId());

			HttpPost httppost = new HttpPost("http://bliss-dev.com/mobiibook/backup_transfer_files");


			List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("sessid",Session.getSessionId()));
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			httppost.setEntity(formEntity);
			HttpResponse response1 = httpclient.execute(httppost);
			System.out.println("the response is " + response1.toString());

			System.out.println(response1.getStatusLine());


			//			HttpParams params = new BasicHttpParams();
			//			String sessid = Session.getSessionId();
			//			System.out.println("The session id is " + sessid);
			//			params.setParameter("sessid", sessid);
			//			httppost.
			//			httppost.setParams(params);
			//httppost.getParams().setParameter("sessid", Session.getSessionId());
			//HttpParams params = new BasicHttpParams();
			//			String sessid = Session.getSessionId();
			//			System.out.println("The session id is " + sessid);
			//			params.setParameter("sessid", sessid);
			//			httppost.setParams(params);
			//File file = new File("c:/TRASH/zaba_1.jpg");
			//File file = new File(Environment.getExternalStorageDirectory(), "Khabar Nahi_Dostana.mkv");
			//			File file = new File(fileAbsolutePath);
			//			MultipartEntity mpEntity = new MultipartEntity();
			//			ContentBody cbFile = new FileBody(file);
			//			mpEntity.addPart("uploadedfile", cbFile);
			//			httppost.setEntity(mpEntity);
			//			System.out.println("executing request " + httppost.getRequestLine());
			//			HttpResponse response = httpclient.execute(httppost);
			//			HttpEntity resEntity = response.getEntity();

			//			System.out.println(response.getStatusLine());
			//			if (resEntity != null) {
			//			System.out.println(EntityUtils.toString(resEntity));
			//			}
			//			if (resEntity != null) {
			//			resEntity.consumeContent();
			//			}

			httpclient.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}



		//		try{
		//		HttpClient httpclient = new DefaultHttpClient();
		//		httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		//		//httpclient.getParams().setParameter("sessid", Session.getSessionId());

		//		HttpPost httppost = new HttpPost("http://bliss-dev.com/mobiibook/backup_transfer_files");

		////		List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		////		postParameters.add(new BasicNameValuePair("sessid",Session.getSessionId()));
		////		UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
		//		System.out.println("==== point 1.1 ====");
		////		HttpParams params = new BasicHttpParams();
		//		String sessid = Session.getSessionId();
		////		System.out.println("The session id is " + sessid);
		////		params.setParameter("sessid", sessid);
		//		//httppost.setParams(params);
		//		System.out.println("==== point 1.2 ====");
		//		//httppost.setEntity(formEntity);
		//		//httppost.addHeader("sessid11", Session.getSessionId());
		//		//httppost.getParams().setParameter("sessid", Session.getSessionId());
		//		//File file = new File("c:/TRASH/zaba_1.jpg");
		//		//File file = new File(Environment.getExternalStorageDirectory(), "you.MPG");
		//		File file = new File(fileAbsolutePath);
		//		MultipartEntity mpEntity = new MultipartEntity();
		//		ContentBody cbFile = new FileBody(file);
		//		//mpEntity.addPart("sessid", new StringBody(sessid.toString(), "text/plain",Charset.forName( "UTF-8" )));
		//		mpEntity.addPart("uploadedfile", cbFile);
		//		System.out.println("==== point 2.1 ====");

		//		httppost.setEntity(mpEntity);
		//		System.out.println("==== point 2.2 ====");
		//		System.out.println("executing request " + httppost.getRequestLine());
		//		System.out.println("==== point 2.3 ====");
		//		HttpResponse response = httpclient.execute(httppost);
		//		System.out.println("==== point 2.4 ====");
		//		HttpEntity resEntity = response.getEntity();
		//		System.out.println("==== point 3.0 ====");
		//		System.out.println(response.getStatusLine());
		//		System.out.println("==== point 3.1 ====");
		//		if (resEntity != null) {
		//		System.out.println(EntityUtils.toString(resEntity));

		//		//if (resEntity != null) {
		//		resEntity.consumeContent();
		//		}
		//		System.out.println("==== point 3.2 ====");

		//		httpclient.getConnectionManager().shutdown();
		//		System.out.println("==== point 3.3 ====");
		//		}catch(Exception e)
		//		{
		//		e.printStackTrace();
		//		}
	}



	/**
	 * File transfer happens over here using HttpClient
	 * @param String fileAbsolutePath
	 * Path of the file
	 * @param String fileName
	 * Name of the file
	 * @author Prateek
	 */
	public void transfer(String fileAbsolutePath, String fileName)
	{
		try{
			if(!backupCancel)
			{
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				//httppost = new HttpPost("http://bliss-dev.com/mobiibook/backup_transfer_files?sessid=" + Session.getSessionId() + "&backup_id=" + backUpId);
				httppost = new HttpPost("http://staging.mobiibook.com/backup_transfer_files?sessid=" + Session.getSessionId() + "&backup_id=" + backUpId);
				//httppost = new HttpPost("http://192.168.1.48/mobiibook_dev/backup_transfer_files?sessid=" + Session.getSessionId() + "&backup_id=" + backUpId);

				File file = new File(fileAbsolutePath);
				MultipartEntity mpEntity = new MultipartEntity();
				ContentBody cbFile = new FileBody(file);
				mpEntity.addPart("uploadedfile", cbFile);
				httppost.setEntity(mpEntity);
				if(!backupCancel)
				{
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity resEntity = response.getEntity();
					System.out.println(response.getStatusLine());
					if (resEntity != null) {
						System.out.println(EntityUtils.toString(resEntity));
					}
					if (resEntity != null) {
						resEntity.consumeContent();
					}
					httpclient.getConnectionManager().shutdown();
				}
			}
		}catch(Exception e)
		{
			System.out.println(e.getMessage() + "--" + e.getLocalizedMessage() + "--" ); 
			if(!httppost.isAborted())
				networkException = true;
			e.printStackTrace();
		}
	}


	/**
	 * Transferring files using multipart
	 * NOT USED
	 * @author Prateek
	 */
	public void transfer3(String fileAbsolutePath, String fileName)
	{
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;

		String pathToOurFile = fileAbsolutePath;
		//Hardcoding
		String urlServer = "http://bliss-dev.com/mobiibook/backup_transfer_files";
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		//String boundary =  "*****";
		String boundary = "----------V2ymHFg03ehbqgZCaKO6jy";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;

		try
		{
			FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
			outputStream = new DataOutputStream( connection.getOutputStream() );
			//outputStream.writeBytes(twoHyphens + boundary + lineEnd);

			//			res.append("Content-Disposition: form-data; name=\"").append(key)
			//			.append("\"\r\n").append("\r\n").append(value).append(
			//			"\r\n").append("--").append(boundary)
			//			.append("\r\n");
			//			outputStream.writeBytes("Content-Disposition: form-data; name=\"sessid=\"" + Session.getSessionId() + "\"" + lineEnd);
			//			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			//outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile +"\"" + lineEnd);
			//outputStream.writeBytes(lineEnd);


			StringBuffer res = new StringBuffer("--").append(boundary).append("\r\n");
			String key = "sessid";
			String value = Session.getSessionId();

			res.append("Content-Disposition: form-data; name=\"").append(key)
			.append("\"\r\n").append("\r\n").append(value).append(
			"\r\n").append("--").append(boundary)
			.append("\r\n");
			//			res.append("Content-Disposition: form-data; name=\"").append("file_info")
			//			.append("\"; filename=\"").append(fileName).append("\"\r\n")
			//			.append("Content-Type: ").append("image/jpeg").append("\r\n\r\n");

			res.append("Content-Disposition: form-data; name=\"").append("file_info")
			.append("\"; filename=\"").append(fileName).append("\"\r\n\r\n");
			//.append("Content-Type: ").append("image/jpeg").append("\r\n\r\n");



			outputStream.writeBytes(res.toString());
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0)
			{
				outputStream.write(buffer, 0, bufferSize);
				outputStream.flush();
				buffer = null;
				System.gc();
				buffer = new byte[bufferSize];
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = connection.getResponseCode();
			String serverResponseMessage = connection.getResponseMessage();

			System.out.println("Response code :" + serverResponseCode);
			System.out.println("Response Message : " + serverResponseMessage);

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}



	/**
	 * Returns the value of the progress bar
	 * @return int
	 * Value of the percentage completion of porgress bar
	 * @author Prateek
	 */
	public int getProgess()
	{
		try{
			return (int) ((bytesTransferred * 100)/bytesToTransfer);
		}catch(Exception e)
		{
			return 0;
		}
	}

}
