package com.blisstering.mobiibook.aidl;

interface RestoreService {
  boolean cancelRestore();
  void resumeRestore();
  void sendUpdates();
}
