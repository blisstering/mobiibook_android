/*
 * Used Android Source Code
 */
package com.blisstering.mobiibook.android.pim.vcard.exception;

public class VCardInvalidCommentLineException extends VCardInvalidLineException {
    public VCardInvalidCommentLineException() {
        super();
    }

    public VCardInvalidCommentLineException(final String message) {
        super(message);
    }
}