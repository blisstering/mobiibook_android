/*
 * Used Android Source Code
 */
package com.blisstering.mobiibook.android.pim.vcard.exception;

public class VCardException extends java.lang.Exception {
    /**
     * Constructs a VCardException object
     */
    public VCardException() {
        super();
    }

    /**
     * Constructs a VCardException object
     *
     * @param message the error message
     */
    public VCardException(String message) {
        super(message);
    }

}
