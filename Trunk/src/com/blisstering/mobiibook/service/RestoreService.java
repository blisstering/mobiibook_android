/**
 * RestoreService.java
 * @author Prateek Jain
 *
 */
package com.blisstering.mobiibook.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.text.DateFormat;

import com.blisstering.mobiibook.restore.RestoreContentsThread;
import com.blisstering.mobiibook.utils.Session;

/**
 * Service that runs in the background for Restore
 * @author Prateek Jain
 *
 */
public class RestoreService extends Service{

	/**
	 * Object refernce for the RestoreContentsThread class
	 */
	RestoreContentsThread restoreContentsThread;
	
	/**
	 * The response returned by the RestoreContentsThread
	 */
	String threadResponse = "";
	
	/**
	 * Value from 0-100 showing the progress of the current backup
	 */
	int progressValue;
	
	/**
	 * Flag indicating whether the current restore is over or not
	 */
	boolean isRestoreProcedureOver = false;
	
	/**
	 * Flag set when a network exception occurs
	 */
	boolean networkException = false;
	
	/**
	 * Flag set when the device runs out of memory
	 */
	boolean outOfMemoryException = false;
	
	/**
	 * The date of the restore
	 */
	String date="";
	
	/**
	 * the time of restore
	 */
	String time="";
	
	/**
	 * The content to be restored
	 */
	String contents="";
	
	/**
	 * The content details to be restored
	 */
	String contentDetails="";
	
	/**
	 * The name of the backup being restored
	 */
	String backupName="";
	
	/**
	 * The start time of the backup that is being restored now
	 */
	String start_time="";


	/**
	 * String constant for the broadcast intent of this service
	 */
	public static final String BROADCAST_ACTION="com.blisstering.mobiibook.service.RestoreServiceActivity";
	
	
	private Intent broadcast=new Intent(BROADCAST_ACTION);
	/**
	 * Binds the service
	 * @author Prateek
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		//arg0.getExtras();
		return binder;
	}

	/**
	 * Called when the service is created
	 * @author Prateek
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		//Toast.makeText(this,"Service created", Toast.LENGTH_LONG).show();
	}

	/**
	 * Called when service is destroyed
	 * @author Prateek
	 */
	@Override
	public void onDestroy() {
		Session.setRestoreService(false);
		System.out.println("In destroy");
		if(restoreContentsThread != null)
			restoreContentsThread.restoreCancel();
		super.onDestroy();
		//Toast.makeText(this,"Service destroyed", Toast.LENGTH_LONG).show();
	}


	/**
	 * Called when service is rebinded
	 * @author Prateek
	 */
	@Override
	public void onRebind(Intent intent) {

	}

	/**
	 * Called when service is unbinded
	 * @author Prateek
	 */
	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(this.getClass().getName(), "UNBIND");
		return true;
	}
	//	public void onStart(int startId, Bundle arguments) {
	//	super.onStart( startId, arguments );
	//	Log.d( LOG_TAG, "onStart" );
	//	serviceHandler = new Handler();
	//	serviceHandler.postDelayed( new RunTask(),1000L );
	//	}

	/**
	 * Restore start process starts from here. Called when the service starts
	 * @author Prateek
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		super.onCreate();
		Session.setRestoreService(true);
		String backupId = intent.getStringExtra("backupId");
		date = intent.getStringExtra("date");
		time =intent.getStringExtra("time");
		contents = intent.getStringExtra("contents");
		contentDetails =intent.getStringExtra("contentDetails");
		start_time = intent.getStringExtra("start_time");
		backupName = intent.getStringExtra("backupName");
		//contentname=intent.getStringExtra("contentname");
		startRestore(backupId);
		setProgress();
		return START_STICKY;
	}

	/**
	 * Thread for a restore process is started from here
	 * @param String backupId
	 * Id of the backup
	 * @author Prateek
	 */
	public void startRestore(final String backupId)
	{	
		new Thread() {
			public void run() {
				restoreContentsThread = new RestoreContentsThread(backupId);
				Context context = RestoreService.this;
				threadResponse = restoreContentsThread.startRestore(context);
				networkException  = restoreContentsThread.isNetworkException();
				outOfMemoryException = restoreContentsThread.isOutOfMemoryException();
				Log.d("THREADRESPONSE", threadResponse);
				isRestoreProcedureOver = true;
			}
		}.start();
	}

	/**
	 * Called when the service is started
	 * @author Prateek
	 */
	@Override
	public void onStart(Intent intent, int startId) {
		super.onCreate();
		Session.setRestoreService(true);
		setProgress();
		//		System.out.println("The service is started");
		//		System.out.println("The argsss " + intent.getExtras());
		//super.onStart( startId, arguments );
		//Toast.makeText(this,"Service started", Toast.LENGTH_LONG).show();
	}

	/*
	 * Binder responsible for connecting to any activity and contains the function described in aidl files by which activity 
	 * communicates with the service
	 */
	private final com.blisstering.mobiibook.aidl.RestoreService.Stub binder = 
		new com.blisstering.mobiibook.aidl.RestoreService.Stub() {
		//		public void startBackup(String bName)
		//		{
		////		backupName = bName;
		////		backupContentThread = new BackupContentsThread(backupName,"Manual");
		////		threadResponse = backupContentThread.startBackup();
		////		isBackupProcedureOver = true;
		//		}
		/**
		 * Cancels the restore
		 * @author Prateek
		 */
		public boolean cancelRestore()
		{
			if(restoreContentsThread != null)
			{
				restoreContentsThread.restoreCancel();
				return true;
			}
			else
			{
				return false;
			}
		}

		/**
		 * To initiate the resume process of the restore
		 * @author Prateek
		 */
		public void resumeRestore()
		{
			new Thread() {
				public void run() {
					isRestoreProcedureOver = false;
					networkException = false;
					outOfMemoryException = false;
					setProgress();
					threadResponse = restoreContentsThread.resumeRestore();
					networkException  = restoreContentsThread.isNetworkException();
					outOfMemoryException = restoreContentsThread.isOutOfMemoryException();
					System.out.println("The restore needs to be resumed");
					isRestoreProcedureOver = true;
				}
			}.start();
		}

		/**
		 * If restore process is running starts the send progress thread
		 * @author Prateek
		 */
		public void sendUpdates() throws RemoteException {
			// TODO Auto-generated method stub
			if(isRestoreProcedureOver)
				setProgress();
		}
	};

	/**
	 * Returns the binder
	 * @author Prateek
	 */
	public IBinder getBinder() {
		return binder;
	}

	/**
	 * Starts the thread which broadcasts the information, received by an activity and shown on the screen
	 * @author Prateek
	 */
	public void setProgress()
	{
		new Thread() {
			public void run() {
				// TODO Auto-generated method stub
				progressValue = 0;
				while(true){
					try{
						if(restoreContentsThread != null)
						{
							//extract the date and time details from the start_time string
							System.out.println(">>>backup start time>>>>"+new java.util.Date(Long.parseLong(start_time)*1000));
							//Date dateobj=new java.util.Date(Long.parseLong(start_time)*1000);
							//time=dateobj.getHours()+":"+dateobj.getMinutes()+":"+dateobj.getSeconds();
							
							//date=dateobj.getDate()+" "+dateobj.getMonth()+" "+dateobj.getYear();
							
							Calendar cal=new GregorianCalendar();
							cal.setTimeInMillis(Long.parseLong(start_time)*1000);
							
							time = cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND);
							
							//date = cal.get(Calendar.DATE)+"/"+(cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.YEAR);
							
							
							
							
							progressValue = restoreContentsThread.getProgess();
							broadcast.putExtra("progress", progressValue);
							broadcast.putExtra("response", threadResponse);
							broadcast.putExtra("restoreover", isRestoreProcedureOver);
							broadcast.putExtra("isNetworkException", networkException);
							broadcast.putExtra("isOutOfMemoryException", outOfMemoryException);
							broadcast.putExtra("date", DateFormat.getDateInstance().format(new Date(Long.parseLong(start_time)*1000)));
							broadcast.putExtra("time", time);
							broadcast.putExtra("contents", contents);
							broadcast.putExtra("contentDetails", contentDetails);
							broadcast.putExtra("backupName", backupName);
							sendBroadcast(broadcast);
							if(isRestoreProcedureOver || networkException || outOfMemoryException)
								break;
							Thread.sleep(2000);
						}
					}
					catch(Throwable t){
					}
				}
			}
		}.start();
	}
}
