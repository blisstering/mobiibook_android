/**
 * ResetOnRebootService.java
 * @author Nitin Singh
 */
package com.blisstering.mobiibook.service;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.blisstering.mobiibook.utils.SchedulesDataHelper;

/**
 * Runs a service in background every time the device restarts and sets all the alarms after reading entries from the database for the scheduled backups
 * @author Nitin Singh
 *
 */
public class ResetOnRebootService extends Service {
	
	/**
	 * String constant for the tag for logging events of this file
	 */
	public static final String TAG = "BootBroadcastReceiver";
	
	/**
	 * The list of days of week
	 */
	String days[]={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
	
	/**
	 * Name of backup
	 */
	String backupname="";
	
	/**
	 * MIME type of the content to be backed up
	 */
	String contenttype="";
	
	/**
	 * Data types to be backed up
	 */
	String contentname="";
	
	/**
	 * type of backup
	 */
	String backuptype="";
	
	/**
	 * the id of the schedule
	 */
	int schedule_id=-1;
	
	/**
	 * the hour in 24-hours format for the time of the backup
	 */
	int mHour=-1;
	
	/**
	 * the minutes in 24-hours format for the time of the backup
	 */
	int mMinute=-1;
	
	/**
	 * the day for the backup
	 */
	int mdayofweek=-1;
	
	/**
	 * Object used to set the alarm for a schedule
	 */
	PendingIntent pendingIntent;

	
	/**
	 * Binds the service
	 * @author Prateek
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	
	/**
	 * Called when the service is created
	 * @author Prateek
	 */
	@Override
	public void onCreate() {

		Log.d(TAG, "onCreate");
	}

	
	/**
	 * Called when service is destroyed
	 * @author Prateek
	 */
	@Override
	public void onDestroy() {

		Log.d(TAG, "onDestroy");
	}

	

	/**
	 * starts the backup and in case a backup is arleady running a notification is popped up
	 */
	@Override
	public int onStartCommand(Intent intent, int flags,int startid) {

		Log.d(TAG, "onStart");

//		for (int i=0;i<10;i++)
//			System.out.println("<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		//do something

		SchedulesDataHelper datahelper=new SchedulesDataHelper(this);

		List<String> names = datahelper.selectAll();

		//StringBuilder sb = new StringBuilder();

		System.out.println("count is:"+names.size());
		for (String temp : names){
			//sb.append(temp);
			
			//sb.append("\n");

			//System.out.println(temp);
			
			inflateDetails(temp);
			
		}


		datahelper.closeDB();


		return START_STICKY;
	}
	
	
	
	/**
	 * function that fills up the details into the variables  so that alarm can be set using them
	 * @param details the details about the schedule read from the DB
	 */
	public void inflateDetails(final String details){
		System.out.println("details from db>>>>>>>>>>");
		System.out.println("details"+details);
		
		
		char mdetails[]=details.toCharArray();
		String tempdetailarray[]=new String[6];
		String builder="";
		int count=0;
		mdayofweek=-1;
		contentname="";
		
		for(int i=0;i<mdetails.length;i++){
			if(mdetails[i]!='\n'){
				builder+=mdetails[i];
			}
			
			else{
				tempdetailarray[count++]=builder;
				builder="";
			}
		}
		
		//tempdetailarray[count]=builder;
		
		schedule_id=Integer.parseInt(tempdetailarray[0]);
		backupname=tempdetailarray[1];
		mHour=Integer.parseInt(tempdetailarray[2].substring(0,tempdetailarray[2].indexOf(":")));
		mMinute=Integer.parseInt(tempdetailarray[2].substring(tempdetailarray[2].indexOf(":")+1,tempdetailarray[2].length()));
		backuptype=tempdetailarray[3];
		contenttype=tempdetailarray[4];


		for(int i=0;i<days.length;i++){
			if(days[i].equalsIgnoreCase(tempdetailarray[5]))
				mdayofweek=i+1;
		}
			//cases switch
		if(contenttype.contains("all"))
			contentname+="All";
		if(contenttype.contains("audio"))
			contentname+="Music";
		if(contenttype.contains("video"))
			contentname+="Video";
		if(contenttype.contains("image"))
			contentname+="Pictures";
		if(contenttype.contains("calendar"))
			contentname+="Calendar";
		if(contenttype.contains("contacts"))
			contentname+="Contacts";
		if(contenttype.contains("textmessages"))
			contentname+="Text Messages";
		if(contenttype.contains("applications"))
			contentname+="Applications";
		
		System.out.println("Extracted details>>>>");
		System.out.println(schedule_id+backupname+mHour+mMinute+backuptype+contenttype+"day"+mdayofweek+contentname);
		
		setAlarm(schedule_id);
	}
	
	
	
	/**
	 *  this function reads the top most row's id and uses the other detaisl along with it to create the alarm
	 *  handle conditions for recurring alarms
	 *  @param id the id of the schedule for which alarm is being set
	 */
	
	public void setAlarm(final int id){
		
		System.out.println("setting the alarm for id>>>>>>>>>>>>>>>>>>>>>>>>>>"+id);
		

			Intent myIntent = new Intent(getApplicationContext(),DaemonBackupService.class);
			myIntent.putExtra("backupname", backupname);
			myIntent.putExtra("contenttype", contenttype);
			myIntent.putExtra("contentname", contentname);
			myIntent.putExtra("backuptype", backuptype);

			//myIntent.putExtra("smallstring","I come from another activity...again and yet again"+count);

			pendingIntent = PendingIntent.getService(getApplicationContext(),id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			System.out.println("alarm set with id :>>>>>>>>>>>>>>>>"+(id));
			
			AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
			alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);

			Calendar cal,current_cal;

			int current_hour=-1;

			current_cal=new GregorianCalendar();

			if(current_cal.get(Calendar.AM_PM)==1)
				current_hour=current_cal.get(Calendar.HOUR)+12;

			else 
				current_hour=current_cal.get(Calendar.HOUR);

			//System.out.println("current_hour"+current_hour);

			if(backuptype.equalsIgnoreCase("daily")){

				cal = Calendar.getInstance();

				if((mHour<current_hour)||((mHour==current_hour) &&  (mMinute<current_cal.get(Calendar.MINUTE)))){
					cal.add(Calendar.DAY_OF_YEAR, current_cal.get(Calendar.DAY_OF_YEAR));
					System.out.println("daily set for next day");
				}

				cal.set(Calendar.HOUR_OF_DAY, mHour);
				cal.set(Calendar.MINUTE, mMinute);
				cal.set(Calendar.SECOND,0);
				cal.set(Calendar.MILLISECOND,0);

				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),24*3600*1000,pendingIntent);

				//Toast.makeText(ScheduleBackupActivity.this, "Daily Backup Scheduled", Toast.LENGTH_LONG).show();
				
				//cal.add(Calendar.DAY_OF_YEAR, cur_cal.get(Calendar.DAY_OF_YEAR));
			}

			else if (backuptype.equalsIgnoreCase("weekly")){

				cal = Calendar.getInstance();

				if((mHour<current_hour)||((mHour==current_hour) &&  (mMinute<current_cal.get(Calendar.MINUTE)))){
					cal.add(Calendar.WEEK_OF_YEAR, current_cal.get(Calendar.WEEK_OF_YEAR));
					System.out.println("weekly set for next week");
				}

				cal.set(Calendar.HOUR_OF_DAY, mHour);
				cal.set(Calendar.MINUTE, mMinute);
				cal.set(Calendar.DAY_OF_WEEK,mdayofweek);

				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),7*24*3600*1000,pendingIntent);

				//Toast.makeText(ScheduleBackupActivity.this, "Weekly Backup Scheduled", Toast.LENGTH_LONG).show();
				
				 
			}
		}
	}

