/**
 * HomeScreenActivity.java
 * @author Prateek Jain
 */
package com.blisstering.mobiibook.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.blisstering.mobiibook.service.BackupService;
import com.blisstering.mobiibook.service.RestoreService;
import com.blisstering.mobiibook.utils.Session;

/**
 * Class responsible for handling the Home Screen of the application
 * @author Prateek Jain
 *
 */
public class HomeScreenActivity extends OptionsActivity {
	
	/**
	 * Object for handling the listview shown on the screen
	 */
	private ListView lv1;

	
	
	/** Called when the activity is first created. 
	 * If the backup service is already running fetch do not start the service again
	 * If not then start the backup service
	 * @author Prateek
	 * */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("My Account");
		}
		lv1=(ListView)findViewById(R.id.ListView01);
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		lv1.setCacheColorHint(00000000); // On scrolling the background of the list view doesn't change
		//		getListView().setTextFilterEnabled(true);
		lv1.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {

				// When clicked, show a toast with the TextView text
				//v.setBackgroundResource(R.drawable.listbackground);
				//v.setBackgroundResource(R.drawable.listcolor);
				Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
						Toast.LENGTH_SHORT).show();
				System.out.println("The position is " + position);
				System.out.println("The id is " + id);
				switch(position)
				{
				case 0:
					backupActivity();
					break;
				case 1:
					restoreNowActivity();
					break;
				case 2:
					shareActivity();
					break;
				case 3:
					buySubscription();
				default:
					break;
				}
			}
		});
	}

	
	
	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}

	
	
	/**
	 * List items to be shown on the screen
	 * @author Prateek
	 */
	static final String[] LISTITEM = new String[] {
		"1.Backup",  "2.Retrieve Now", "3.Share" , "4.Buy Subscription"
	};
	
	

	/**
	 * Backup process is initiated.
	 * If backup process is already running redirect to existing backup now screen
	 * else if restore is running backup cannot be started
	 * else start the backup process
	 * @author Prateek
	 */
	public void backupActivity()
	{

		Intent explicitIntent = new Intent(HomeScreenActivity.this,BackupTypeSelectionActivity.class);
		explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(explicitIntent);


		//		if(Session.isBackupServiceRunning())
		//		{
		//			Intent explicitIntent;
		//			explicitIntent = new Intent(HomeScreenActivity.this,BackupServiceActivity.class);
		//			startActivity(explicitIntent);
		//		}
		//		else if(Session.isRestoreServiceRunning())
		//		{
		//			showResponse(getText(R.string.restoreinprogress).toString());
		//		}
		//		else
		//		{
		//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
		//			builder.setMessage("Are you sure you want to start backup?")
		//			.setCancelable(false)
		//			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		//				public void onClick(DialogInterface dialog, int id) {
		//					//MyActivity.this.finish();
		//					Intent explicitIntent;
		//					//explicitIntent = new Intent(HomeScreenActivity.this,StartBackupActivity.class);
		//					explicitIntent = new Intent(HomeScreenActivity.this,SelectiveBackupActivity.class);
		//					//explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		//					startActivity(explicitIntent);
		//				}
		//			})
		//			.setNegativeButton("No", new DialogInterface.OnClickListener() {
		//				public void onClick(DialogInterface dialog, int id) {
		//					dialog.cancel();
		//				}
		//			});
		//			AlertDialog alert = builder.create();
		//			alert.show();
		//			
		//			Intent explicitintent=new Intent(HomeScreenActivity.this,SelectiveBackupActivity.class);
		//			startActivity(explicitintent);
		//			
		//			
		//		}
	}
	
	

	/**
	 * Restore process is initiated.
	 * If restore process is already running redirect to existing restore now screen
	 * else if backup is running restore cannot be started
	 * else start the restore process
	 * @author Prateek
	 */
	public void restoreNowActivity()
	{
		if(Session.isRestoreServiceRunning())
		{
			Intent explicitIntent;
			explicitIntent = new Intent(HomeScreenActivity.this,RestoreServiceActivity.class);
			startActivity(explicitIntent);
		}
		else if(Session.isBackupServiceRunning())
		{
			showResponse(getText(R.string.backupinprogress).toString());
		}
		else
		{
			Intent explicitIntent = new Intent(HomeScreenActivity.this,RestoreActivity.class);
			explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(explicitIntent);
		}
	}

	
	
	/**
	 * Redirect to share activity screen.
	 * @author Prateek
	 */
	public void shareActivity()
	{
		Intent explicitIntent = new Intent(HomeScreenActivity.this,ShareActivity.class);
		startActivity(explicitIntent);
	}
	
	

	/**
	 * Override the Back button pressed event
	 * If backup / restore is running, prompt for the backup/ restore cancellation message\
	 * logout from the application
	 * @author Prateek
	 */
	@Override
	public void onBackPressed()
	{
		Log.d("MobiiBook", "in onbackPressed");
		logout();
		/*String msg;
		if(Session.isBackupServiceRunning())
			msg = getText(R.string.exitmessagewithbackup).toString();
		else if(Session.isRestoreServiceRunning())
			msg = getText(R.string.exitmessagewithrestore).toString();
		else
			msg = getText(R.string.exitmessage).toString();

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msg)
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.finish();
				if(Session.isBackupServiceRunning())
				{
					Intent i = new Intent(HomeScreenActivity.this,BackupService.class);
					stopService( i );
				}
				else if(Session.isRestoreServiceRunning())
				{
					Intent i = new Intent(HomeScreenActivity.this,RestoreService.class);
					stopService( i );	
				}
				setResult(RESULT_OK);
				logoutWS();
				//finish();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();*/
	}

	
	/**
	 * The function called when user selects "Buy Subscription" on Home Screen
	 * It first shows a message notifying that the native web browser will be opened and then launches an intent to open m.mobiibook.com 
	 */
	public void buySubscription()
	{
		AlertDialog alert;

		AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreenActivity.this);
		builder.setMessage(getText(R.string.buysubscriptionmessage))
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://m.mobiibook.com"));
				startActivity(browserIntent);
			}
		});
		alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}
}
