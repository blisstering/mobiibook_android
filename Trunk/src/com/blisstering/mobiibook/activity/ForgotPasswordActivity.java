/**
 * ForgotPasswordActivity.java
 * @author Nitin Singh
 */
package com.blisstering.mobiibook.activity;

import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.blisstering.mobiibook.utils.Session;
import com.blisstering.mobiibook.utils.Validation;

/**
 * Class responsible for handling the Forgot Password Screen and the calls to user.forgotpassword WS
 * @author Nitin Singh
 *
 */
public class ForgotPasswordActivity extends Activity {

	/**
	 * String storing the email id enterd by the user
	 */
	String emailid;
	
	/**
	 * String storing the response returned by the user.forgotpassword WS
	 */
	String forgotpasswordresponse;
	
	/**
	 * Object for handling the Progress Dialog being shown on the Forgot Password Screen
	 */
	ProgressDialog progressDialog;
	
	/**
	 * Response code returned by the user.forgotpassword WS
	 */
	int responsecode;
	
	/**
	 * Flag indicating whether the call to user.forgotpassword was successful or not
	 */
	boolean passwordchangedflag;
	
	
	
	/**
	 * Called when the activity is first created
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.forgotpasswordactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Forgot Password");
		}
	}
	
	

	/**
	 * The get new password button clicked event is handled in this function
	 * @author Nitin
	 * @param View view
	 * view object gives all the layout elements of the screen.
	 *
	 */
	public void onForgotPasswordClick(View view){
		
		EditText email;
		email = (EditText)findViewById(R.id.EditText01);
		emailid=email.getText().toString().trim();

		//if the email entered is valid call forgotpassword web services
		if(validateFields(emailid)){

			systemConnectWS();

		}

		else {
			showResponse(forgotpasswordresponse);	
		}
	}

	

	/**
	 * This function validates the email entered by the user i.e it shouldnt be empty and should be a valid mail id 
	 * @author Nitin
	 * @param String mailid
	 * mailid has the trimmed email-id entered by the user
	 *
	 */
	public boolean validateFields(String mailid){

		if(mailid.length()== 0)
		{
			forgotpasswordresponse= getText(R.string.enteremail).toString();
			return false;
		}
		Validation v = new Validation();
		if(v.validateEmail(mailid))
		{
			return true;
		}
		else
		{
			forgotpasswordresponse = getText(R.string.entervalidemail).toString();
			return false;
		}	
	}



	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			
			progressDialog.dismiss();
			Session.setSessionId(null);
			showResponse(forgotpasswordresponse);

		}	
	};

	

	/**
	 * This function is responsible for displaying a dialog box in case any notification is to be shown to the user
	 * @author Nitin
	 * @param String response 
	 * response is the message that has to be displayed to the user in the dialog box
	 */
	public void showResponse(String response){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setInverseBackgroundForced(true)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.fi
				if(passwordchangedflag==true){
					Intent explicitIntent = new Intent(ForgotPasswordActivity.this,login.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}

	
	
	/**
	 * system.connect web service call
	 * @author Nitin
	 */
	public void systemConnectWS()
	{
		progressDialog = ProgressDialog.show(this, "", "Validating Email-ID...");
		
		
		//		progressDialog = new ProgressDialog(this); 
		//		progressDialog.setTitle(""); 
		//		progressDialog.setMessage("Loading. Please wait..."); s
		//		progressDialog.setIndeterminate(true); 
		//		progressDialog.setProgressStyle(R.style.MobiiBookProgressDialog);
		//		progressDialog.show();
		
		new Thread() {
			public void run() {
				try {
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("system.connect", "UTF-8");
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Calling web service");
					//wsCall.start();
					//System.out.println("Waiting for thread to die");
					//wsCall.join();
					//WebServiceCall castedThread = (WebServiceCall) wsCall;
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							System.out.println("Error in system.connect");
						}
						else
						{
							String sessionId = obj.getString("sessid");
							Session.setSessionId(sessionId);
							if(Session.getSessionId() != null)
							{
								//progressDialog.dismiss();

								forgotpasswordWS(emailid.toString().trim());
							}
							else
							{
								System.out.println("session id not available");
							}
						}
					} else {
						System.out.println("The value is " + getText(R.string.network_message).toString());
						forgotpasswordresponse = getText(R.string.network_message).toString();
						//showResponse((getText(R.string.network_message)).toString());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/** Message handler is being called */
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
		
	}

	

	/**
	 * user.forgotpassword web service call
	 * @author Nitin
	 */
	public void forgotpasswordWS(final String emailId){

		try {
			String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.forgotpassword", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("email_id", "UTF-8")+"="+URLEncoder.encode(emailId, "UTF-8");
			WebServiceCall wsCall = new WebServiceCall(data);
			//System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("calling forgotpassword....");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();

			System.out.println("threadstatus is :"+threadStatus);

			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());

				//JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				JSONArray jArray = wsCall.getExecutionResponse().getJSONArray("#data");

				System.out.println(jArray.toString());

				responsecode=jArray.getInt(0);

				if(responsecode==0)
					passwordchangedflag=true;//to be used in showresponse() to navigate to login screen

				forgotpasswordresponse=jArray.getString(1);

				System.out.println("calling dialog");


				//			if(obj.has("#error"))
				//			{
				//				//System.out.println("Error in restoreFileWS");
				//				obj.get("#message").toString();
				//				//continueRestore = false;
				//			}
				//			else if(!(obj.getInt("response_code") == 0))
				//			{
				//				//continueRestore = false;
				//				System.out.println("RESPONSE : " + obj.getString("response_message"));
				//			}




			}
			else {
				System.out.println("The value is " + getText(R.string.network_message).toString());
				forgotpasswordresponse = getText(R.string.network_message).toString();
				//showResponse((getText(R.string.network_message)).toString());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
