/**
 * ShareActivity.java
 * @author Prateek Jain
 */
package com.blisstering.mobiibook.activity;

import java.io.File;
import java.net.URLEncoder;

import org.json.JSONObject;

import com.blisstering.mobiibook.utils.Session;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Class responsible for handling the screen shown when the user selects Share from the homescreen or the bottom bar
 * @author Prateek Jain
 *
 */
public class ShareActivity extends OptionsActivity{
	
	/**
	 * Object responsible for showing the list of data types on the screen namely "Music" , "Picutures" and "Video Clips"
	 */
	private ListView lv1;
	
	/**
	 * Response from the file.check WS
	 */
	String response="";
	
	/** Called when the activity is first created. 
	 * @author Prateek*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setListAdapter(new ArrayAdapter(this, R.layout.homescreenactivity, WORLDCUP2010));
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Select Content to Share");
		}
		lv1=(ListView)findViewById(R.id.ListView01);
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		lv1.setCacheColorHint(00000000);
		lv1.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {
				// When clicked, show a toast with the TextView text
				Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
						Toast.LENGTH_SHORT).show();
				System.out.println("The position is " + position);
				System.out.println("The id is " + id);
				switch(position)
				{
				case 0:
					/* To open the image gallery */
					openIntent("image/*");
					break;
				case 1:
					/* To open the music gallery */
					openIntent("audio/*");
					break;
				case 2:
					/* To open the video gallery */
					openIntent("video/*");
					break;
				default:
					break;
				}
			}
		});
	}

	
	
	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response)
	{
		
		System.out.println("reached here");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}
	
	
	
	/**
	 * List items to be shown on the screen
	 * @author Prateek
	 */
	static final String[] LISTITEM = new String[] {
		"1.Pictures",  "2.Music", "3.Video Clips" 
	};
	
	

	/**
	 * Opens the intent for selecting image,  video or music file
	 * @param String type
	 * Type of file which is to be selected (image, video or music)
	 * @author Prateek
	 */
	public void openIntent(String type)
	{
		Intent intent = new Intent();
		intent.setType(type);  
		intent.setAction(Intent.ACTION_GET_CONTENT); 
		//intent.setAction(Intent.CATEGORY_DEFAULT);
		//intent.setAction(Intent.ACTION_PICK);
		/* Starts the activity for result and return back after selecting the image. Calls onActivityResult function */
		//startActivityForResult(Intent.createChooser(intent, "Select"),1);
		startActivityForResult(intent,1);
		//startActivityForResult(intent, 1);
		
	}

	
	
	/**
	 * Called after returng from activityForResult
	 * @param int requestCode
	 * Code of the request for the intent
	 * @param int resultCode
	 * Result code of the response
	 * @param Intent data
	 * Data which comes from the activity
	 * @author Prateek
	 */
	@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data) {   

		if (resultCode == RESULT_OK) {  

			if (requestCode == 1) {  

				// currImageURI is the global variable I'm using to hold the content:// URI of the image  
				Uri currImageURI = data.getData(); 

				System.out.println("THE URI IS " + getRealPathFromURI(currImageURI));
				
				//call file.check WS
				fileCheckWS(getRealPathFromURI(currImageURI));
				
				
				//shareWS(getRealPathFromURI(currImageURI));
				Intent explicitIntent = new Intent(ShareActivity.this,FriendListActivity.class);
				explicitIntent.putExtra("path", getRealPathFromURI(currImageURI));
				explicitIntent.putExtra("filecheckresponse", response);
				explicitIntent.putExtra("previouscrashflag",crashflag);
				startActivity(explicitIntent);
			}  
		}  
	}  

	
	/**
	 * WS call to file.check
	 * @param path path of the file chosen for restore
	 */
	public void fileCheckWS(final String path){
		
		crashflag=false;

		File file = new File(path);
		//File file = new File("data/data/com.android.createFileTest/test.txt");
		if (!file.exists()) {
			response="File does not exist";
		}
		else
		{
			try {
				if(Session.getSessionId()!=null){
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("file.check", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					data += "&" + URLEncoder.encode("file_name", "UTF-8")+"="+URLEncoder.encode(file.getName(), "UTF-8");
					String length = Long.toString(file.length());
					data += "&" + URLEncoder.encode("file_size", "UTF-8")+"="+URLEncoder.encode(length, "UTF-8");
					data += "&" + URLEncoder.encode("file_path", "UTF-8")+"="+URLEncoder.encode(file.getAbsolutePath(), "UTF-8");		
					data += "&" + URLEncoder.encode("file_last_modified_time", "UTF-8")+"="+URLEncoder.encode(Long.toString(file.lastModified()), "UTF-8");
					
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Starting Thread");
					System.out.println("Waiting for thread to die");
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							System.out.println("Error in startBackupWS");
						}
						else
						{
						
							int responsecode=obj.getInt("response_code");
							if(responsecode==0)
								response=" ";
							else 
								response=obj.getString("response_message");
							
						}

					} else {
						System.out.println("Failure");
					}
				}

				else{
					crashflag=true;
					response=getText(R.string.oncrashmessage).toString();
				}
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	/**
	 * Returns the real path of the file (sdcard path)
	 * @param Uri contentUri
	 * Unique Uri of the file
	 * @return String
	 * Real path of the file
	 * @author Prateek
	 */
	public String getRealPathFromURI(Uri contentUri) {  

		// can post image  

		String [] proj={MediaStore.Images.Media.DATA};
		Cursor cursor = managedQuery( contentUri,  
				proj, // Which columns to return  
				null,       // WHERE clause; which rows to return (all rows)  
				null,       // WHERE clause selection arguments (none)  
				null); // Order-by clause (ascending by name)  
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);  
		cursor.moveToFirst();  
		return cursor.getString(column_index);  
	}
}
