/**
 * BackupServiceActivity.java
 * @author Prateek Jain/Nitin Singh
 */
package com.blisstering.mobiibook.activity;

import java.io.File;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blisstering.mobiibook.service.BackupService;
import com.blisstering.mobiibook.utils.Session;

/**
 * Class responsible for showing the Backup Progress Screen
 * @author Prateek Jain/Nitin Singh
 *
 */
public class BackupServiceActivity extends OptionsActivity{

	/**
	 * Object that provides an entry point to the BackupService Class 
	 */
	private com.blisstering.mobiibook.aidl.BackupService backupService;

	/**
	 * The tag for the logging of events in this file
	 */
	private static final String LOG_TAG = "BACKUPSERVICECLIENT";

	/**
	 * Object to create a connection to BackupService Class
	 */
	private BackupServiceConnection conn;

	/**
	 * Flag showing whether the backup service has started or not
	 */
	private boolean serviceStarted = false;

	/**
	 * Object for handling the Progress bar being shown on the Backup Progress Screen
	 */
	ProgressBar progressBar;

	/**
	 * Object for handling the Progress Dialog shown when user cancels the ongoing backup
	 */
	ProgressDialog progressDialog;

	/**
	 * The response message returned by the BackupService
	 */
	String response;

	/**
	 * Flag that shows whether a Network error has occurred while a backup is in progress or not
	 */
	boolean isNetworkException = false;

	/**
	 * Object responsible for showing date on the Backup Progress Screen
	 */
	TextView dateTV;

	/**
	 * Object responsible for showing time on the Backup Progress Screen
	 */
	TextView timeTV;

	/**
	 * Object responsible for showing contents on the Backup Progress Screen
	 */
	TextView contentsTV;

	/**
	 * Object responsible for showing content details on the Backup Progress Screen
	 */
	TextView contentDetailsTV;

	/**
	 * String containing the full path of the temporary file(mobiicalendar.calendar) generated during backup which contains the calendar events and which is sent to the server 
	 */
	private static final String EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiicalendar.calendar";

	/**
	 * String containing the full path of the temporary file(mobiimessages.messages) generated during backup which contains the text messages and which is sent to the server 
	 */
	private static final String MESSAGES_EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiimessages.messages";

	/**
	 * String containing the full path of the temporary file(contacts.vcf) generated during backup which contains the contacts and which is sent to the server 
	 */
	private static final String CONTACTS_EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/contacts.vcf";

	/**
	 * String containing the full path of the temporary folder(mobiitemps) generated during backup which contains applications and which is sent to the server 
	 */
	private static final String APPLICATIONS_EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiitempapps";

	/**
	 * Object responsible for showing the Dialog boxes on the Backup Progress Screen
	 */
	AlertDialog alert;


	/** Called when the activity is first created. 
	 * If the backup service is already running fetch do not start the service again
	 * If not then start the backup service
	 * @author Prateek
	 * */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.backupserviceactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Backup");
		}
		serviceStarted = Session.isBackupServiceRunning();
		dateTV = (TextView)findViewById(R.id.TextView02);
		timeTV = (TextView)findViewById(R.id.TextView04);
		contentsTV = (TextView)findViewById(R.id.TextView06);
		contentDetailsTV = (TextView)findViewById(R.id.TextView08);
		dateTV.setText("Updating...");
		timeTV.setText("Updating...");
		contentsTV.setText("Updating...");
		contentDetailsTV.setText("Updating...");
		if(!serviceStarted)
		{
			startService();
			//		invokeService();
		}
		initService();
		//Receiver is registerd with the service here
		registerReceiver(receiver,new IntentFilter(BackupService.BROADCAST_ACTION));
		ImageButton cancel = (ImageButton)findViewById(R.id.Button01);
		cancel.setOnClickListener(cancelListener);
		progressBar = (ProgressBar)findViewById(R.id.ProgressBar01);
		progressBar.setClickable(false);
		progressBar.setProgress(0);
	}



	/**
	 * Override the Back button pressed event
	 * If backup / restore is running, prompt for the backup/ restore cancellation message\
	 * logout from the application
	 * @author Prateek
	 */
	@Override
	public void onBackPressed()
	{
		Intent explicitintent=new Intent(BackupServiceActivity.this,HomeScreenActivity.class);
		explicitintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(explicitintent);
	}



	/** 
	 * On activity destruction unregister the receiver
	 * Unbind / Release the backup service
	 * @author Prateek
	 * */
	protected void onDestroy()
	{
		unregisterReceiver(receiver);
		releaseService();
		super.onDestroy();
	}



	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * Displays the message and dismiss the progressdialog 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try{

				File myFile = new File( EXPORT_FILE_NAME );
				myFile.delete();

				myFile = new File(MESSAGES_EXPORT_FILE_NAME);
				myFile.delete();


				myFile = new File(CONTACTS_EXPORT_FILE_NAME);
				myFile.delete();

				File dir=new File(APPLICATIONS_EXPORT_FILE_NAME);

				//delete all files in the directory
				if (dir.isDirectory()) {
					String[] children = dir.list();
					for (int i=0; i<children.length; i++) {
						(new File(APPLICATIONS_EXPORT_FILE_NAME+"/"+children[i])).delete();			            
					}
				}

				// The directory is now empty so delete it
				if(dir.delete())
					System.out.println("application directory has been deleted succesfully>>>>>");



				System.out.println("crashflag is>>>>>>>>>>>>>>>>>>>>>>"+crashflag);

				if(alert!=null)
					alert.dismiss();

				if(crashflag){

					showResponse(getText(R.string.oncrashmessage).toString(), 0);
				}

				else if(isNetworkException)
					showResponse(getText(R.string.backupresume).toString(), 1);



				else
				{ 

					System.out.println("becoming confused>>>>>>>>>>");
					//					releaseService();
					//					stopService();
					//					
					//					Session.setBackupService(false);
					//					System.out.println("set to false>>>>>>>>>>>>>>>>>>>>>>>"+Session.isBackupServiceRunning());
					//					showNotification("tickertext","title",response);
					//					//showResponse(response, 0);


					releaseService();
					stopService();
					showResponse(response, 0);
				}
				if(progressDialog != null)
					progressDialog.dismiss();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	};



	//	//fucntion that shows a notification when needed
	//	public void showNotification(final String tickertext,final String title,final String text){
	//		
	//		System.out.println("inside notification>>>>>>>>>>>>>>>>>>>>>>>");
	//		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	//		Notification notification = new Notification(R.drawable.mobiibooklogo,tickertext, System.currentTimeMillis());
	//
	//		PendingIntent contentIntent = PendingIntent.getActivity(this,1,null, 0);
	//
	//		notification.setLatestEventInfo(this,title,text,null);
	//		notification.contentIntent=contentIntent;
	//
	//		manager.notify(1, notification);
	//	} 



	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed on the dialog shown on the Backup Progress Screen
	 * @param int value
	 * If value=0 then a normal notification dialog is shown , if value=1 then a network connection error has occurred and a different dialog with the option of retry is shown
	 * @author Prateek
	 */
	public void showResponse(String response,int value)
	{
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		switch(value)
		{
		case 0:
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();

					if(crashflag){
						Intent explicitIntent= new Intent(BackupServiceActivity.this,login.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(explicitIntent);
					}
					else {
						Intent explicitIntent = new Intent(BackupServiceActivity.this,HomeScreenActivity.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(explicitIntent);
					}
				}
			});
			alert = builder.create();
			alert.setTitle("Notification");
			alert.show();
			break;
		case 1:
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					try{
						backupService.resumeBackup();
					}catch(Exception e)
					{
						showResponse(getText(R.string.backupabort).toString(), 0);
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					releaseService();
					stopService();
					dialog.cancel();
					Intent explicitIntent = new Intent(BackupServiceActivity.this,HomeScreenActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			});
			alert = builder.create();
			alert.show();

		}
	}



	/**
	 * On click listener to cancel backup
	 * @author Prateek
	 */
	private OnClickListener cancelListener = new OnClickListener() {
		public void onClick(View v){

			AlertDialog.Builder builder = new AlertDialog.Builder(BackupServiceActivity.this);
			builder.setMessage("Are you sure you want to cancel the backup?")
			.setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					if(conn != null)
					{
						showCancelSplash();
						try {
							if(backupService == null)
							{
								showResponse("No backup in progress",0);
							}
							else if(!backupService.cancelBackup())
							{
								if(progressDialog != null && progressDialog.isShowing())
									progressDialog.dismiss();
								showResponse(getText(R.string.backupabort).toString(),0);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();

		}	        	
	};



	/**
	 * Shows the cancelling backup progress dialog
	 * @author Prateek
	 */
	public void showCancelSplash()
	{
		progressDialog = ProgressDialog.show(this, "", "Cancelling.....");
	}



	/**
	 * Class to create a connetion to the backup service
	 * @author Prateek
	 *
	 */
	class BackupServiceConnection implements ServiceConnection {
		/**
		 * When service is connected this function is called
		 * @author Prateek
		 */
		public void onServiceConnected(ComponentName className, 
				IBinder boundService ) {
			backupService = com.blisstering.mobiibook.aidl.BackupService.Stub.asInterface((IBinder)boundService);
			Log.d( LOG_TAG,"onServiceConnected" );	
			try {
				backupService.sendUpdates();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//			invokeService();
		}

		/**
		 * When service is dis-connected this function is called
		 * @author Prateek
		 */
		public void onServiceDisconnected(ComponentName className) {
			backupService = null;
			//unregisterReceiver(receiver);
			Log.d( LOG_TAG,"onServiceDisconnected" );
			//updateServiceStatus();
		}
	};



	/**
	 * Binds the service with the connection object
	 * @author Prateek
	 */
	private void initService() {
		if( conn == null ) {
			conn = new BackupServiceConnection();
			//			Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
			//			i.putExtra("key", "value");
			Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
			//i.setClassName("com.blisstering.mobiibook.service", "com.blisstering.mobiibook.service.BackupService");
			//boolean result = this.bindService( i, conn,0);
			boolean result = getApplicationContext().bindService( i, conn,0);
			Log.d( LOG_TAG, "BindService:" + result);
			if(conn == null)
				Log.d( LOG_TAG, "conn is null" );
			else
				Log.d( LOG_TAG, "Conn not null" );	

			Log.d( LOG_TAG, "bindService()" );
		} else {
			//			NotificationManager nm = (NotificationManager)
			//			getSystemService( Context.NOTIFICATION_SERVICE);
			//			nm.notifyWithText(123, "Cannot bind - service already bound",
			//			NotificationManager.LENGTH_LONG, null );
		}
	}



	/**
	 * Unbinds the service from the connection object
	 * @author Prateek
	 */
	private void releaseService() {
		try{
			if( conn != null ) {
				getApplicationContext().unbindService( conn );
				conn = null;
				//updateServiceStatus();
				Log.d( LOG_TAG, "unbindService()" );
			} else {
				//				NotificationManager nm = (NotificationManager)
				//				getSystemService( Context.NOTIFICATION_SERVICE);	
				//				nm.notifyWithText(123, "Cannot unbind - service not bound",
				//				NotificationManager.LENGTH_LONG, null );
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}



	/**
	 * Starts the backup service
	 * @author Prateek
	 */
	private void startService() {
		//		if( started ) {
		//		NotificationManager nm = (NotificationManager)
		//		getSystemService( Context.NOTIFICATION_SERVICE);
		//		nm.notifyWithText(123, "Service already started",
		//		NotificationManager.LENGTH_LONG, null );
		//		} else {
		//		exportContact();
		Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
		Bundle bundle = this.getIntent().getExtras();
		final String backupName = bundle.getString("backupName");
		final String contenttypetobebackedup=bundle.getString("contenttypetobebackedup");
		final String contentnametobebackedup=bundle.getString("contentnametobebackedup");
		i.putExtra("backupName", backupName);
		i.putExtra("contenttypetobebackedup", contenttypetobebackedup);
		i.putExtra("contentnametobebackedup",contentnametobebackedup);
		i.putExtra("backuptype", bundle.getString("backuptype"));
		//i.setClassName( "com.blisste;ring.mobiibook.service", "BackupService" );
		//Bundle b = new Bundle();
		//startService( i,b );
		startService(i);
		Log.d( LOG_TAG, "startService()" );
		//		started = true;
		//		updateServiceStatus();
		//}
	}



	/**
	 * Stops the backup service
	 * @author Prateek
	 */
	private void stopService() {
		//		if( !started ) {
		//		NotificationManager nm = (NotificationManager)
		//		getSystemService( Context.NOTIFICATION_SERVICE);
		//		nm.notifyWithText(123, "Service not yet started",
		//		NotificationManager.LENGTH_LONG, null );
		//		} else {
		Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
		//i.setClassName( "aexp.dualservice", 
		//	"aexp.dualservice.DualService" );
		stopService( i );
		Log.d( LOG_TAG, "stopService()" );
		//		started = false;
		//		updateServiceStatus();
		//		}
	}

	//	private void invokeService() {
	////	if( conn == null ) {
	////	NotificationManager nm = (NotificationManager)
	////	getSystemService( Context.NOTIFICATION_SERVICE);
	////	nm.notifyWithText(123, "Cannot invoke - service not bound",
	////	NotificationManager.LENGTH_LONG, null );
	////	} else {
	//	Bundle bundle = this.getIntent().getExtras();
	//	final String backupName = bundle.getString("backupName");
	//	new Thread() {
	//	public void run() {
	//	try {
	//	exportContact();
	//	backupService.startBackup(backupName);
	//	} catch( Exception ex ) {
	//	Log.e( LOG_TAG, "DeadObjectException" );
	//	}
	//	}
	//	}.start();
	//	}
	//	}



	/**
	 * Receiver to get the details broadcasted by Backup Service
	 * @author Prateek
	 */
	private BroadcastReceiver receiver=new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			final int value = bundle.getInt("progress");
			System.out.println("the value is " + value);
			updateProgressBar(value);
			updateDisplay(bundle.getString("date"), bundle.getString("time"), bundle.getString("contents"), bundle.getString("contentDetails"));
			if(bundle.getBoolean("crashflag")){

				crashflag=bundle.getBoolean("crashflag");
				messageHandler.sendEmptyMessage(0);
			}

			else if(bundle.getBoolean("backupover") || bundle.getBoolean("isNetworkException"))
			{	
				response = bundle.getString("response");
				isNetworkException = bundle.getBoolean("isNetworkException");
				messageHandler.sendEmptyMessage(0);
			}
		}

	};



	/**
	 * To update the progress bar on the screen
	 * @author Prateek
	 * @param int value
	 * ranges from (1-100) depending on how much of the data has been backed up
	 */
	public void updateProgressBar(int value)
	{

		progressBar.setProgress(value);
	}



	/**
	 * To update the other details of the backup on the screen
	 * @author Prateek
	 * @param String date
	 * backup date
	 * @param String time
	 * backup time
	 * @param String contents
	 * the type of contents being backed up
	 * @param String contentDetails
	 * the details of the contents being backed up 
	 *
	 */
	public void updateDisplay(String date, String time, String contents, String contentDetails)
	{
		dateTV.setText(date);
		timeTV.setText(time);
		contentsTV.setText(contents);
		contentDetailsTV.setText(contentDetails);
	}

}
