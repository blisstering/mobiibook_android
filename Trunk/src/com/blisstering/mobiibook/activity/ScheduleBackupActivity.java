/**
 * ScheduleBackupActivity.java
 * @author Nitin Singh
 */
package com.blisstering.mobiibook.activity;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.blisstering.mobiibook.service.DaemonBackupService;
import com.blisstering.mobiibook.utils.SchedulesDataHelper;


/**
 * Class responsible for handling the Schedule Backup screen 
 * @author Nitin Singh
 *
 */
public class ScheduleBackupActivity extends OptionsActivity implements AdapterView.OnItemSelectedListener{

	/**
	 * List of types of backups namely , "Daily" and "Weekly"
	 */
	String backuptypes[]={"Daily","Weekly"};

	/**
	 * Name of the backup being taken
	 */
	String backupname="";

	/**
	 * Scheduled time of the backup
	 */
	String backuptime="";

	/**
	 * Scheduled day of the backup 
	 */
	String backupday;

	/**
	 * Type of the scheduled backup
	 */
	String backuptype="";

	/**
	 * Message to be shown to the user on the dialog
	 */
	String response="";

	/**
	 * The MIME type of the content being backed up
	 */
	String contenttype="";

	/**
	 * The content type of the conetnt being backed up
	 */
	String contentname="";

	/**
	 * List of days of the week
	 */
	String days[]={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};


	int mYear,mMonth,mDay,mHour,mMinute,mdayofweek=-1;

	/**
	 * Flag indicating whether the backup was successfully scheduled or not and the alarm for it set 
	 */
	boolean succesfullyscheduledflag=false;

	/**
	 * Username of the currently logged in user
	 */
	private String username="";

	/**
	 * Password of the currently logged in user
	 */
	private String password="";

	/**
	 * Object used to set the alarm for the backup being scheduled
	 */
	private PendingIntent pendingIntent;

	/**
	 * The array storing the state of items(checked/unchecked) from the pop up showing content type to be backedup up
	 */
	boolean selections[];

	/**
	 * The string storing the Preference file name used for storing user credentials
	 */
	public static final String PREFS_NAME = "MobiibookPrefsFile";

	/**
	 * The string used to refer the username in the Preference file used to store user credentials
	 */
	private static final String PREF_USERNAME = "username";

	/**
	 * The string used to refer the password in the Preference file used to store user credentials
	 */
	private static final String PREF_PASSWORD = "password";

	/**
	 * List of MIME types supported by the application for backup 
	 */
	String mimenames[]={"all","calendar","contacts","image","audio","video","textmessages","applications"};

	/**
	 * List of types of data allowed to be backedup by the application
	 */
	String contentlist[]={"All","Calendar","Contacts","Pictures","Music","Video Clips","Text Messages","Applications and Games "};

	/**
	 * Object for handling the time picker shown on the screen
	 */
	AlertDialog timepicker;

	/**
	 * Constant refernce for the time chooser dialog
	 */
	static final int TIME_DIALOG_ID = 0;

	/**
	 * Object to handle the choose time button
	 */
	Button timebutton;

	Button datebutton;

	static final int DATE_DIALOG_ID = 1;

	static final int CONTENT_DIALOG_ID = 2;

	/**
	 * Object to handle the spinner for selecting type of backup
	 */
	Spinner backuptypespinner;

	/**
	 * Object to handle the spinner for selecting day of the week
	 */
	Spinner backupdayspinner;


	/**
	 * Called when the activity is first created
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		selections=new boolean[contentlist.length];

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.schedulebackupactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Add Schedule");
		}

		timebutton=(Button)findViewById(R.id.Button01);
		//		datebutton=(Button)findViewById(R.id.Button02);

		//code for backuptype spinner
		backuptypespinner=(Spinner)findViewById(R.id.spinner1);

		backuptypespinner.setOnItemSelectedListener(this);
		ArrayAdapter<String> backuptypeadapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,backuptypes);

		backuptypeadapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
		backuptypespinner.setAdapter(backuptypeadapter);


		//code for backupday spinner
		backupdayspinner=(Spinner)findViewById(R.id.spinner2);

		backupdayspinner.setOnItemSelectedListener(this);
		ArrayAdapter<String> dayadapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,days);

		dayadapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item);
		backupdayspinner.setAdapter(dayadapter);


		//		//code to set the datepicker to current date
		//		
		//		final Calendar c = Calendar.getInstance();
		//		mYear = c.get(Calendar.YEAR);
		//		mMonth = c.get(Calendar.MONTH);
		//		mDay = c.get(Calendar.DAY_OF_MONTH);
		//
		//		// display the current date
		//		updateDateDisplay();


		//code to check whether a new schedule is being created or old one is being edited....pass the intent id which is by default 0 and if intent passes it its non zero...check for 
		//the id and if its non zero call editschedule() which gets values from db and enters them here in the fields ....after editing is done normal flow starts from here
	}


	/**
	 * Function handling the events when the Choose Time button is selected
	 * @param view
	 * view object gives all the layout elements of the screen. 
	 */
	public void onScheduleTimeClick(View view){
		//code for timepicker
		showDialog(TIME_DIALOG_ID);
	}


	//	public void onScheduleDateClick(View view){
	//		//code for datepicker
	//		showDialog(DATE_DIALOG_ID);
	//	}


	/**
	 * Function to create different types of dialogs namely , Time chooser , Content choooser
	 * @param id
	 * The constant id for the type of dialog to be created
	 * @return
	 * Object for the dialog created as per id supplied to the function
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		//			case DATE_DIALOG_ID:
		//				return new DatePickerDialog(this,
		//							mDateSetListener,
		//							mYear, mMonth, mDay);

		case TIME_DIALOG_ID:
			return new TimePickerDialog(this,
					mTimeSetListener, mHour, mMinute, false);

		case CONTENT_DIALOG_ID:

			timepicker=new AlertDialog.Builder( this )
			.setTitle( "Content types" )
			.setMultiChoiceItems( contentlist, selections, new DialogSelectionClickHandler() )
			.setPositiveButton( "OK", new DialogButtonClickHandler() )
			.create();

			return timepicker;
		}
		return null;
	}



	/**
	 * Class responsible for handling the events when content types is being selected
	 * @author Nitin
	 *
	 */
	public class DialogSelectionClickHandler implements DialogInterface.OnMultiChoiceClickListener
	{

		/**
		 * Function called when a content type is checked/unchecked on the list
		 * @param dialog 
		 * Object for the dialog being shown on screen
		 * @param clicked
		 * the position of the item being clicked on the content type list
		 * @param selected
		 * indicating the state of the clicked item(checked/unchecked)
		 * 
		 */
		public void onClick( DialogInterface dialog, int clicked, boolean selected )
		{
			System.out.println("id:"+clicked+"selected:"+selected);
			if(clicked == 0 && selections[0]==true)
			{
				for(int i =1;i<mimenames.length;i++)
				{
					timepicker.getListView().setItemChecked(i,true);
					selections[i]=true;
				}
			}
			else if(clicked == 0 && selections[0]==false)
			{
				for(int i =1;i<mimenames.length;i++)
				{
					timepicker.getListView().setItemChecked(i,false);
					selections[i]=false;
				}
			}
			if(clicked !=0)
			{
				timepicker.getListView().setItemChecked(0, false);
				selections[0]=false;
			}
		}
	}



	/**
	 * Class responsible for handling the event generated by the button shown on the pop up for content types to be backed up
	 * @author Nitin Singh
	 *
	 */
	public class DialogButtonClickHandler implements DialogInterface.OnClickListener
	{
		/**
		 * Function called when the button on the content type pop up is clicked
		 * @param dialog
		 * Object for the dialog being shown on the screen
		 * @param clicked
		 * indicating the state of the button
		 */
		public void onClick( DialogInterface dialog, int clicked )
		{
			contenttype="";
			switch( clicked )
			{
			case DialogInterface.BUTTON_POSITIVE:
				dialog.dismiss();
				if(selections[0]){
					contenttype="all";
					contentname=contentlist[0]+",";
				}
				else{
					for(int i=1;i<selections.length;i++){
						if(selections[i]){
							contenttype+=mimenames[i]+",";
							contentname+=contentlist[i]+",";
						}
					}
				}

				break;
			}
		}
	}
	//	// updates the date we display in the TextView
	//	private void updateDateDisplay() {
	//		datebutton.setText(
	//				new StringBuilder()
	//				// Month is 0 based so add 1
	//				.append(mMonth + 1).append("-")
	//				.append(mDay).append("-")
	//				.append(mYear).append(" "));
	//	}
	//	
	//	// the callback received when the user "sets" the date in the dialog
	//	private DatePickerDialog.OnDateSetListener mDateSetListener =
	//		new DatePickerDialog.OnDateSetListener() {
	//			public void onDateSet(DatePicker view, int year, 
	//					int monthOfYear, int dayOfMonth) {
	//					mYear = year;
	//					mMonth = monthOfYear;
	//					mDay = dayOfMonth;
	//					updateDateDisplay();
	//			}
	//		};
	//		

	//code for the time picker to work
	// updates the time we display in the TextView
	/**
	 * Function to update the variables storing the time details every time the values are changed by the user in the GUI
	 */
	private void updateTimeDisplay() {
		String temp=new StringBuilder()
		.append(pad(mHour)).append(":")
		.append(pad(mMinute)).toString();

		backuptime=temp;
		timebutton.setText(backuptime.toString());
		System.out.println("times is "+backuptime);
	}



	/**
	 * the callback received when the user "sets" the time in the dialog
	 */
	private TimePickerDialog.OnTimeSetListener mTimeSetListener =
		new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHour = hourOfDay;
			mMinute = minute;
			updateTimeDisplay();
		}
	};



	/**
	 * Function to do padding of the string formulated as a result of the user choosing time on the GUI
	 * @param c
	 * the string containing the formatted time chosen by the user on the GUI
	 * @return
	 */
	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}


	//code to get values from the spinners
	/**
	 * The get the value from country and gender when the spinners for them are selected by the user.
	 * @author Nitin
	 * @param AdapterView
	 * parent object specifies the view object that triggered the event
	 * @param View
	 * The view within the AdapterView that was clicked
	 * @param position
	 * The position of the view in the adapter
	 * @param id
	 *The row id of the item that is selected
	 */
	public void onItemSelected(AdapterView parent,View v, int position, long id) {

		if(parent==backuptypespinner){
			backuptype=backuptypes[position];
			if(backuptype.equals("Daily")){
				backupdayspinner.setClickable(false);
				backupdayspinner.getBackground().setColorFilter(new LightingColorFilter(0, 0xff444444));
			}


			if(backuptype.equals("Weekly")){
				backupdayspinner.setClickable(true);
				backupdayspinner.getBackground().clearColorFilter();


			}

			System.out.println("backuptype"+backuptype);
		}	

		if(parent==backupdayspinner)
		{
			backupday=days[position]; 

			mdayofweek=position+1;

			System.out.println("backupday"+backupday);
		}	


	}

	/**
	 * This method is triggered in case the spinner is not selected by the user
	 * @author Nitin
	 * @param AdapterView
	 * parent object specifies the view object that triggered the event
	 *  
	 */
	public void onNothingSelected(AdapterView parent) {

	}



	/**
	 * Function responsible for handling events when the Proceed button is clicked on the Schedule backup screen
	 * @param view
	 * view object gives all the layout elements of the screen.
	 */
	public void onScheduleNextClick(View view){

		succesfullyscheduledflag=false;

		EditText temp;
		temp = (EditText)findViewById(R.id.EditText01);
		backupname=temp.getText().toString().trim();

		if(validateFields()){
			System.out.println(backuptype+backupname+backuptime+backupday+contenttype);

			//call checkpreferences() to check the user name and passwd and if not present ask for user to enter 
			if(checkPreferenceCredentials()){
				if(backuptype.equals("Daily")){
					insertIntoDB(backupname,backuptime,backuptype,contenttype,"");
					mdayofweek=-1;
				}

				else{
					insertIntoDB(backupname,backuptime,backuptype,contenttype,backupday);
				}

			}

			else{

				final AlertDialog.Builder alert = new AlertDialog.Builder(this);
				LayoutInflater factory = LayoutInflater.from(this);
				final View textEntryView = factory.inflate(R.layout.preferencecredentials, null);

				final EditText usernamebox = (EditText) textEntryView.findViewById(R.id.EditText01);
				final EditText passwordbox = (EditText) textEntryView.findViewById(R.id.EditText02);

				String storedusername=getSharedPreferences(PREFS_NAME,MODE_PRIVATE).getString(PREF_USERNAME, null);

				if(storedusername!=null)
					usernamebox.setText(storedusername.toCharArray(),0,storedusername.length());

				alert.setView(textEntryView);
				alert.setTitle("Credentials needed for Schedule backup");
				//alert.setView(passwordbox);
				alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						String musername = usernamebox.getText().toString().trim();
						String mpassword = passwordbox.getText().toString().trim();

						username=musername;
						password=mpassword;

						//the user name that the user used to login must be same as the one which is being entred while scheduling a backup
						if(username.equals(getSharedPreferences(PREFS_NAME,MODE_PRIVATE).getString(PREF_USERNAME, null))){

							getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
							.edit()
							.putString(PREF_USERNAME, username)
							.putString(PREF_PASSWORD, password)
							.commit();
						}

						else{

							AlertDialog.Builder builder = new AlertDialog.Builder(ScheduleBackupActivity.this);
							builder.setMessage("The email id should be same as the one using which you hav logged into the application. Please re-enter the email id.")
							.setCancelable(false)
							.setInverseBackgroundForced(true)
							.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {

								}
							});
							AlertDialog alert = builder.create();
							alert.setTitle("Notification");
							alert.show();

						}

						//System.out.println("credentials"+username+password);
					}
				});

				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
					}
				});

				alert.show();

			}

			//next put the entries into the db and after successful entry set the alarm using alarm manager.
		}

		else
			showResponse(response);
	}



	/**
	 * Function responsible for performing the validation tests on the data entered by the user for scheduling the backup
	 * @return true if the data entered by the user passes the validation tests flase otherwise
	 */
	public boolean validateFields(){
		response="";

		if(backupname.length()==0){
			response=getText(R.string.emptywarning).toString()+" "+getText(R.string.schedulebackupname).toString();
			return false;
		}

		if(backuptime.length()==0){
			response=getText(R.string.emptywarning).toString()+" "+getText(R.string.schedulebackuptime).toString();
			return false;
		}

		if(backuptype.length()==0){
			response=getText(R.string.emptywarning).toString()+" "+getText(R.string.schedulebackuptype).toString();
			return false;
		}

		if(backupday.length()==0){
			response=getText(R.string.emptywarning).toString()+" "+getText(R.string.schedulebackupday).toString();
			return false;
		}

		if(contenttype.trim().length()==0){
			response=getText(R.string.emptywarning).toString()+" "+getText(R.string.schedulebackupcontenttype).toString();
			return false;
		}

		else return true;
	}



	/**
	 * This function is responsible for displaying a dialog box in case any notification is to be shown to the user
	 * @author Nitin
	 * @param String response 
	 * response is the message that has to be displayed to the user in the dialog box
	 *   
	 * passwordchangedflag is set in the method forgotpasswordWS() when the user.forgotpassword web-service returns a success 
	 * This flag allows to handle this special case where after displaying the dialog box the login activity needs to be shown
	 */
	public void showResponse(String response){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setInverseBackgroundForced(true)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				if(succesfullyscheduledflag){
					Intent explicitIntent=new Intent(ScheduleBackupActivity.this,BackupTypeSelectionActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}

			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}



	/**
	 * Function called when Choose content type is clicked on the screen
	 * @param view
	 * view object gives all the layout elements of the screen.
	 */
	public void onSelectFilesClick(View view){
		//code to create the alertdialog for file selection
		showDialog(CONTENT_DIALOG_ID);
	}

	

	/**
	 * method to check whether the username and password are there in the preference and if not there obtain them using a dialog and save them in preferences file
	 */
	public boolean checkPreferenceCredentials(){
		SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);   
		username = pref.getString(PREF_USERNAME, null);
		password = pref.getString(PREF_PASSWORD, null);

		if(username!=null && password!=null){
			return true;

		}

		else return false;
	}

	

	/**
	 * insert the entry into the db after checking that another entry with same time is not there
	 * @param bname
	 * the name of the backup
	 * @param btime
	 * the time of the backup
	 * @param btype
	 * the type of the backup
	 * @param bcontent
	 * the content type to be backed up
	 * @param bday
	 * the day of the backup, in case its a weekly backup
	 */
	public void insertIntoDB(final String bname,final String btime,final String btype,final String bcontent,final String bday){

		boolean duplicateflag=false;
		String mtemp="";

		SchedulesDataHelper datahelper=new SchedulesDataHelper(this);
		List<String> names = datahelper.selectAll();
		StringBuilder sb = new StringBuilder();

		for (String temp : names){
			sb.append(temp);
			sb.append("\n");
			mtemp=temp;

			System.out.println(temp);

			if(btype.equalsIgnoreCase("daily")){
				if(temp.contains(btime)){
					duplicateflag=true;
					response="A schedule called "+mtemp.subSequence(mtemp.indexOf("\n")+1,mtemp.indexOf("\n", mtemp.indexOf("\n")+1))+" has already been scheduled for this time.Pick another time";
					break;
				}
			}

			else if(btype.equalsIgnoreCase("weekly")){
				if(temp.contains(btime) && temp.contains(bday)){
					duplicateflag=true;

					if(temp.contains("Daily"))
						response="A schedule called "+mtemp.subSequence(mtemp.indexOf("\n")+1,mtemp.indexOf("\n", mtemp.indexOf("\n")+1))+" has already been scheduled for this time and day.Pick another time";

					else if(temp.contains("Weekly"))
						response="A schedule called "+mtemp.subSequence(mtemp.indexOf("\n")+1,mtemp.indexOf("\n", mtemp.indexOf("\n")+1))+" has already been scheduled for this time and day.Pick another time or day";

					break;
				}

			}

			//			if(temp.contains(btime)){
			//				duplicateflag=true;
			//				break;
			//			}
		}

		//needs to be improvised *************************************************************************
		if(duplicateflag){
			showResponse(response);	
			datahelper.closeDB();
		}

		else{
			datahelper.insert(bname, btime, btype, bcontent, bday,username);
			setAlarm(datahelper.lastEntryID());
			datahelper.closeDB();
		}
		//		//this code is jst to check whether databse entry was successfull or not
		//		List<String> names = datahelper.selectAll();
		//		StringBuilder sb = new StringBuilder();
		//	 
		//		for (String temp : names){
		//			sb.append(temp);
		//			sb.append("\n");
		//		}
		//		
		//		System.out.println("schedules are "+sb.toString());

	}


	/**
	 *  this function reads the top most row's id and uses the other detaisl along with it to create the alarm
	 *  handle conditions for recurring alarms
	 *  @param id
	 * 	The id of the alarm to be set
	 */
	public void setAlarm(final int id){

		if(id==-1){
			response="DB ERROR";
			showResponse(response);
		}

		else{
			//System.out.println("id is :"+id);
			System.out.println("time: "+backuptime+"name :"+backupname+"type :"+backuptype+"day :"+backupday+"content :"+contenttype+"mhour"+mHour+"mminute"+mMinute+"day"+mdayofweek+"contentname"+contentname);

			Intent myIntent = new Intent(getApplicationContext(),DaemonBackupService.class);
			myIntent.putExtra("backupname", backupname);
			myIntent.putExtra("contenttype", contenttype);
			myIntent.putExtra("contentname",contentname.substring(0,contentname.length()-1));
			myIntent.putExtra("backuptype", backuptype);
			myIntent.putExtra("username", username);

			//myIntent.putExtra("smallstring","I come from another activity...again and yet again"+count);

			pendingIntent = PendingIntent.getService(getApplicationContext(),id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			System.out.println("alarm set with id :>>>>>>>>>>>>>>>>"+(id));

			AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
			alarmManager.cancel(pendingIntent);
			alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);

			Calendar cal,current_cal;

			int current_hour=-1;

			current_cal=new GregorianCalendar();

			if(current_cal.get(Calendar.AM_PM)==1)
				current_hour=current_cal.get(Calendar.HOUR)+12;

			else 
				current_hour=current_cal.get(Calendar.HOUR);

			//System.out.println("current_day>>>>>>"+current_cal.get(Calendar.DAY_OF_WEEK));

			if(backuptype.equalsIgnoreCase("daily")){

				cal = Calendar.getInstance();

				if((mHour<current_hour)||((mHour==current_hour) &&  (mMinute<current_cal.get(Calendar.MINUTE)))){
					cal.add(Calendar.DAY_OF_YEAR, current_cal.get(Calendar.DAY_OF_YEAR));
					System.out.println("daily set for next day");
				}

				cal.set(Calendar.HOUR_OF_DAY, mHour);
				cal.set(Calendar.MINUTE, mMinute);
				cal.set(Calendar.SECOND,0);
				cal.set(Calendar.MILLISECOND,0);

				//System.out.println("time from calendar daily>>>>>>"+cal.getTimeInMillis());

				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),24*3600*1000,pendingIntent);

				//Toast.makeText(ScheduleBackupActivity.this, "Daily Backup Scheduled", Toast.LENGTH_LONG).show();
				succesfullyscheduledflag=true;
				showResponse("Daily Backup Scheduled");

				//cal.add(Calendar.DAY_OF_YEAR, cur_cal.get(Calendar.DAY_OF_YEAR));
			}

			else if (backuptype.equalsIgnoreCase("weekly")){

				cal = Calendar.getInstance();

				if((mHour<current_hour)||((mHour==current_hour) &&  (mMinute<current_cal.get(Calendar.MINUTE)))||(mdayofweek<current_cal.get(Calendar.DAY_OF_WEEK))){
					cal.add(Calendar.WEEK_OF_YEAR, current_cal.get(Calendar.WEEK_OF_YEAR));
					System.out.println("weekly set for next week");
				}

				cal.set(Calendar.HOUR_OF_DAY, mHour);
				cal.set(Calendar.MINUTE, mMinute);
				cal.set(Calendar.SECOND,0);
				cal.set(Calendar.MILLISECOND,0);
				cal.set(Calendar.DAY_OF_WEEK,mdayofweek);

				//System.out.println("time from calendar weekly>>>>>>"+cal.getTimeInMillis());

				alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),7*24*3600*1000,pendingIntent);

				//Toast.makeText(ScheduleBackupActivity.this, "Weekly Backup Scheduled", Toast.LENGTH_LONG).show();

				succesfullyscheduledflag=true;
				showResponse("Weekly Backup Scheduled");

			}
		}
	}
}