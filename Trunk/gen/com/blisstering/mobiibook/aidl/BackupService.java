/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: C:\\Documents and Settings\\Prateek\\workspace\\android_mobiibook20100210\\src\\com\\blisstering\\mobiibook\\aidl\\BackupService.aidl
 */
package com.blisstering.mobiibook.aidl;
public interface BackupService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.blisstering.mobiibook.aidl.BackupService
{
private static final java.lang.String DESCRIPTOR = "com.blisstering.mobiibook.aidl.BackupService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.blisstering.mobiibook.aidl.BackupService interface,
 * generating a proxy if needed.
 */
public static com.blisstering.mobiibook.aidl.BackupService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.blisstering.mobiibook.aidl.BackupService))) {
return ((com.blisstering.mobiibook.aidl.BackupService)iin);
}
return new com.blisstering.mobiibook.aidl.BackupService.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_cancelBackup:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.cancelBackup();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_resumeBackup:
{
data.enforceInterface(DESCRIPTOR);
this.resumeBackup();
reply.writeNoException();
return true;
}
case TRANSACTION_sendUpdates:
{
data.enforceInterface(DESCRIPTOR);
this.sendUpdates();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.blisstering.mobiibook.aidl.BackupService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
public boolean cancelBackup() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_cancelBackup, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
public void resumeBackup() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_resumeBackup, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
public void sendUpdates() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_sendUpdates, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_cancelBackup = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_resumeBackup = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_sendUpdates = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
}
public boolean cancelBackup() throws android.os.RemoteException;
public void resumeBackup() throws android.os.RemoteException;
public void sendUpdates() throws android.os.RemoteException;
}
