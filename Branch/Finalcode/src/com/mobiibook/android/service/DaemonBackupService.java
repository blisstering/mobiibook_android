/**
 * DaemonBackupService.java
 * @author Nitin Singh
 */
package com.mobiibook.android.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;

import com.mobiibook.android.activity.NotificationsActivity;
import com.mobiibook.android.activity.R;
import com.mobiibook.android.activity.WebServiceCall;
import com.mobiibook.android.utils.Session;


/**
 * Class responsible for running the backup in background when a the time for a scheduled backup arrives
 * @author Nitin Singh
 *
 */
public class DaemonBackupService extends Service {
	
	/**
	 * The string storing the Preference file name used for storing details about notifications
	 */
	private static final String PREFS_NOTIFICATION_FILENAME = "MobiibookNotificationsPrefsFile";
	
	/**
	 * The string used to refer the number of notifications in the Preference file used to store notification data
	 */
	private static final String PREFS_COUNT = "notificationcount";
	
	/**
	 * The string used to refer the details in the Preference file used to store notification data
	 */
	private static final String PREFS_DETAILS = "details";
	
	/**
	 * String for the tag for logging events in this file
	 */
	private static final String TAG = "DaemonBackupService";

	/**
	 * Username of currently logged in user
	 */
	private String username="";
	
	/**
	 * Password of currently logged in user
	 */
	private String password="";
	
	/**
	 * Username of the user who set the schedule
	 */
	private String scheduleusername="";

	/**
	 * The string storing the Preference file name used for storing user credentials
	 */
	public static final String PREFS_NAME = "MobiibookPrefsFile";
	
	/**
	 * The string used to refer the username in the Preference file used to store user credentials
	 */
	private static final String PREF_USERNAME = "username";
	
	/**
	 * The string used to refer the password in the Preference file used to store user credentials
	 */
	private static final String PREF_PASSWORD = "password";

	/**
	 * backup name
	 */
	String backupname="";
	
	/**
	 * Data types to be backed up 
	 */
	String contentname="";
	
	/**
	 * Type of backup
	 */
	String backuptype="";
	
	/**
	 * MIME types of data to be backed up
	 */
	String contenttype="";

	/**
	 * Response to be shown in notification
	 */
	String daemonresponse="";

	/**
	 * Indicates whether session id is set in Session.java or not
	 */
	private boolean gotsessionidflag=false;
	
	/**
	 * Indicates whether the user was successfully logged in the background or not
	 */
	private boolean successfullyloggedinflag=false;
	
	/**
	 * Flag set when the silent background login fails due to any reason
	 */
	private boolean failureflag=false;


	
	/**
	 * Binds the service
	 * 
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	
	
	/**
	 * Called when the service is created
	 */
	@Override
	public void onCreate() {

		Log.d(TAG, "onCreate");
	}

	

	/**
	 * Called when service is destroyed
	 */
	@Override
	public void onDestroy() {

		Log.d(TAG, "onDestroy");
	}

	

	/**
	 * starts the backup and in case a backup is already running a notification is popped up
	 * 
	 */
	@Override
	public int onStartCommand(Intent intent, int flags,int startid) {

		Log.d(TAG, "onStart");
		//		System.out.println("on start>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		successfullyloggedinflag=false;
		gotsessionidflag=false;

		backupname=intent.getStringExtra("backupname");
		contenttype=intent.getStringExtra("contenttype");
		contentname=intent.getStringExtra("contentname");
		backuptype=intent.getStringExtra("backuptype");	
		scheduleusername=intent.getStringExtra("username");

		backupProcess();

		return START_STICKY;
	}



	/**
	 * starts the backupprocess after doing the checks
	 */
	public void backupProcess(){
		successfullyloggedinflag=false;
		gotsessionidflag=false;
		failureflag=false;
		
		//System.out.println("session id in backupprocess is >>>>>>>>>>>>>>>>>>>>>"+Session.getSessionId());
		
		//		System.out.println("entering backuprocess>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		//if user is not logged in login using the user credentials saved in preferences
		if(Session.getSessionId()==null){

			SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);   
			username = pref.getString(PREF_USERNAME, null);
			password = pref.getString(PREF_PASSWORD, null);

			if(username==null||password==null){
				daemonresponse=getText(R.string.remembermenotchecked).toString();

				showNotification(getText(R.string.mobiibooknotification).toString(),backupname+getText(R.string.schedulefailed).toString(),daemonresponse);

			}

			else{

				Log.d(TAG,username);
				Log.d(TAG,password);

				//System.out.println("username from preferences:>>>>>>>>>>>>>>>>>"+username);
				//System.out.println("username from preferences:>>>>>>>>>>>>>>>>>"+password);

				System.out.println("username from preferences:>>>>>>>>>>>>>>>>>"+scheduleusername);

				if(username.equals(scheduleusername))
					//call the WS to do the login
					systemConnectWS(username,password);

				else 
					failureflag=true;
			}
		}

		//if any backup is already running show a notification saying that a scheduled backup has been cancelled
		if(Session.isBackupServiceRunning()){

			Log.d(TAG,">>>>>>>>>>>>>>>> backup already running <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			daemonresponse=getText(R.string.backupalreadyrunning).toString();

			showNotification(getText(R.string.mobiibooknotification).toString(),backupname+getText(R.string.schedulefailed).toString(),daemonresponse);

		}

		//if any backup is already running show a notification saying that a scheduled backup has been cancelled
		if(Session.isRestoreServiceRunning()){

			Log.d(TAG,">>>>>>>>>>>>>>>> restore already running <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			daemonresponse=getText(R.string.restorealreadyrunning).toString();

			showNotification(getText(R.string.mobiibooknotification).toString(),backupname+getText(R.string.schedulefailed).toString(),daemonresponse);

		}



		//start the backup service
		else if(Session.getSessionId()!=null && !Session.isBackupServiceRunning()&&!Session.isRestoreServiceRunning()&&!failureflag){

			//System.out.println("username from preferences final:>>>>>>>>>>>>>>>>>"+username);
			//System.out.println("username from preferences final:>>>>>>>>>>>>>>>>>"+password);

			//System.out.println("username from preferences:>>>>>>>>>>>>>>>>>"+scheduleusername);

			SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE); 
			
			if(pref.getString(PREF_USERNAME, null).equals(scheduleusername))
			{
				Log.d(TAG,backupname);
				Log.d(TAG,contenttype);
				Log.d(TAG,contentname);

				Intent i = new Intent(DaemonBackupService.this,BackupService.class);

				i.putExtra("backupName", backupname);
				i.putExtra("contenttypetobebackedup",contenttype);
				i.putExtra("contentnametobebackedup",contentname);
				i.putExtra("backuptype",backuptype);	

				//System.out.println("starting the service>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				if(contenttype.contains("contacts")||contenttype.contains("all"))
				{
					System.out.println("calling export contacts");
					exportContact();
				}


				startService(i);
			}
		}

	}


	
	
	/**
	 * Exports all the contact to a .vcf file saved in the externalStorage 
	 * @author Prateek
	 */
	public void exportContact()
	{
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		System.out.println("The count of contacts is " + cur.getCount());
		if (cur.getCount() > 0) {
			try{
				String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
				File vCardFile = new File(extStorageDirectory,"/contacts.vcf");
				if (!vCardFile.exists()) {
					try {
						vCardFile.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				OutputStream os = new FileOutputStream(vCardFile);

				while (cur.moveToNext()) {
					try{
						String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
						Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
						System.out.println("The value is " + cr.getType(uri));
						AssetFileDescriptor fd = cr.openAssetFileDescriptor(uri, "r");
						FileInputStream fis = fd.createInputStream();
						byte[] b = new byte[1024*4];  
						int read;  
						while ((read = fis.read(b)) != -1) {  
							os.write(b, 0, read);  
						} 
						//								byte[] buf = new byte[(int)fd.getDeclaredLength()];
						//								if (0 < fis.read(buf))
						//								{
						//								String vCard = new String(buf);
						//								System.out.println("The vCard value is " + vCard);
						//								}
						fis.close();
					}
					catch(Exception e)
					{
						System.out.println(e.getStackTrace());
					}
				}
				os.flush();
				os.close();	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}
	}

	

	/**
	 * Function responsible for generating the notifications for the application
	 * @param tickertext the scroll text to be shown on the system tray
	 * @param title title of the notification
	 * @param text the content of the notification
	 */
	public void showNotification(final String tickertext,final String title,final String text){

		failureflag=true;
		
		//preference file for storing notification information
		SharedPreferences pref = getSharedPreferences(PREFS_NOTIFICATION_FILENAME,MODE_PRIVATE); 
		int tempcount=pref.getInt(PREFS_COUNT, 0);
		String tempdetails=pref.getString(PREFS_DETAILS,"");
		
		
		GregorianCalendar cal=new GregorianCalendar();
		String time=""+cal.get(Calendar.HOUR_OF_DAY);
		if(cal.get(Calendar.MINUTE)<10)
			time+=":0"+cal.get(Calendar.MINUTE);
		else 
			time+=":"+cal.get(Calendar.MINUTE);
		
		getSharedPreferences(PREFS_NOTIFICATION_FILENAME,MODE_PRIVATE)
		.edit()
		.putInt(PREFS_COUNT, tempcount+1) 
		.putString(PREFS_DETAILS,tempdetails+"Backup name  : "+title+"\n"+"Time \t\t\t\t: "+ time+"\n"+text+"\n\n\n")
		.commit();

		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.mobiibooklogo,tickertext, System.currentTimeMillis());
		
		
		PendingIntent contentIntent = PendingIntent.getActivity(this,1,new Intent(this,NotificationsActivity.class), 0);

		notification.setLatestEventInfo(this,"MobiiBook","("+(tempcount+1)+")"+" Notification.Tap here for details.",null);
		notification.contentIntent=contentIntent;
		notification.flags=Notification.FLAG_AUTO_CANCEL;
		

		manager.notify(1, notification);
		
	} 


	
	
	/**
	 * system.connect web service call
	 * @author Prateek
	 */
	public void systemConnectWS(String uname,String pwd)
	{
		//		System.out.println("starting the systemeconnecct>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("system.connect", "UTF-8");
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Calling web service");

			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					System.out.println("Error in system.connect");
					daemonresponse="error in system.connect";
				}
				else
				{
					String sessionId = obj.getString("sessid");
					Session.setSessionId(sessionId);
					if(Session.getSessionId() != null)
					{
						gotsessionidflag=true;
						loginWS(uname,pwd);
					}
					else
					{
						daemonresponse="session id not available";
					}
				}
			} else {
				System.out.println("The value is " + getText(R.string.network_message).toString());
				daemonresponse = getText(R.string.network_message).toString();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if(!gotsessionidflag)
			showNotification(getText(R.string.mobiibooknotification).toString(),backupname+getText(R.string.schedulefailed).toString(),daemonresponse);

	}

	
	/**
	 * user.login web service call
	 * @param String uname
	 * Username of the user
	 * @param String pwd
	 * Password of the user
	 * @author Prateek
	 * 
	 */
	public void loginWS(final String uname,final String pwd)
	{
		//		System.out.println("starting the login>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		//		System.out.println("username from loginws:>>>>>>>>>>>>>>>>>"+uname);
		//		System.out.println("username from loginws:>>>>>>>>>>>>>>>>>"+pwd);

		try {
			String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.login", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(uname, "UTF-8");
			data += "&" + URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(pwd, "UTF-8");
			WebServiceCall wsCall = new WebServiceCall(data);

			System.out.println("Calling login....");

			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());

				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					//System.out.println(obj.get("#message"));
					daemonresponse = obj.get("#message").toString();

				}
				else
				{
					System.out.println("the object returned " + obj);
					Session.setSessionId(new String(obj.getString("sessid")));
					System.out.println("Session Id(Logged In)"+Session.getSessionId());
					successfullyloggedinflag=true;
				}
			} else {
				System.out.println("The value is " + getText(R.string.network_message).toString());
				daemonresponse = getText(R.string.network_message).toString();

			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}


		//		if(successfullyloggedinflag)
		//		{
		//			System.out.println("calling loginsuccess from login>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		//			loginSuccess();
		//		}
		//		else 
		//			showNotification("tickertext","title","sch backup"+backupname+"failed due to"+daemonresponse);


		if(!successfullyloggedinflag)
			showNotification(getText(R.string.mobiibooknotification).toString(),backupname+getText(R.string.schedulefailed).toString(),daemonresponse);
	}


	//	public void loginSuccess(){
	//		if(Session.isBackupServiceRunning()){
	//
	//			Log.d(TAG,">>>>>>>>>>>>>>>> backup already running <<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	//
	//			showNotification("tickertext","title","Scheduled Backup "+backupname+ "cancelled");
	//
	//		}
	//
	//
	//		//start the backup service
	//		else{
	//			Log.d(TAG,backupname);
	//			Log.d(TAG,contenttype);
	//			Log.d(TAG,contentname);
	//
	//			Intent i = new Intent(DaemonBackupService.this,BackupService.class);
	//
	//			i.putExtra("backupName", backupname);
	//			i.putExtra("contenttypetobebackedup",contenttype);
	//			i.putExtra("contentnametobebackedup",contentname);
	//			i.putExtra("backuptype",backuptype);	
	//
	//			System.out.println("starting the service within loginsuccess>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	//			startService(i);
	//		}
	//	}

}