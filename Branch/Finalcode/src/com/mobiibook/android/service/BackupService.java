/**
 * BackupService.java
 * @author Prateek Jain
 */
package com.mobiibook.android.service;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.mobiibook.android.activity.NotificationsActivity;
import com.mobiibook.android.activity.R;
import com.mobiibook.android.backup.BackupContentsThread;
import com.mobiibook.android.utils.Session;

/**
 * Service that runs in the background for backup
 * @author Prateek jain
 *
 */
public class BackupService extends Service{

	/**
	 * Object reference to the BackupContentsThread class
	 */
	BackupContentsThread backupContentThread;

	/**
	 * The response returned by the BackupContentsThread
	 */
	String threadResponse = "";

	/**
	 * Value from 0-100 showing the progress of the current backup
	 */
	int progressValue;

	/**
	 * Flag indicating whether the current backup is over or not
	 */
	boolean isBackupProcedureOver = false;

	/**
	 * Flag set when a network exception occurs
	 */
	boolean networkException = false;

	/**
	 * Backup date
	 */
	String date;

	/**
	 * Backup time
	 */
	String time;

	/**
	 * Content being backed up
	 */
	String contents;

	String contenttypetobebackedup="all";

	/**
	 * Type of data items being backed up
	 */
	String contentnametobebackedup;

	/**
	 * Type of backup (Manual,Scheduled)
	 */
	String backuptype;

	/**
	 * Backup name
	 */
	String bName="";

	/**
	 * Flag to be set depending on whether or not the stored session id in Session.java is null nor not , if null the appliacation is deemed as crashed and the user needs to login again to establish a valid session 
	 */
	boolean crashflag=false;

	/**
	 * The string storing the Preference file name used for storing details about notifications
	 */ 
	private static final String PREFS_NOTIFICATION_FILENAME = "MobiibookNotificationsPrefsFile";

	/**
	 * The string used to refer the number of notifications in the Preference file used to store notification data
	 */
	private static final String PREFS_COUNT = "notificationcount";

	/**
	 * The string used to refer the details in the Preference file used to store notification data
	 */
	private static final String PREFS_DETAILS = "details";

	/**
	 * String constant for the broadcast intent of this service
	 */
	public static final String BROADCAST_ACTION="com.blisstering.mobiibook.service.BackupServiceActivity";


	private Intent broadcast=new Intent(BROADCAST_ACTION);


	/**
	 * Binds the service
	 * @author Prateek
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		//arg0.getExtras();
		return binder;
	}



	/**
	 * Called when the service is created
	 * @author Prateek
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		date = DateFormat.getDateInstance().format(new Date()).toString();
		time = DateFormat.getTimeInstance().format(new Date()).toString();
		contents = "All";
		//contentDetails = "Calendars,Contacts,Photos,Music,Videos";
		//Toast.makeText(this,"Service created", Toast.LENGTH_LONG).show();
	}



	/**
	 * Called when service is destroyed
	 * @author Prateek
	 */
	@Override
	public void onDestroy() {
		Session.setBackupService(false);
		if(backupContentThread!= null)
			backupContentThread.backupCancel();
		System.out.println("In destroy");
		super.onDestroy();
		//Toast.makeText(this,"Service destroyed", Toast.LENGTH_LONG).show();
	}



	/**
	 * Called when service is rebinded
	 * @author Prateek
	 */

	@Override
	public void onRebind(Intent intent) {

	}



	/**
	 * Called when service is unbinded
	 * @author Prateek
	 */
	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(this.getClass().getName(), "UNBIND");
		return true;
	}
	//	public void onStart(int startId, Bundle arguments) {
	//	super.onStart( startId, arguments );
	//	Log.d( LOG_TAG, "onStart" );
	//	serviceHandler = new Handler();
	//	serviceHandler.postDelayed( new RunTask(),1000L );
	//	}



	/**
	 * Backup start process starts from here. Called when the service starts
	 * @author Prateek
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		super.onCreate();
		Session.setBackupService(true);
		bName = intent.getStringExtra("backupName");
		contenttypetobebackedup=intent.getStringExtra("contenttypetobebackedup");
		contentnametobebackedup=intent.getStringExtra("contentnametobebackedup");
		backuptype=intent.getStringExtra("backuptype");
		startBackup(bName,contenttypetobebackedup,backuptype);
		setProgress();
		return START_STICKY;
	}



	/**
	 * Thread for a backup process is started from here
	 * @param String bName
	 * Name of the backup
	 * @author Prateek
	 */
	public void startBackup(final String bName,final String mcontenttypetobebackedup,final String backuptype)
	{
		new Thread() {
			public void run() {
				backupContentThread = new BackupContentsThread(bName,mcontenttypetobebackedup,backuptype,contentnametobebackedup,getApplicationContext());
				threadResponse = backupContentThread.startBackup();
				Log.d("THREADRESPONSE", threadResponse);
				networkException  = backupContentThread.isNetworkException();
				crashflag=backupContentThread.isCrashflagSet();
				isBackupProcedureOver = true;

				System.out.println("networkexceptions is  "+networkException+">>>>>isloggedout"+Session.isLoggedOut()+">>>>>"+crashflag);

				if(!networkException&&!Session.isLoggedOut()&&!crashflag){
					Session.setBackupService(false);
					System.out.println(">>>>>>>>>>>>>>>>>>>calling notify1");
					showNotification(getText(R.string.mobiibooknotification).toString(),bName, threadResponse);
				}

				else if(Session.isLoggedOut() && !crashflag && !networkException){
					System.out.println(">>>>>>>>>>>>>>>>>>>calling notify2");
					showNotification(getText(R.string.mobiibooknotification).toString(), bName,threadResponse);
				}
				Session.setLoggedOut(false);
			}
		}.start();
	}


	/**
	 * Function responsible for generating the notifications for the application
	 * @param tickertext the scroll text to be shown on the system tray
	 * @param title title of the notification
	 * @param text the content of the notification
	 */
	public void showNotification(final String tickertext,final String title,final String text){

		//preference file for storing notification information
		SharedPreferences pref = getSharedPreferences(PREFS_NOTIFICATION_FILENAME,MODE_PRIVATE); 
		int tempcount=pref.getInt(PREFS_COUNT, 0);
		String tempdetails=pref.getString(PREFS_DETAILS,"");

		getSharedPreferences(PREFS_NOTIFICATION_FILENAME,MODE_PRIVATE)
		.edit()
		.putInt(PREFS_COUNT, tempcount+1) 
		.putString(PREFS_DETAILS,tempdetails+"Backup name  : "+title+"\n"+"Time \t\t\t\t: "+ new GregorianCalendar().get(Calendar.HOUR_OF_DAY)+" : "+new GregorianCalendar().get(Calendar.MINUTE)+"\n"+text+"\n\n\n")
		.commit();



		System.out.println("inside notification>>>>>>>>>>>>>>>>>>>>>>>");
		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.mobiibooklogo,tickertext, System.currentTimeMillis());

		PendingIntent contentIntent = PendingIntent.getActivity(this,1,new Intent(this,NotificationsActivity.class), 0);

		notification.setLatestEventInfo(this,"MobiiBook","("+(tempcount+1)+")"+" Notification.Tap here for details.",contentIntent);
		//notification.contentIntent=contentIntent;
		notification.flags=Notification.FLAG_AUTO_CANCEL;

		//this makes sure that notifcations dont get overrided by each other.
		//		Random random=new Random();

		//		int temp=random.nextInt(100);

		int temp=1;

		System.out.println("notification id is >>>>>>>>>>>>>>"+temp);

		manager.notify(temp, notification);
	} 



	/**
	 * Called when the service is started
	 * @author Prateek
	 */
	@Override
	public void onStart(Intent intent, int startId) {
		super.onCreate();
		Session.setBackupService(true);
		setProgress();
		//		System.out.println("The service is started");
		//		System.out.println("The argsss " + intent.getExtras());
		//super.onStart( startId, arguments );
		//Toast.makeText(this,"Service started", Toast.LENGTH_LONG).show();
	}

	/*
	 * Binder responsible for connecting to any activity and contains the function described in aidl files by which activity 
	 * communicates with the service
	 */

	private final com.mobiibook.android.aidl.BackupService.Stub binder = 
		new com.mobiibook.android.aidl.BackupService.Stub() {
		//		public void startBackup(String bName)
		//		{
		////		backupName = bName;
		////		backupContentThread = new BackupContentsThread(backupName,"Manual");
		////		threadResponse = backupContentThread.startBackup();
		////		isBackupProcedureOver = true;
		//		}



		/**
		 * Cancels the backup
		 * @author Prateek
		 */
		public boolean cancelBackup()
		{
			if(backupContentThread != null)
			{
				backupContentThread.backupCancel();
				return true;
			}
			else
			{
				return false;
			}
		}



		/**
		 * To initiate the resume process of the backup
		 * @author Prateek
		 */
		public void resumeBackup()
		{
			new Thread() {
				public void run() {
					isBackupProcedureOver = false;
					networkException = false;
					setProgress();
					threadResponse = backupContentThread.resumeBackup();
					networkException  = backupContentThread.isNetworkException();
					System.out.println("The backup needs to be resumed");
					isBackupProcedureOver = true;


					System.out.println("doing the resume backup thing>>>>>>>>>>>>.");
					System.out.println("networkexceptions is  "+networkException+">>>>>isloggedout"+Session.isLoggedOut()+">>>>>"+crashflag);

					if(!networkException&&!Session.isLoggedOut()&&!crashflag){
						Session.setBackupService(false);
						System.out.println(">>>>>>>>>>>>>>>>>>>calling notify1");
						showNotification(getText(R.string.mobiibooknotification).toString(),bName, threadResponse);
					}

					else if(Session.isLoggedOut() && !crashflag && !networkException){
						System.out.println(">>>>>>>>>>>>>>>>>>>calling notify2");
						showNotification(getText(R.string.mobiibooknotification).toString(), bName,threadResponse);
					}
					Session.setLoggedOut(false);
				}
			}.start();
		}

		/**
		 * If backup process is running starts the send progress thread
		 * @author Prateek
		 */

		public void sendUpdates() throws RemoteException {
			// TODO Auto-generated method stub
			if(isBackupProcedureOver)
				setProgress();
		}
	};



	/**
	 * Returns the binder
	 * @author Prateek
	 */
	public IBinder getBinder() {
		return binder;
	}



	/**
	 * Starts the thread which broadcasts the information, received by an activity and shown on the screen
	 * @author Prateek
	 */
	public void setProgress()
	{
		new Thread() {
			public void run() {
				// TODO Auto-generated method stub
				progressValue = 0;
				while(true){
					try{
						if(backupContentThread != null)
						{
							progressValue = backupContentThread.getProgess();
							broadcast.putExtra("progress", progressValue);
							broadcast.putExtra("response", threadResponse);
							broadcast.putExtra("backupover", isBackupProcedureOver);
							broadcast.putExtra("isNetworkException", networkException);
							broadcast.putExtra("crashflag", crashflag);
							broadcast.putExtra("date", date);
							broadcast.putExtra("time", time);
							broadcast.putExtra("contents", contents);
							broadcast.putExtra("contentDetails", contentnametobebackedup);

							sendBroadcast(broadcast);
							if(isBackupProcedureOver || networkException)
								break;
							Thread.sleep(2000);
						}
					}
					catch(Throwable t){
					}
				}
			}
		}.start();
	}
}