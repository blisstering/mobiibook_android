/**
 * RestoreContentsThread.java
 * @author Prateek Jain
 */
package com.mobiibook.android.restore;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

import com.mobiibook.android.activity.WebServiceCall;
import com.mobiibook.android.utils.CalendarXMLHandler;
import com.mobiibook.android.utils.MessagesXMLHandler;
import com.mobiibook.android.utils.Session;

/**
 * Class responsible for running the restore process in the background 
 * @author Prateek Jain
 */
public class RestoreContentsThread {
	
	/**
	 * Backup id of the backup being restored
	 */
	String backupId;
	
	/**
	 * Message returned by restore WS restore.start,restore.complete,restore.cancel,restore.filetransfer
	 */
	String returnMessage="";
	
	String[] downloadLinks;
	
	/**
	 * The JSONArray of files to be restored
	 */
	JSONArray jArray;
	
	/**
	 * The restore id of the current restore returned by restore.start 
	 */
	String restoreId;
	
	/**
	 * Flag set when the current restore is to be resumed
	 */
	boolean continueRestore = true;
	
	/**
	 * Flag set when the current restore is to be cancelled
	 */
	boolean restoreCancel = false;
	
	/**
	 * Flag set when a network exception has occured
	 */
	boolean networkException = false;
	
	/**
	 * Flag set when the device has ran out of memory
	 */
	boolean outOfMemoryException = false;
	
	/**
	 * The number of bytes to be downloaded from the server for current restore
	 */
	long bytesToDownload;
	
	/**
	 * The number of bytes downloaded from the server for current restore
	 */
	long bytesDownloaded = 0;
	
	/**
	 * Number of files already restored in the current restore
	 */
	int numberOfFilesRestored = 0;
	
	/**
	 * The json object returned by the various WS used in restore
	 */
	JSONObject obj;
	
	/**
	 * Object reference to the current context of the application
	 */
	Context context;

	/**
	 * String containing the full path of the temporary file(mobiicalendar.calendar) generated during backup which contains the calendar events and which is sent to the server 
	 */
	private static final String EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiicalendar.calendar";
	
	/**
	 * String containing the full path of the temporary file(mobiimessages.messages) generated during backup which contains the text messages and which is sent to the server 
	 */
	private static final String MESSAGES_EXPORT_FILE_NAME =  Environment.getExternalStorageDirectory().toString()+"/mobiimessages.messages";
	
	
	
	/**
	 * Constructor of the class
	 * @param String backupid 
	 * @author Prateek
	 */
	public RestoreContentsThread(String backupid) {
		// TODO Auto-generated constructor stub
		backupId = backupid;
	}
	
	

	/**
	 * Starts the restore procedure
	 * @param Context context1
	 * Context of the Activity which will be used in restoring the contacts
	 * @author Prateek
	 */
	public String startRestore(Context context1)
	{
		context = context1;
		startRestoreWS();
		return returnMessage;
	}

	
	
	/**
	 * Returns if restore was stopped due to any network exception or not
	 * @return boolean
	 * @author Prateek
	 */
	public boolean isNetworkException()
	{
		return networkException;
	}

	
	
	/**
	 * Returns if the memory if full or not 
	 * @return boolean
	 * @author Prateek
	 */
	public boolean isOutOfMemoryException()
	{
		return outOfMemoryException;	
	}

	
	
	/**
	 * restore.start web service is called from here
	 * @author Prateek
	 */
	public void startRestoreWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("restore.start", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			//HardCoding for Android Client ID
			data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
			data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backupId, "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					returnMessage = obj.get("#message").toString();
					continueRestore = false;
				}
				else
				{
					System.out.println("@startBackupWS : The object value " + obj.toString());
					restoreId = obj.getString("restore_id");
					System.out.println("the restore id is " + restoreId);
					startRestoreProcedure(obj,false);
				}
			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	/**
	 * Restore procedure starts from here
	 * @param JSONObject obj
	 * The json object of links of files to be restored
	 * @param resumeRestore
	 * If true -  means that restore process is resumed which was being interrupted previously
	 * If false - The restore process is strated from beginning 
	 * @author Prateek
	 */
	public void startRestoreProcedure(JSONObject obj,boolean resumeRestore)
	{
		try{
			System.out.println("============== " + obj.getJSONArray("files").toString());
			//returnMessage = obj.getJSONArray("files").toString();
			jArray = obj.getJSONArray("files");
			downloadLinks = new String[jArray.length()];
			JSONObject jObj;
			if(!resumeRestore)
			{
				bytesToDownload = 0;
				for(int i=0;i<jArray.length();i++)
				{
					bytesToDownload += jArray.getJSONObject(i).getLong("size");
				}
			}
			if(!resumeRestore)
				numberOfFilesRestored = 0;
			for(int i=numberOfFilesRestored;i<jArray.length();i++)
			{
				jObj = jArray.getJSONObject(i);
				if(continueRestore && !restoreCancel && !networkException)
				{
					restore(jObj.getString("fid"),jObj.getString("mobile_file_path"), jObj.getString("download_url"),jObj.getLong("last_modified_time"),jObj.getLong("size"),jObj.getString("name"));
				}
				else
				{
					break;
				}
			}
			if(!networkException)
			{
				if(restoreCancel)
					restoreCancelWS();
				else if(continueRestore)
					restoreCompleteWS();
			}
		}catch(Exception e)
		{
			continueRestore = false;
			e.printStackTrace();
		}

	}

	
	
	/**
	 * Actual restore of the file is done here and saved to there path
	 * @param String fid
	 * ID of the file.
	 * @param String absolutePath
	 * Path of the file
	 * @param String downloadUrl
	 * Download URL of the file
	 * @param Long lastModifiedTime
	 * Last Modified time of the file
	 * @param Long fileSize
	 * Size of the file
	 * @param String fileName
	 * Name of the file
	 * @author Prateek
	 */
	public void restore(String fid, String absolutePath, String downloadUrl, Long lastModifiedTime,Long fileSize, String fileName)
	{
		try{
			System.out.println(fid + "." + absolutePath + "." + downloadUrl + "." + lastModifiedTime +  "." + fileName+ "." + fileName);
			URL url = new URL(downloadUrl+"?sessid=" + Session.getSessionId()); //you can write here any link
			File file = new File(absolutePath);
			System.out.println("The values " + file.exists() + "-" + file.lastModified()+ "-" + lastModifiedTime + "-" + file.length()+ "-" + fileSize);
			//File file = new File("data/data/com.android.createFileTest/test.txt");
			if(file.exists() && file.lastModified() == lastModifiedTime && file.length() == fileSize)
			{
				restoreFileTransferWS(restoreId, fid);
				if(continueRestore && !networkException)
				{
					bytesDownloaded += fileSize;
					numberOfFilesRestored++;
				}
			}
			else
			{
				// Checks if the memory is available of not
				StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath()); 
				long bytesAvailable = (long)stat.getAvailableBlocks() * (long)stat.getBlockSize();
				if(bytesAvailable > fileSize)
				{
					//Create parent directory if it doesn't exists
					if(!new File(file.getParent()).exists())
					{
						if(!(new File(file.getParent()).mkdirs()))
							absolutePath = Environment.getExternalStorageDirectory().toString() + "/" + fileName;
					}
					file = new File(absolutePath + ".mobiibookpart");
					file.createNewFile();
					/* Open a connection to that URL. */
					URLConnection ucon = url.openConnection();
					/*
					 * Define InputStreams to read from the URLConnection.
					 */
					InputStream is = ucon.getInputStream();
					/*
					 * Read bytes to the Buffer until there is nothing more to read(-1).
					 */
					FileOutputStream fos = new FileOutputStream(file);
					int size = 1024*1024;
					byte[] buf = new byte[size];
					int byteRead;
					long bytesBeforeDownloaded = bytesDownloaded;
					while (((byteRead = is.read(buf)) != -1) && !restoreCancel) {
						fos.write(buf, 0, byteRead);
						bytesDownloaded += byteRead;
					}


					fos.close();

					if(file.length() != fileSize)
					{
						networkException = true;
						bytesDownloaded = bytesBeforeDownloaded;
						file.delete();
					}
					else
					{
						//Change the last modified time of the file to the one given by the web service 
						file.setLastModified(lastModifiedTime);
						file.renameTo(new File(absolutePath));
						if(fileName.equalsIgnoreCase("contacts.vcf"))
							importContacts();

						if(fileName.equalsIgnoreCase("mobiicalendar.calendar"))
							importCalendar();
						
//						if(fileName.contains(".apk"))
//							restoreApplications();

//uncomment the code below to make the sms restore work
//						if(fileName.equalsIgnoreCase("mobiimessages.messages"))
//							importMessages();


						if(!restoreCancel)
						{
							restoreFileTransferWS(restoreId, fid);
							if(continueRestore && !networkException)
								numberOfFilesRestored++;
						}
					}
				}
				else
				{
					outOfMemoryException = true;
					continueRestore = false;
				}
			}
		}
		catch(FileNotFoundException e)
		{
			//Skip File if it does not exists at server
		}
		catch(IOException io)
		{
			outOfMemoryException  = true;
			continueRestore = false;
		}
		catch(Exception e)
		{	
			continueRestore = false;
			e.printStackTrace();
		}
	}

	
	
	/**
	 * restore.filetransfer web service is called from here
	 * @param String rid
	 * Restore Id
	 * @param String fid
	 * ID of the file
	 * @author Prateek
	 */
	public void restoreFileTransferWS(String rid, String fid)
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("restore.filetransfer", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("restore_id", "UTF-8")+"="+URLEncoder.encode(restoreId, "UTF-8");
			data += "&" + URLEncoder.encode("fid", "UTF-8")+"="+URLEncoder.encode(fid, "UTF-8");

			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			System.out.println("Waiting for thread to die");
			boolean threadStatus = wsCall.isThreadSucceeded();

			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					//System.out.println("Error in restoreFileWS");
					returnMessage = obj.get("#message").toString();
					continueRestore = false;
				}
				else if(!(obj.getInt("response_code") == 0))
				{
					continueRestore = false;
					returnMessage = obj.getString("response_message");
				}

			} else {
				System.out.println("Failure");
				networkException = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			continueRestore = false;
			e.printStackTrace();
		}
	}

	
	
	/**
	 * restore.complete web service is called from here
	 * @author Prateek
	 */
	public void restoreCompleteWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("restore.complete", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("restore_id", "UTF-8")+"="+URLEncoder.encode(restoreId, "UTF-8");
			data += "&" + URLEncoder.encode("restore_status", "UTF-8")+"="+URLEncoder.encode("Completed", "UTF-8");

			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			System.out.println("Waiting for thread to die");
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					returnMessage = obj.get("#message").toString();
					continueRestore = false;
				}
				else
				{
					returnMessage = obj.getString("response_message");
				}

			} else {
				System.out.println("Failure");
				networkException = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			continueRestore = false;
			e.printStackTrace();
		}
	}

	
	
	/**
	 * restore.cancel web service is called from here
	 * @author Prateek
	 */
	public void restoreCancelWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("restore.cancel", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("restore_id", "UTF-8")+"="+URLEncoder.encode(restoreId, "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					System.out.println("Error in cancelRestoreWS");
					returnMessage = obj.get("#message").toString();
					continueRestore = false;
				}
				else
				{
					returnMessage = obj.getString("response_message");
				}

			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	/**
	 * resume the restore process interrupted by network exception of anything else
	 * @author Prateek
	 */
	public String resumeRestore()
	{
		networkException = false;
		restoreCancel = false;
		outOfMemoryException = false;
		continueRestore = true;
		if(restoreId != null)
			startRestoreProcedure(obj, true);
		else
			startRestore(context);
		return returnMessage;
	}
	
	

	/**
	 * cancel the restore process
	 * @author Prateek
	 */
	public void restoreCancel()
	{
		restoreCancel = true;
	}

	
	
	/**
	 * Returns the value of the progress bar
	 * @return int
	 * Value of the percentage completion of porgress bar
	 * @author Prateek
	 */
	public int getProgess()
	{
		try{
			return (int) ((bytesDownloaded * 100)/bytesToDownload);
		}catch(Exception e)
		{
			return 0;
		}
	}

	
	
	/**
	 * Start the import contacts process
	 * @author Prateek
	 */
	public void importContacts()
	{
		ImportContacts obj = new ImportContacts();
		//Hardcoding
		obj.startImport(new File("/sdcard/contacts.vcf"),context);
	}


	
	/**
	 * Start the import calendar process
	 * @author Nitin
	 */
	public void importCalendar()
	{
		try {

			/** Handling XML */

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			File myFile = new File( EXPORT_FILE_NAME );
			myFile.createNewFile();

			System.out.println("reading the file>>>>>>>>>>>>>>>>>>>");

			FileInputStream fOut =  new FileInputStream(myFile);
			BufferedInputStream bos = new BufferedInputStream( fOut );

			/** Create handler to handle XML Tags ( extends DefaultHandler ) */

			System.out.println("starting the hanlder>>>>>>>>>>>>>>>>>>>");

			
			CalendarXMLHandler myXMLHandler = new CalendarXMLHandler(context);
			xr.setContentHandler(myXMLHandler);
			xr.parse(new InputSource(bos));

		} catch (Exception e) {
			System.out.println("XML Pasing Excpetion = " + e);
		}
	}

	

	/**
	 * Start the import messages process
	 * @author Nitin
	 */
	public void importMessages(){
		try {

			/** Handling XML */

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			File myFile = new File( MESSAGES_EXPORT_FILE_NAME );
			myFile.createNewFile();

			System.out.println("reading the file>>>>>>>>>>>>>>>>>>>");

			FileInputStream fOut =  new FileInputStream(myFile);
			BufferedInputStream bos = new BufferedInputStream( fOut );

			/** Create handler to handle XML Tags ( extends DefaultHandler ) */

			System.out.println("starting the hanlder>>>>>>>>>>>>>>>>>>>");

			MessagesXMLHandler myXMLHandler = new MessagesXMLHandler(context);
			xr.setContentHandler(myXMLHandler);
			xr.parse(new InputSource(bos));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	/**
//	 * Start the restore of applications
//	 * @author Nitin
//	 */
// public void restoreApplications(){
//	 System.out.println("inside the restore application function>>>>>>>>>>>>>>>"+(count++));
//	 
//	 
////	 Intent intent=new Intent();
////	 intent.setAction(android.content.Intent.ACTION_VIEW);
////     //intent.setDataAndType(Uri.parse("file:///sdcard/.sampleapk"), "application/vnd.android.package-archive");
////	 
////	 intent.setDataAndType(Uri.parse("file://"+APPLICATIONS_EXPORT_FILE_NAME+"com.facebook.android.apk"), "application/vnd.android.package-archive");
////      
////	 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////	 context.startActivity(intent);
//	 
//	 
// }
}
