/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobiibook.android.utils;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Two-way map that maps MIME-types to file extensions and vice versa.
 */
public class MyMimeTypeMap {

    /**
     * Singleton MIME-type map instance:
     */
    private static MyMimeTypeMap sMyMimeTypeMap;

    /**
     * MIME-type to file extension mapping:
     */
    private HashMap<String, String> mMimeTypeToExtensionMap;

    /**
     * File extension to MIME type mapping:
     */
    private HashMap<String, String> mExtensionToMyMimeTypeMap;

    /**
     * Creates a new MIME-type map.
     */
    private MyMimeTypeMap() {
        mMimeTypeToExtensionMap = new HashMap<String, String>();
        mExtensionToMyMimeTypeMap = new HashMap<String, String>();
    }

    /**
     * Returns the file extension or an empty string iff there is no
     * extension. This method is a convenience method for obtaining the
     * extension of a url and has undefined results for other Strings.
     * @param url
     * @return The file extension of the given url.
     */
    public static String getFileExtensionFromUrl(String url) {
        if (url != null && url.length() > 0) {
            int query = url.lastIndexOf('?');
            if (query > 0) {
                url = url.substring(0, query);
            }
            int filenamePos = url.lastIndexOf('/');
            String filename =
                0 <= filenamePos ? url.substring(filenamePos + 1) : url;

            // if the filename contains special characters, we don't
            // consider it valid for our matching purposes:
            if (filename.length() > 0 &&
                Pattern.matches("[a-zA-Z_0-9\\.\\-\\(\\)]+", filename)) {
                int dotPos = filename.lastIndexOf('.');
                if (0 <= dotPos) {
                    return filename.substring(dotPos + 1);
                }
            }
        }

        return "";
    }

    /**
     * Load an entry into the map. This does not check if the item already
     * exists, it trusts the caller!
     */
    private void loadEntry(String mimeType, String extension) {
        //
        // if we have an existing x --> y mapping, we do not want to
        // override it with another mapping x --> ?
        // this is mostly because of the way the mime-type map below
        // is constructed (if a mime type maps to several extensions
        // the first extension is considered the most popular and is
        // added first; we do not want to overwrite it later).
        //
        if (!mMimeTypeToExtensionMap.containsKey(mimeType)) {
            mMimeTypeToExtensionMap.put(mimeType, extension);
        }

        mExtensionToMyMimeTypeMap.put(extension, mimeType);
    }

    /**
     * Return true if the given MIME type has an entry in the map.
     * @param mimeType A MIME type (i.e. text/plain)
     * @return True iff there is a mimeType entry in the map.
     */
    public boolean hasMimeType(String mimeType) {
        if (mimeType != null && mimeType.length() > 0) {
            return mMimeTypeToExtensionMap.containsKey(mimeType);
        }

        return false;
    }

    /**
     * Return the MIME type for the given extension.
     * @param extension A file extension without the leading '.'
     * @return The MIME type for the given extension or null iff there is none.
     */
    public String getMimeTypeFromExtension(String extension) {
        if (extension != null && extension.length() > 0) {
            return mExtensionToMyMimeTypeMap.get(extension);
        }

        return null;
    }

    /**
     * Return true if the given extension has a registered MIME type.
     * @param extension A file extension without the leading '.'
     * @return True iff there is an extension entry in the map.
     */
    public boolean hasExtension(String extension) {
        if (extension != null && extension.length() > 0) {
            return mExtensionToMyMimeTypeMap.containsKey(extension);
        }
        return false;
    }

    /**
     * Return the registered extension for the given MIME type. Note that some
     * MIME types map to multiple extensions. This call will return the most
     * common extension for the given MIME type.
     * @param mimeType A MIME type (i.e. text/plain)
     * @return The extension for the given MIME type or null iff there is none.
     */
    public String getExtensionFromMimeType(String mimeType) {
        if (mimeType != null && mimeType.length() > 0) {
            return mMimeTypeToExtensionMap.get(mimeType);
        }

        return null;
    }

    /**
     * Get the singleton instance of MyMimeTypeMap.
     * @return The singleton instance of the MIME-type map.
     */
    public static MyMimeTypeMap getSingleton() {
        if (sMyMimeTypeMap == null) {
            sMyMimeTypeMap = new MyMimeTypeMap();

            // The following table is based on /etc/mime.types data minus
            // chemical/* MIME types and MIME types that don't map to any
            // file extensions. We also exclude top-level domain names to
            // deal with cases like:
            //
            // mail.google.com/a/google.com
            //
            // and "active" MIME types (due to potential security issues).
            
            sMyMimeTypeMap.loadEntry("applications/mobiibook-applications","apk");
            
            sMyMimeTypeMap.loadEntry("textmessages/mobiibook-textmessages","messages");
            
            sMyMimeTypeMap.loadEntry("calendar/mobiibook-calendar","calendar");
            
            sMyMimeTypeMap.loadEntry("contacts/mobiibook-contacts","vcf");
            
//            sMyMimeTypeMap.loadEntry("application/andrew-inset", "ez");
//            sMyMimeTypeMap.loadEntry("application/dsptype", "tsp");
//            sMyMimeTypeMap.loadEntry("application/futuresplash", "spl");
//            sMyMimeTypeMap.loadEntry("application/hta", "hta");
//            sMyMimeTypeMap.loadEntry("application/mac-binhex40", "hqx");
//            sMyMimeTypeMap.loadEntry("application/mac-compactpro", "cpt");
//            sMyMimeTypeMap.loadEntry("application/mathematica", "nb");
//            sMyMimeTypeMap.loadEntry("application/msaccess", "mdb");
//            sMyMimeTypeMap.loadEntry("application/oda", "oda");
//            sMyMimeTypeMap.loadEntry("application/ogg", "ogg");
//            sMyMimeTypeMap.loadEntry("application/pdf", "pdf");
//            sMyMimeTypeMap.loadEntry("application/pgp-keys", "key");
//            sMyMimeTypeMap.loadEntry("application/pgp-signature", "pgp");
//            sMyMimeTypeMap.loadEntry("application/pics-rules", "prf");
//            sMyMimeTypeMap.loadEntry("application/rar", "rar");
//            sMyMimeTypeMap.loadEntry("application/rdf+xml", "rdf");
//            sMyMimeTypeMap.loadEntry("application/rss+xml", "rss");
//            sMyMimeTypeMap.loadEntry("application/zip", "zip");
//            sMyMimeTypeMap.loadEntry("application/vnd.android.package-archive", 
              //      "apk");
//            sMyMimeTypeMap.loadEntry("application/vnd.cinderella", "cdy");
//            sMyMimeTypeMap.loadEntry("application/vnd.ms-pki.stl", "stl");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.database", "odb");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.formula", "odf");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.graphics", "odg");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.graphics-template",
//                    "otg");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.image", "odi");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.spreadsheet", "ods");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.spreadsheet-template",
//                    "ots");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.text", "odt");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.text-master", "odm");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.text-template", "ott");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.oasis.opendocument.text-web", "oth");
//            sMyMimeTypeMap.loadEntry("application/msword", "doc");
//            sMyMimeTypeMap.loadEntry("application/msword", "dot");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
//                    "docx");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
//                    "dotx");
//            sMyMimeTypeMap.loadEntry("application/vnd.ms-excel", "xls");
//            sMyMimeTypeMap.loadEntry("application/vnd.ms-excel", "xlt");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
//                    "xlsx");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
//                    "xltx");
//            sMyMimeTypeMap.loadEntry("application/vnd.ms-powerpoint", "ppt");
//            sMyMimeTypeMap.loadEntry("application/vnd.ms-powerpoint", "pot");
//            sMyMimeTypeMap.loadEntry("application/vnd.ms-powerpoint", "pps");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
//                    "pptx");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.presentationml.template",
//                    "potx");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
//                    "ppsx");
//            sMyMimeTypeMap.loadEntry("application/vnd.rim.cod", "cod");
//            sMyMimeTypeMap.loadEntry("application/vnd.smaf", "mmf");
//            sMyMimeTypeMap.loadEntry("application/vnd.stardivision.calc", "sdc");
//            sMyMimeTypeMap.loadEntry("application/vnd.stardivision.draw", "sda");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.stardivision.impress", "sdd");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.stardivision.impress", "sdp");
//            sMyMimeTypeMap.loadEntry("application/vnd.stardivision.math", "smf");
//            sMyMimeTypeMap.loadEntry("application/vnd.stardivision.writer",
//                    "sdw");
//            sMyMimeTypeMap.loadEntry("application/vnd.stardivision.writer",
//                    "vor");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.stardivision.writer-global", "sgl");
//            sMyMimeTypeMap.loadEntry("application/vnd.sun.xml.calc", "sxc");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.sun.xml.calc.template", "stc");
//            sMyMimeTypeMap.loadEntry("application/vnd.sun.xml.draw", "sxd");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.sun.xml.draw.template", "std");
//            sMyMimeTypeMap.loadEntry("application/vnd.sun.xml.impress", "sxi");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.sun.xml.impress.template", "sti");
//            sMyMimeTypeMap.loadEntry("application/vnd.sun.xml.math", "sxm");
//            sMyMimeTypeMap.loadEntry("application/vnd.sun.xml.writer", "sxw");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.sun.xml.writer.global", "sxg");
//            sMyMimeTypeMap.loadEntry(
//                    "application/vnd.sun.xml.writer.template", "stw");
//            sMyMimeTypeMap.loadEntry("application/vnd.visio", "vsd");
//            sMyMimeTypeMap.loadEntry("application/x-abiword", "abw");
//            sMyMimeTypeMap.loadEntry("application/x-apple-diskimage", "dmg");
//            sMyMimeTypeMap.loadEntry("application/x-bcpio", "bcpio");
//            sMyMimeTypeMap.loadEntry("application/x-bittorrent", "torrent");
//            sMyMimeTypeMap.loadEntry("application/x-cdf", "cdf");
//            sMyMimeTypeMap.loadEntry("application/x-cdlink", "vcd");
//            sMyMimeTypeMap.loadEntry("application/x-chess-pgn", "pgn");
//            sMyMimeTypeMap.loadEntry("application/x-cpio", "cpio");
//            sMyMimeTypeMap.loadEntry("application/x-debian-package", "deb");
//            sMyMimeTypeMap.loadEntry("application/x-debian-package", "udeb");
//            sMyMimeTypeMap.loadEntry("application/x-director", "dcr");
//            sMyMimeTypeMap.loadEntry("application/x-director", "dir");
//            sMyMimeTypeMap.loadEntry("application/x-director", "dxr");
//            sMyMimeTypeMap.loadEntry("application/x-dms", "dms");
//            sMyMimeTypeMap.loadEntry("application/x-doom", "wad");
//            sMyMimeTypeMap.loadEntry("application/x-dvi", "dvi");
//            sMyMimeTypeMap.loadEntry("application/x-flac", "flac");
//            sMyMimeTypeMap.loadEntry("application/x-font", "pfa");
//            sMyMimeTypeMap.loadEntry("application/x-font", "pfb");
//            sMyMimeTypeMap.loadEntry("application/x-font", "gsf");
//            sMyMimeTypeMap.loadEntry("application/x-font", "pcf");
//            sMyMimeTypeMap.loadEntry("application/x-font", "pcf.Z");
//            sMyMimeTypeMap.loadEntry("application/x-freemind", "mm");
//            sMyMimeTypeMap.loadEntry("application/x-futuresplash", "spl");
//            sMyMimeTypeMap.loadEntry("application/x-gnumeric", "gnumeric");
//            sMyMimeTypeMap.loadEntry("application/x-go-sgf", "sgf");
//            sMyMimeTypeMap.loadEntry("application/x-graphing-calculator", "gcf");
//            sMyMimeTypeMap.loadEntry("application/x-gtar", "gtar");
//            sMyMimeTypeMap.loadEntry("application/x-gtar", "tgz");
//            sMyMimeTypeMap.loadEntry("application/x-gtar", "taz");
//            sMyMimeTypeMap.loadEntry("application/x-hdf", "hdf");
//            sMyMimeTypeMap.loadEntry("application/x-ica", "ica");
//            sMyMimeTypeMap.loadEntry("application/x-internet-signup", "ins");
//            sMyMimeTypeMap.loadEntry("application/x-internet-signup", "isp");
//            sMyMimeTypeMap.loadEntry("application/x-iphone", "iii");
//            sMyMimeTypeMap.loadEntry("application/x-iso9660-image", "iso");
//            sMyMimeTypeMap.loadEntry("application/x-jmol", "jmz");
//            sMyMimeTypeMap.loadEntry("application/x-kchart", "chrt");
//            sMyMimeTypeMap.loadEntry("application/x-killustrator", "kil");
//            sMyMimeTypeMap.loadEntry("application/x-koan", "skp");
//            sMyMimeTypeMap.loadEntry("application/x-koan", "skd");
//            sMyMimeTypeMap.loadEntry("application/x-koan", "skt");
//            sMyMimeTypeMap.loadEntry("application/x-koan", "skm");
//            sMyMimeTypeMap.loadEntry("application/x-kpresenter", "kpr");
//            sMyMimeTypeMap.loadEntry("application/x-kpresenter", "kpt");
//            sMyMimeTypeMap.loadEntry("application/x-kspread", "ksp");
//            sMyMimeTypeMap.loadEntry("application/x-kword", "kwd");
//            sMyMimeTypeMap.loadEntry("application/x-kword", "kwt");
//            sMyMimeTypeMap.loadEntry("application/x-latex", "latex");
//            sMyMimeTypeMap.loadEntry("application/x-lha", "lha");
//            sMyMimeTypeMap.loadEntry("application/x-lzh", "lzh");
//            sMyMimeTypeMap.loadEntry("application/x-lzx", "lzx");
//            sMyMimeTypeMap.loadEntry("application/x-maker", "frm");
//            sMyMimeTypeMap.loadEntry("application/x-maker", "maker");
//            sMyMimeTypeMap.loadEntry("application/x-maker", "frame");
//            sMyMimeTypeMap.loadEntry("application/x-maker", "fb");
//            sMyMimeTypeMap.loadEntry("application/x-maker", "book");
//            sMyMimeTypeMap.loadEntry("application/x-maker", "fbdoc");
//            sMyMimeTypeMap.loadEntry("application/x-mif", "mif");
//            sMyMimeTypeMap.loadEntry("application/x-ms-wmd", "wmd");
//            sMyMimeTypeMap.loadEntry("application/x-ms-wmz", "wmz");
//            sMyMimeTypeMap.loadEntry("application/x-msi", "msi");
//            sMyMimeTypeMap.loadEntry("application/x-ns-proxy-autoconfig", "pac");
//            sMyMimeTypeMap.loadEntry("application/x-nwc", "nwc");
//            sMyMimeTypeMap.loadEntry("application/x-object", "o");
//            sMyMimeTypeMap.loadEntry("application/x-oz-application", "oza");
//            sMyMimeTypeMap.loadEntry("application/x-pkcs12", "p12");
//            sMyMimeTypeMap.loadEntry("application/x-pkcs7-certreqresp", "p7r");
//            sMyMimeTypeMap.loadEntry("application/x-pkcs7-crl", "crl");
//            sMyMimeTypeMap.loadEntry("application/x-quicktimeplayer", "qtl");
//            sMyMimeTypeMap.loadEntry("application/x-shar", "shar");
//            sMyMimeTypeMap.loadEntry("application/x-stuffit", "sit");
//            sMyMimeTypeMap.loadEntry("application/x-sv4cpio", "sv4cpio");
//            sMyMimeTypeMap.loadEntry("application/x-sv4crc", "sv4crc");
//            sMyMimeTypeMap.loadEntry("application/x-tar", "tar");
//            sMyMimeTypeMap.loadEntry("application/x-texinfo", "texinfo");
//            sMyMimeTypeMap.loadEntry("application/x-texinfo", "texi");
//            sMyMimeTypeMap.loadEntry("application/x-troff", "t");
//            sMyMimeTypeMap.loadEntry("application/x-troff", "roff");
//            sMyMimeTypeMap.loadEntry("application/x-troff-man", "man");
//            sMyMimeTypeMap.loadEntry("application/x-ustar", "ustar");
//            sMyMimeTypeMap.loadEntry("application/x-wais-source", "src");
//            sMyMimeTypeMap.loadEntry("application/x-wingz", "wz");
//            sMyMimeTypeMap.loadEntry("application/x-webarchive", "webarchive");
//            sMyMimeTypeMap.loadEntry("application/x-x509-ca-cert", "crt");
//            sMyMimeTypeMap.loadEntry("application/x-x509-user-cert", "crt");
//            sMyMimeTypeMap.loadEntry("application/x-xcf", "xcf");
//            sMyMimeTypeMap.loadEntry("application/x-xfig", "fig");
//            sMyMimeTypeMap.loadEntry("application/xhtml+xml", "xhtml");
            sMyMimeTypeMap.loadEntry("audio/basic", "snd");
            sMyMimeTypeMap.loadEntry("audio/midi", "mid");
            sMyMimeTypeMap.loadEntry("audio/midi", "midi");
            sMyMimeTypeMap.loadEntry("audio/midi", "amr");
            sMyMimeTypeMap.loadEntry("audio/midi", "kar");
            sMyMimeTypeMap.loadEntry("audio/mpeg", "mpga");
            sMyMimeTypeMap.loadEntry("audio/mpeg", "mpega");
            sMyMimeTypeMap.loadEntry("audio/mpeg", "mp2");
            sMyMimeTypeMap.loadEntry("audio/mpeg", "mp3");
            sMyMimeTypeMap.loadEntry("audio/mpeg", "m4a");
            sMyMimeTypeMap.loadEntry("audio/mpegurl", "m3u");
            sMyMimeTypeMap.loadEntry("audio/prs.sid", "sid");
            sMyMimeTypeMap.loadEntry("audio/x-aiff", "aif");
            sMyMimeTypeMap.loadEntry("audio/x-aiff", "aiff");
            sMyMimeTypeMap.loadEntry("audio/x-aiff", "aifc");
            sMyMimeTypeMap.loadEntry("audio/x-gsm", "gsm");
            sMyMimeTypeMap.loadEntry("audio/x-mpegurl", "m3u");
            sMyMimeTypeMap.loadEntry("audio/x-ms-wma", "wma");
            sMyMimeTypeMap.loadEntry("audio/x-ms-wax", "wax");
            sMyMimeTypeMap.loadEntry("audio/x-pn-realaudio", "ra");
            sMyMimeTypeMap.loadEntry("audio/x-pn-realaudio", "rm");
            sMyMimeTypeMap.loadEntry("audio/x-pn-realaudio", "ram");
            sMyMimeTypeMap.loadEntry("audio/x-realaudio", "ra");
            sMyMimeTypeMap.loadEntry("audio/x-scpls", "pls");
            sMyMimeTypeMap.loadEntry("audio/x-sd2", "sd2");
            sMyMimeTypeMap.loadEntry("audio/x-wav", "wav");
            sMyMimeTypeMap.loadEntry("image/bmp", "bmp");
            sMyMimeTypeMap.loadEntry("image/gif", "gif");
            sMyMimeTypeMap.loadEntry("image/ico", "cur");
            sMyMimeTypeMap.loadEntry("image/ico", "ico");
            sMyMimeTypeMap.loadEntry("image/ief", "ief");
            sMyMimeTypeMap.loadEntry("image/jpeg", "jpeg");
            sMyMimeTypeMap.loadEntry("image/jpeg", "jpg");
            sMyMimeTypeMap.loadEntry("image/jpeg", "jpe");
            sMyMimeTypeMap.loadEntry("image/pcx", "pcx");
            sMyMimeTypeMap.loadEntry("image/png", "png");
            sMyMimeTypeMap.loadEntry("image/svg+xml", "svg");
            sMyMimeTypeMap.loadEntry("image/svg+xml", "svgz");
            sMyMimeTypeMap.loadEntry("image/tiff", "tiff");
            sMyMimeTypeMap.loadEntry("image/tiff", "tif");
            sMyMimeTypeMap.loadEntry("image/vnd.djvu", "djvu");
            sMyMimeTypeMap.loadEntry("image/vnd.djvu", "djv");
            sMyMimeTypeMap.loadEntry("image/vnd.wap.wbmp", "wbmp");
            sMyMimeTypeMap.loadEntry("image/x-cmu-raster", "ras");
            sMyMimeTypeMap.loadEntry("image/x-coreldraw", "cdr");
            sMyMimeTypeMap.loadEntry("image/x-coreldrawpattern", "pat");
            sMyMimeTypeMap.loadEntry("image/x-coreldrawtemplate", "cdt");
            sMyMimeTypeMap.loadEntry("image/x-corelphotopaint", "cpt");
            sMyMimeTypeMap.loadEntry("image/x-icon", "ico");
            sMyMimeTypeMap.loadEntry("image/x-jg", "art");
            sMyMimeTypeMap.loadEntry("image/x-jng", "jng");
            sMyMimeTypeMap.loadEntry("image/x-ms-bmp", "bmp");
            sMyMimeTypeMap.loadEntry("image/x-photoshop", "psd");
            sMyMimeTypeMap.loadEntry("image/x-portable-anymap", "pnm");
            sMyMimeTypeMap.loadEntry("image/x-portable-bitmap", "pbm");
            sMyMimeTypeMap.loadEntry("image/x-portable-graymap", "pgm");
            sMyMimeTypeMap.loadEntry("image/x-portable-pixmap", "ppm");
            sMyMimeTypeMap.loadEntry("image/x-rgb", "rgb");
            sMyMimeTypeMap.loadEntry("image/x-xbitmap", "xbm");
            sMyMimeTypeMap.loadEntry("image/x-xpixmap", "xpm");
            sMyMimeTypeMap.loadEntry("image/x-xwindowdump", "xwd");
            sMyMimeTypeMap.loadEntry("model/iges", "igs");
            sMyMimeTypeMap.loadEntry("model/iges", "iges");
            sMyMimeTypeMap.loadEntry("model/mesh", "msh");
            sMyMimeTypeMap.loadEntry("model/mesh", "mesh");
            sMyMimeTypeMap.loadEntry("model/mesh", "silo");
            sMyMimeTypeMap.loadEntry("text/calendar", "ics");
            sMyMimeTypeMap.loadEntry("text/calendar", "icz");
            sMyMimeTypeMap.loadEntry("text/comma-separated-values", "csv");
            sMyMimeTypeMap.loadEntry("text/css", "css");
            sMyMimeTypeMap.loadEntry("text/h323", "323");
            sMyMimeTypeMap.loadEntry("text/iuls", "uls");
            sMyMimeTypeMap.loadEntry("text/mathml", "mml");
            // add it first so it will be the default for ExtensionFromMimeType
            //sMyMimeTypeMap.loadEntry("text/plain", "txt");
            sMyMimeTypeMap.loadEntry("text/plain", "asc");
            sMyMimeTypeMap.loadEntry("text/plain", "text");
            sMyMimeTypeMap.loadEntry("text/plain", "diff");
            sMyMimeTypeMap.loadEntry("text/plain", "po");     // reserve "pot" for vnd.ms-powerpoint
            sMyMimeTypeMap.loadEntry("text/richtext", "rtx");
            sMyMimeTypeMap.loadEntry("text/rtf", "rtf");
            sMyMimeTypeMap.loadEntry("text/texmacs", "ts");
            sMyMimeTypeMap.loadEntry("text/text", "phps");
            sMyMimeTypeMap.loadEntry("text/tab-separated-values", "tsv");
            // sMyMimeTypeMap.loadEntry("text/xml", "xml");
            sMyMimeTypeMap.loadEntry("text/x-bibtex", "bib");
            sMyMimeTypeMap.loadEntry("text/x-boo", "boo");
            sMyMimeTypeMap.loadEntry("text/x-c++hdr", "h++");
            sMyMimeTypeMap.loadEntry("text/x-c++hdr", "hpp");
            sMyMimeTypeMap.loadEntry("text/x-c++hdr", "hxx");
            sMyMimeTypeMap.loadEntry("text/x-c++hdr", "hh");
            sMyMimeTypeMap.loadEntry("text/x-c++src", "c++");
            sMyMimeTypeMap.loadEntry("text/x-c++src", "cpp");
            sMyMimeTypeMap.loadEntry("text/x-c++src", "cxx");
            sMyMimeTypeMap.loadEntry("text/x-chdr", "h");
            sMyMimeTypeMap.loadEntry("text/x-component", "htc");
            sMyMimeTypeMap.loadEntry("text/x-csh", "csh");
            sMyMimeTypeMap.loadEntry("text/x-csrc", "c");
            sMyMimeTypeMap.loadEntry("text/x-dsrc", "d");
            sMyMimeTypeMap.loadEntry("text/x-haskell", "hs");
            sMyMimeTypeMap.loadEntry("text/x-java", "java");
            sMyMimeTypeMap.loadEntry("text/x-literate-haskell", "lhs");
            sMyMimeTypeMap.loadEntry("text/x-moc", "moc");
            sMyMimeTypeMap.loadEntry("text/x-pascal", "p");
            sMyMimeTypeMap.loadEntry("text/x-pascal", "pas");
            sMyMimeTypeMap.loadEntry("text/x-pcs-gcd", "gcd");
            sMyMimeTypeMap.loadEntry("text/x-setext", "etx");
            sMyMimeTypeMap.loadEntry("text/x-tcl", "tcl");
            sMyMimeTypeMap.loadEntry("text/x-tex", "tex");
            sMyMimeTypeMap.loadEntry("text/x-tex", "ltx");
            sMyMimeTypeMap.loadEntry("text/x-tex", "sty");
            sMyMimeTypeMap.loadEntry("text/x-tex", "cls");
            sMyMimeTypeMap.loadEntry("text/x-vcalendar", "vcs");
            //sMyMimeTypeMap.loadEntry("text/x-vcard", "vcf");
            sMyMimeTypeMap.loadEntry("video/3gpp", "3gp");
            sMyMimeTypeMap.loadEntry("video/3gpp", "3g2");
            sMyMimeTypeMap.loadEntry("video/dl", "dl");
            sMyMimeTypeMap.loadEntry("video/dv", "dif");
            sMyMimeTypeMap.loadEntry("video/dv", "dv");
            sMyMimeTypeMap.loadEntry("video/fli", "fli");
            sMyMimeTypeMap.loadEntry("video/mpeg", "mpeg");
            sMyMimeTypeMap.loadEntry("video/mpeg", "mpg");
            sMyMimeTypeMap.loadEntry("video/mpeg", "mpe");
            sMyMimeTypeMap.loadEntry("video/mp4", "mp4");
            sMyMimeTypeMap.loadEntry("video/mpeg", "VOB");
            sMyMimeTypeMap.loadEntry("video/quicktime", "qt");
            sMyMimeTypeMap.loadEntry("video/quicktime", "mov");
            sMyMimeTypeMap.loadEntry("video/vnd.mpegurl", "mxu");
            sMyMimeTypeMap.loadEntry("video/x-la-asf", "lsf");
            sMyMimeTypeMap.loadEntry("video/x-la-asf", "lsx");
            sMyMimeTypeMap.loadEntry("video/x-mng", "mng");
            sMyMimeTypeMap.loadEntry("video/x-ms-asf", "asf");
            sMyMimeTypeMap.loadEntry("video/x-ms-asf", "asx");
            sMyMimeTypeMap.loadEntry("video/x-ms-wm", "wm");
            sMyMimeTypeMap.loadEntry("video/x-ms-wmv", "wmv");
            sMyMimeTypeMap.loadEntry("video/x-ms-wmx", "wmx");
            sMyMimeTypeMap.loadEntry("video/x-ms-wvx", "wvx");
            sMyMimeTypeMap.loadEntry("video/x-msvideo", "avi");
            sMyMimeTypeMap.loadEntry("video/x-sgi-movie", "movie");
            sMyMimeTypeMap.loadEntry("x-conference/x-cooltalk", "ice");
            sMyMimeTypeMap.loadEntry("x-epoc/x-sisx-app", "sisx");
        }

        return sMyMimeTypeMap;
    }
}

