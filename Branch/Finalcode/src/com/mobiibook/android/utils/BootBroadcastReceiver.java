/**
 * BootBroadcastReceiver.java
 * @author Nitin Singh
 */
package com.mobiibook.android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * Class responsible for starting the ResetOnRebootService when the device gets rebooted
 * @author Nitin Singh
 *
 */
public class BootBroadcastReceiver extends BroadcastReceiver {

	public static final String TAG = "BootBroadcastReceiver";

	/**
	 * The function called when the device is restarted 
	 * This calls the ResetOnRebootService
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		
//		// just make sure we are getting the right intent (better safe than sorry)
//		if( "android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {

			Intent serviceIntent = new Intent();
			serviceIntent.setAction("com.blisstering.mobiibook.service.ResetOnRebootService");
			context.startService(serviceIntent);
//		} 

//		else {
//			Log.e(TAG, "Received unexpected intent " + intent.toString());   
//		}
	}
}