/**
 * MessagesXMLHandler.java
 * @author Nitin Singh
 */
package com.mobiibook.android.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;

/**
 *  Helper class for parsing the messages XML file during restore
 * @author Nitin Singh
 *
 */
public class MessagesXMLHandler extends DefaultHandler{

	/**
	 * Object reference for the current application context
	 */
	Context mcontext;

	/**
	 * Object for storing an event
	 */
	ContentValues values;

	/**
	 * the default tag used while parsing
	 */
	String temp="defaulttag";

	/**
	 * The string constant containing the path for SMS content handler
	 */
	private static final String messagesProviderPath="content://sms";

	/**
	 * The string constant containing the URI for SMS content handler
	 */
	Uri messagesUri;

	
	
	/**
	 * constructor 
	 * @param context Object refernce for current application context
	 */
	public MessagesXMLHandler(Context context){

		mcontext=context;
		messagesUri=Uri.parse(messagesProviderPath);

		System.out.println(">>>>starting the insertion process?>>>>>>>>");

		//		ContentValues cv=new ContentValues();
		//		cv.put("body","body");
		//		cv.put("status",64);
		//		cv.put("address","123456789");
		//		cv.put("read",1);
		//		cv.put("locked",0);
		//		cv.put("type",2);
		//		cv.put("date",Long.parseLong("1299489579510"));
		//		cv.put("thread_id",0);



		//		Long id=new Long(0);
		//
		//		Cursor cursor = context.getContentResolver().query(
		//				Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,"123456789"),
		//				new String[] { ContactsContract.PhoneLookup._ID }, null, null, null);
		//		if (cursor != null) {
		//			try {
		//				if (cursor.getCount() > 0) {
		//					cursor.moveToFirst();
		//					id = Long.valueOf(cursor.getLong(0));
		//
		//					System.out.println("id is ?>>>>>>>>>>>"+id);
		//				}
		//			} finally {
		//				cursor.close();
		//			}
		//		}

		//		cv.put("_id", 4);
		//		System.out.println("trying to insert into the db>>>>>>>");
		//		mcontext.getContentResolver().insert(messagesUri,cv);
		//		
		//		cv.put("body","body1");
		//		cv.put("status",-1);
		//		cv.put("address","1234567890");
		//		cv.put("read",1);
		//		cv.put("locked",0);
		//		cv.put("type",2);
		//		cv.put("date",Long.parseLong("1299489579510"));
		//		cv.put("thread_id",0);
		//		
		//		cv.put("_id", 0);
		//		System.out.println("trying to insert into the db>>>>>>>");
		//		mcontext.getContentResolver().insert(messagesUri,cv);
		//		
		//		cv.put("body","body2");
		//		cv.put("status",-1);
		//		cv.put("address","1234567890");
		//		cv.put("read",1);
		//		cv.put("locked",0);
		//		cv.put("type",2);
		//		cv.put("date",Long.parseLong("1299489579510"));
		//		cv.put("thread_id",0);
		//		
		//		cv.put("_id", 23);
		//		System.out.println("trying to insert into the db>>>>>>>");
		//		mcontext.getContentResolver().insert(messagesUri,cv);
		//		System.out.println("done inserting into the db>>>>>>");

	}



	//the thread_id for all the messages should be kept as 0 or the insertion wont work properly only some entries will be getting inserted into the db and the thread order would be random
	/** Called when tag starts ( ex:- <name>AndroidPeople</name>
	 * -- <name> )*/
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException {

		if(!localName.equalsIgnoreCase("messages")){
			if(localName.equalsIgnoreCase("entry")){
				values=new ContentValues();
			}

			else if(localName.trim().length()!=0){
				temp=localName;
			}
		}
	}



	/** Called when tag closing ( ex:- <name>AndroidPeople</name>
	 * -- </name> )*/
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if(!localName.equalsIgnoreCase("messages")){

			if(localName.equalsIgnoreCase("entry")){
				try{
					values.put("thread_id",0);
					mcontext.getContentResolver().insert(messagesUri,values);
				}

				catch(SQLiteConstraintException e){
					e.printStackTrace();
				}
			}

			else if(localName.trim().length()!=0){
				temp="defaulttag";
			}
		}
	}



	/** Called to get tag characters ( ex:- <name>AndroidPeople</name>
	 * -- to get AndroidPeople Character ) */
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		String value="";

		if(!temp.equalsIgnoreCase("defaulttag") || !temp.equalsIgnoreCase("thread_id")){
			try{
				value=new String(ch,start,length);

				if(!temp.equalsIgnoreCase("address")){
					if(value.length()<=10){
						int tempint=Integer.parseInt( value);
						values.put(temp,tempint );
						System.out.println(temp+">>>>int >>>>>>>>"+value+">>>>"+tempint);
					}

					else {
						long templong=Long.parseLong(value);
						values.put(temp,templong );

						System.out.println(temp+">>>>long>>>>>>>>"+value+">>>>>>>"+templong);
					}
				}

				else {
					values.put(temp,value);
					System.out.println(temp+">>>>>>addstring >>>>>>>"+value);
				}

			}
			catch(NumberFormatException e){
				values.put(temp,value);
				System.out.println(temp+">>>>>>string >>>>>>>"+value);

			}
		}

	}

}