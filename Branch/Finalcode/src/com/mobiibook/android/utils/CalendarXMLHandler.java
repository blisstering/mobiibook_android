/**
 * CalendarXMLHandler.java
 * @author Nitin Singh
 */
package com.mobiibook.android.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;

/**
 * Helper class for parsing the calendar XML file during restore
 * @author Nitin Singh
 *
 */
public class CalendarXMLHandler extends DefaultHandler {

	//String eventable="originalEvent,visibility,rrule,hasAlarm,transparency,rdate,dtstart,hasAttendeeData,commentsUri,description,htmlUri,hasExtendedProperties,eventLocation,dtend,allDay,organizer,originalInstanceTime,selfAttendeeStatus,eventTimezone,lastDate,guestsCanModify,exrule,guestsCanSeeGuests,title,_id,calendar_id,originalAllDay,duration,guestsCanInviteOthers,exdate,eventStatus";

	/**
	 * The comma sepearted list of columns that will be put back in the calendar
	 */
	String eventable="visibility,hasAlarm,transparency,dtstart,description,eventLocation,dtend,allDay,title,eventStatus,rrule,rdate,exrule,exdate";
	
	/**
	 * The default tag used while parsing
	 */
	String temp="defaulttag";

	/**
	 * storing the rrule for reccuring calendar events
	 */
	String rrule="";

	/**
	 * Set when a duplicate event is found
	 */
	boolean repeateventflag=false;

	/**
	 * Object for storing an event
	 */
	ContentValues event;

	/**
	 * Object reference for the current application context
	 */
	Context mcontext;

	/**
	 * The URI for the Calendar content handler
	 */
	Uri eventUri;

	
	
	/**
	 * constructor 
	 * @param context current application context
	 */
	public CalendarXMLHandler(Context context){
		mcontext=context;

		//System.out.println("inside handleer>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		if (android.os.Build.VERSION.SDK_INT <= 7 )
		{
			//the old way
			eventUri = Uri.parse("content://calendar/events"); 
		}
		else
		{
			//the new way
			eventUri = Uri.parse("content://com.android.calendar/events");
		} 

		//		ContentValues event = new ContentValues();
		//
		//		event.put("calendar_id", 1);
		//		event.put("title", "Today's Event [TEST]");
		//		event.put("description", "8 Hour Presentation");
		//		event.put("eventLocation", "Online");
		//
		//		long time=System.currentTimeMillis();
		//
		//		long startTime = time + 1000 * 60 * 60;
		//		long endTime = time + 1000 * 60 * 60 * 2;
		//
		//		event.put("dtstart",startTime);
		//		System.out.println("start"+startTime);
		//		event.put("dtend", endTime);
		//
		//		event.put("allDay", 0); // 0 for false, 1 for true
		//		event.put("eventStatus", 1);
		//		event.put("visibility", 0);
		//		event.put("transparency", 0);
		//		event.put("hasAlarm", 0); // 0 for false, 1 for true
		//
		//		Uri eventsUri = Uri.parse("content://calendar/events");
		//
		//		mcontext.getContentResolver().insert(eventsUri, event);


		//first delete all the entries in the current table of events
		deleteRowsFromCurrentDB(mcontext);
	}


	/**
	 * function that deletes all the rows from the events DB
	 * @param context current application context
	 */
	public void deleteRowsFromCurrentDB(Context context){
		System.out.println(">>>>>>inside the clear current db function");

		Cursor cursor=context.getContentResolver().query(eventUri, new String[] {"_id"}, null,null,null);

		try{
			while(cursor.moveToNext()){
				System.out.println(">>>>"+cursor.getString(0));

				context.getContentResolver().delete(Uri.withAppendedPath(eventUri,cursor.getString(0)), null, null);
			}

			cursor.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/** Called when tag starts ( ex:- <name>AndroidPeople</name>
	 * -- <name> )*/
	@Override
	public void startElement(String uri, String localName, String qName,Attributes attributes) throws SAXException {

		if(localName.equalsIgnoreCase("entry")){
			event = new ContentValues();
		}

		else if(! localName.equalsIgnoreCase("calendar") && ! localName.equalsIgnoreCase("end")){

			if(eventable.contains(localName))
				temp=localName;
		}

		//		System.out.println(localName);

	}


	/** Called when tag closing ( ex:- <name>AndroidPeople</name>
	 * -- </name> )*/
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if(!localName.equalsIgnoreCase("entry")&&!localName.equalsIgnoreCase("calendar") && ! localName.equalsIgnoreCase("end") && !localName.equalsIgnoreCase("_id")){
			temp="defaulttag";
		}

		if(localName.equalsIgnoreCase("entry")){

			try{
				if(!repeateventflag){
					event.put("calendar_id",1);
					mcontext.getContentResolver().insert(eventUri, event);
				}
				repeateventflag=false;
			}

			catch(SQLiteConstraintException e){
				e.printStackTrace();
			}
		}

		//		System.out.println(localName);
	}


	/** Called to get tag characters ( ex:- <name>AndroidPeople</name>
	 * -- to get AndroidPeople Character ) */
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		String value="";

		if(!temp.equalsIgnoreCase("defaulttag")){
			try{

				value= new String(ch, start, length);

				if(value.length()<=10){
					int tempint=Integer.parseInt( new String(ch, start, length));
					event.put(temp,tempint );
					System.out.println(temp+">>>>int >>>>>>>>"+value+">>>>"+tempint);
				}

				else{
					long templong=Long.parseLong(value);
					event.put(temp,templong );
					System.out.println(temp+">>>>long>>>>>>>>"+value+">>>>>>>"+templong);
				}
			}

			catch(NumberFormatException e){

				//this is to make sure there is no redundancy due to recurring events
				if(temp.equalsIgnoreCase("rrule")){
					if(rrule.equals(new String(ch,start,length))){
						repeateventflag=true;
					}
					
					else{
						rrule=new String(ch,start,length);
						repeateventflag=false;
					}
				}

				event.put(temp,new String(ch, start, length));
				System.out.println(temp+">>>>>>string >>>>>>>"+new String(ch,start,length));
			}
		}

		//		System.out.println(new String(ch,start,length));
	}
}