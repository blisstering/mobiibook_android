package com.mobiibook.android.aidl;

/**
*Interface for RestoreService.java
*/
interface RestoreService {

/**
*Function called when ongoing restore is to be cancelled
*/
  boolean cancelRestore();
  
/**
*Function called when a restore is to be resumed
*/
  void resumeRestore();
  
/**
*Function called to send updates about current restore to Retrieve progress screen
*/
  void sendUpdates();
}
