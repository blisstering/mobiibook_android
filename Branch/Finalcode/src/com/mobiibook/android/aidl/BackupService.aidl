package com.mobiibook.android.aidl;

/**
*Interface for BackupService.java
*/

interface BackupService {

/**
*Function called when currently running backup is to be cancelled
*/
  boolean cancelBackup();
  
  
/**
*Function called when backup needs to be resumed
*/
  void resumeBackup();
  
  
/**
*Function called to send updates about current backup to Backup progress screen
*/
  void sendUpdates();
   
}


