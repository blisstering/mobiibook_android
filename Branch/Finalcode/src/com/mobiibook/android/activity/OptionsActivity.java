/**
 * OptionsActivity.java
 * @author Nitin Singh
 */
package com.mobiibook.android.activity;

import java.net.URLEncoder;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.service.BackupService;
import com.mobiibook.android.service.RestoreService;
import com.mobiibook.android.utils.Session;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


/**
 * Class responsible for handling the events generated from the bottom bar
 * @author Prateek Jain
 *
 */
public class OptionsActivity extends Activity{

	/**
	 * Reference to the Menu.FIRST constant 
	 */
	private static final int MENU_LOGOUT = Menu.FIRST;

	/**
	 * Object to handle the Progress Dialog shown on the screen
	 */
	ProgressDialog progressDialog;

	/**
	 * Response returned by the user.logout WS
	 */
	String response = "";

	/**
	 * Flag to be set depending on whether or not the stored session id in Session.java is null nor not , if null the appliacation is deemed as crashed and the user needs to login again to establish a valid session 
	 */
	boolean crashflag=false;

	/**
	 * Flag indicating whether the call to user.logout was successful or not
	 */
	boolean logoutSuccessful = false;



	/**
	 * Function responsible for adding options to the Menu list
	 * @param menu 
	 * Object Reference for the default menu object to manipulate Menu button behaviour and look
	 * @return
	 * true/false based on whether the addition was successful or not
	 */
	public boolean onCreateOptionsMenu(Menu menu) {    
		menu.add(0, MENU_LOGOUT, 0, "Logout");    
		//menu.add(0, MENU_QUIT, 0, "Quit");    
		return true;
	}




	/**
	 * Function responsible for handling selection events on the menu
	 * @param item 
	 * Object Reference for the default MenuItem object to manipulate Menu button behaviour
	 * @return true/false based on whether the selection and the subsequent operation was successful or not
	 */
	public boolean onOptionsItemSelected(MenuItem item) {    
		switch (item.getItemId()) {    
		case MENU_LOGOUT:        
			logout();        
			return true;    
		}    
		return false;
	}



	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog.dismiss();

			if(!logoutSuccessful && crashflag){
				AlertDialog alert;

				AlertDialog.Builder builder = new AlertDialog.Builder(OptionsActivity.this);
				builder.setMessage(getText(R.string.oncrashmessage))
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent explicitIntent= new Intent(OptionsActivity.this,login.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(explicitIntent);
					}
				});
				alert = builder.create();
				alert.setTitle("Notification");
				alert.show();
			}

			else if(!logoutSuccessful)		
				showResponse(response);

			else
			{	
				Intent explicitIntent = new Intent(OptionsActivity.this,login.class);
				explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(explicitIntent);
			}

		}
	};



	/**
	 * Show alert dialog box on the screen
	 * @param response  
	 * The message to be displayed on the screen in the dialog
	 * @author Prateek
	 */
	public void showResponse(final String response)
	{
		 

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}



	/**
	 * Function called to logout from the application
	 * It checks if backup / resore is running or not
	 * @author Prateek
	 */
	public void logout()
	{
		String msg;

		if(Session.isBackupServiceRunning())
		{
			msg = getText(R.string.logoutmessagewithbackup).toString();
			//showDialog = true;
		}
		else if(Session.isRestoreServiceRunning())
		{
			msg = getText(R.string.logoutmessagewithrestore).toString();
			//showDialog = true;
		}
		else
			msg = getText(R.string.exitmessage).toString();

		//		if(showDialog)
		//		{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(msg)
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.finish();
				//set the loggedoutflag to true
				Session.setLoggedOut(true);

				System.out.println("logged out flag during exit"+Session.isLoggedOut());


				if(Session.isBackupServiceRunning())
				{
					Intent i = new Intent(OptionsActivity.this,BackupService.class);
					stopService( i );
				}
				else if(Session.isRestoreServiceRunning())
				{
					Intent i = new Intent(OptionsActivity.this,RestoreService.class);
					stopService( i );	
				}
				logoutWS();
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		//}
		//		else
		//			logoutWS();
	}


	/**
	 * user.logout WS call
	 * @author Prateek
	 */
	public void logoutWS()
	{
		crashflag=false;

		progressDialog = ProgressDialog.show(this, "", "Please wait... Logging out");
		new Thread() {	
			public void run() {
				try {
					if(Session.getSessionId()!=null){
						String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.logout", "UTF-8");


						data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");

						WebServiceCall wsCall = new WebServiceCall(data);
						System.out.println("Calling web service");
						//wsCall.start();
						//System.out.println("Waiting for thread to die");
						//wsCall.join();
						//WebServiceCall castedThread = (WebServiceCall) wsCall;
						boolean threadStatus = wsCall.isThreadSucceeded();
						if (threadStatus) {
							System.out.println("Success");
							System.out.println("The response " + wsCall.getExecutionResponse());
							System.out.println("There is no error");
							logoutSuccessful = true;
							Session.setSessionId(null);
						} else {
							System.out.println("The value is " + getText(R.string.network_message).toString());
							response = getText(R.string.network_message).toString();
							Session.setSessionId(null);


						}
					}

					else
						crashflag=true;
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}



	/**
	 * Function called when home icon is clicked on the bottom bar
	 * Redirect to the home screen
	 * @param view
	 * view object gives all the layout elements of the screen.
	 * @author Prateek
	 */
	public void onHomeIconClick(View view)
	{
		System.out.println("Home Icon Clicked");
		Intent explicitIntent = new Intent(OptionsActivity.this,HomeScreenActivity.class);
		explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(explicitIntent);
	}



	/**
	 * Function called when backup icon is clicked on the bottom bar
	 * Backup process is initiated.
	 * If backup process is already running redirect to existing backup now screen
	 * else if restore is running backup cannot be started
	 * else start the backup process
	 * @param view 
	 * view object gives all the layout elements of the screen.
	 * @author Prateek
	 */
	public void onBackupIconClick(View view)
	{
		System.out.println("Backup Icon Clicked");
		//		if(Session.isBackupServiceRunning())
		//		{
		//			Intent explicitIntent;
		//			explicitIntent = new Intent(OptionsActivity.this,BackupServiceActivity.class);
		//			startActivity(explicitIntent);
		//		}
		//		else if(Session.isRestoreServiceRunning())
		//		{
		//			showResponse(getText(R.string.restoreinprogress).toString());
		//		}
		//		else
		//		{
		//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
		//			builder.setMessage("Are you sure you want to start backup?")
		//			.setCancelable(false)
		//			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		//				public void onClick(DialogInterface dialog, int id) {
		//					//MyActivity.this.finish();
		//					Intent explicitIntent;
		//					explicitIntent = new Intent(OptionsActivity.this,StartBackupActivity.class);
		//					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		//					startActivity(explicitIntent);
		//				}
		//			})
		//			.setNegativeButton("No", new DialogInterface.OnClickListener() {
		//				public void onClick(DialogInterface dialog, int id) {
		//					dialog.cancel();
		//				}
		//			});
		//			AlertDialog alert = builder.create();
		//			alert.show();

		Intent explicitIntent=new Intent(OptionsActivity.this,BackupTypeSelectionActivity.class);
		startActivity(explicitIntent);
		//		}
	}



	/**
	 * Function called when restore icon is clicked on the bottom bar
	 * Restore process is initiated.
	 * If restore process is already running redirect to existing restore now screen
	 * else if backup is running restore cannot be started
	 * else start the restore process
	 * @param view
	 * view object gives all the layout elements of the screen
	 * @author Prateek
	 */
	public void onRestoreIconClick(View view)
	{
		System.out.println("Restore Icon Clicked");
		if(Session.isRestoreServiceRunning())
		{
			Intent explicitIntent;
			explicitIntent = new Intent(OptionsActivity.this,RestoreServiceActivity.class);
			startActivity(explicitIntent);
		}
		else if(Session.isBackupServiceRunning())
		{
			showResponse(getText(R.string.backupinprogress).toString());
		}
		else
		{
			Intent explicitIntent = new Intent(OptionsActivity.this,RestoreActivity.class);
			explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(explicitIntent);
		}
	}



	/**
	 * Function called when share icon is clicked on the bottom bar
	 * Redirect to share activity screen.
	 * @param view
	 * view object gives all layout elements shown on the screen
	 * @author Prateek
	 */
	public void onShareIconClick(View view)
	{
		Intent explicitIntent = new Intent(OptionsActivity.this,ShareActivity.class);
		startActivity(explicitIntent);
	}



	/**
	 * Checks if the external storage is available or not
	 * @return true if external storage is available else false  
	 * @author Prateek
	 */
	public boolean isexternalStorageAvailable()
	{
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		if(mExternalStorageAvailable && mExternalStorageWriteable)
			return true;
		else
			return false;
	}

}
