/**
 * ShareActivity.java
 * @author Prateek Jain
 */
package com.mobiibook.android.activity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.json.JSONObject;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.MyMimeTypeMap;
import com.mobiibook.android.utils.Session;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Class responsible for handling the screen shown when the user selects Share from the homescreen or the bottom bar
 * @author Prateek Jain
 *
 */
public class ShareActivity extends OptionsActivity{

	/**
	 * Object responsible for showing the list of data types on the screen namely "Music" , "Picutures" and "Video Clips"
	 */
	private ListView lv1;

	/**
	 * Response from the file.check WS
	 */
	String response="";


	/**
	 * The MIME type of the file that the user has selected for sharing
	 */
	String MIMEType="";

	/** Called when the activity is first created. 
	 * @author Prateek*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setListAdapter(new ArrayAdapter(this, R.layout.homescreenactivity, WORLDCUP2010));
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Select Content to Share");
		}
		lv1=(ListView)findViewById(R.id.ListView01);
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		lv1.setCacheColorHint(00000000);
		lv1.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {
				// When clicked, show a toast with the TextView text
				Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
						Toast.LENGTH_SHORT).show();
				System.out.println("The position is " + position);
				System.out.println("The id is " + id);
				switch(position)
				{
				case 0:
					/* To open the image gallery */
					openIntent("image/*");
					break;
				case 1:
					/* To open the music gallery */
					openIntent("audio/*");
					break;
				case 2:
					/* To open the video gallery */
					openIntent("video/*");
					break;
				default:
					break;
				}
			}
		});
	}



	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response)
	{

		System.out.println("reached here");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}



	/**
	 * List items to be shown on the screen
	 * @author Prateek
	 */
	static final String[] LISTITEM = new String[] {
		"1.Pictures",  "2.Music", "3.Video Clips" 
	};



	/**
	 * Opens the intent for selecting image,  video or music file
	 * @param String type
	 * Type of file which is to be selected (image, video or music)
	 * @author Prateek
	 */
	public void openIntent(String type)
	{
		MIMEType=type.substring(0, type.indexOf("/"));

		System.out.println(">>>>>>>>>>>>>>external memory is<<<<<<<<<"+Environment.getExternalStorageDirectory().toString());

		Intent intent = new Intent();
		intent.setType(type);  
		intent.setAction(Intent.ACTION_GET_CONTENT); 
		//intent.setAction(Intent.CATEGORY_DEFAULT);
		//intent.setAction(Intent.ACTION_PICK);
		/* Starts the activity for result and return back after selecting the image. Calls onActivityResult function */
		//startActivityForResult(Intent.createChooser(intent, "Select"),1);
		startActivityForResult(intent,1);
		//startActivityForResult(intent, 1);

	}



	/**
	 * Called after returning from activityForResult
	 * @param int requestCode
	 * Code of the request for the intent
	 * @param int resultCode
	 * Result code of the response
	 * @param Intent data
	 * Data which comes from the activity
	 * @author Prateek
	 */
	@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data) {   

		response="";
		String filerealpath=null;

		if (resultCode == RESULT_OK) {  

			if (requestCode == 1) {  

				// currImageURI is the global variable I'm using to hold the content:// URI of the image  
				Uri currentURI = data.getData(); 

				filerealpath=getRealPathFromURI(currentURI);

				System.out.println(">>>>>>>>>the uri is<<<<<<<<"+currentURI);
				System.out.println(">>>>>>>>>finally THE real path<<<<<<<<" + filerealpath);



				try
				{
					if(filerealpath==null){

						String URIString=currentURI.toString();

						String SDRoot="/sdcard/";

						filerealpath=Environment.getExternalStorageDirectory().toString()+"/"+URIString.substring(URIString.indexOf(SDRoot,0)+SDRoot.length(),URIString.length());					
					}


					try {
						filerealpath=URLDecoder.decode(filerealpath, "UTF-8");

					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}


					System.out.println("finally the uri is"+currentURI);
					System.out.println("finally THE real path" + filerealpath);

					System.out.println("mime type:"+MIMEType);

					String mimename=MyMimeTypeMap.getSingleton().getMimeTypeFromExtension(filerealpath.substring((filerealpath.lastIndexOf("."))+1));
					System.out.println("file mime type"+mimename);

					//check to make sure that the file selected by the use is of the same type as the type of content he wishes to share
					if(mimename!=null&&MIMEType.equalsIgnoreCase(mimename.substring(0,mimename.indexOf("/"))))	
					{
						//in case the application could not read the file path of the file selected by the user show a notification saying the same

						//call file.check WS
						fileCheckWS(filerealpath);
					}

					else
						response="Wrong file type selected. Please select a "+MIMEType+" file";
				}
				catch(Exception e){
					response="Unable to read the selected file";
				}
			}

			//shareWS(getRealPathFromURI(currImageURI));
			Intent explicitIntent = new Intent(ShareActivity.this,FriendListActivity.class);
			explicitIntent.putExtra("path", filerealpath);
			explicitIntent.putExtra("filecheckresponse", response);
			explicitIntent.putExtra("previouscrashflag",crashflag);
			startActivity(explicitIntent);

		}  
	}  



	/**
	 * WS call to file.check
	 * @param path path of the file chosen for restore
	 */
	public void fileCheckWS(final String path){

		crashflag=false;

		String finalpath=null;

		File file=null;

		try {
			finalpath=URLDecoder.decode(path, "UTF-8");

			file = new File(finalpath);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("path is<<<< <<<<<<<<<<"+finalpath+">>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println("absolute path is<<<<<<<<<<<<<<<<<<"+file.getAbsolutePath()+">>>>>>>>>>>>>>>>>>>>>>>");



		//File file = new File("data/data/com.android.createFileTest/test.txt");
		if (!file.exists()) {
			response="File does not exist";
		}
		else
		{
			try {
				if(Session.getSessionId()!=null){
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("file.check", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					data += "&" + URLEncoder.encode("file_name", "UTF-8")+"="+URLEncoder.encode(file.getName(), "UTF-8");
					String length = Long.toString(file.length());
					data += "&" + URLEncoder.encode("file_size", "UTF-8")+"="+URLEncoder.encode(length, "UTF-8");
					data += "&" + URLEncoder.encode("file_path", "UTF-8")+"="+URLEncoder.encode(file.getAbsolutePath(), "UTF-8");		
					data += "&" + URLEncoder.encode("file_last_modified_time", "UTF-8")+"="+URLEncoder.encode(Long.toString(file.lastModified()), "UTF-8");

					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Starting Thread");
					System.out.println("Waiting for thread to die");
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							System.out.println("Error in sharingentrypoint");
							if(obj.getString("#message").equalsIgnoreCase("access denied")){
								crashflag=true;
								Session.setSessionId(null);
								response=getText(R.string.oncrashmessage).toString();
							}
						}
						else
						{

							int responsecode=obj.getInt("response_code");
							if(responsecode==0)
								response=" ";
							else 
								response=obj.getString("response_message");

						}

					} else {
						System.out.println("Failure");
					}
				}

				else{
					crashflag=true;
					response=getText(R.string.oncrashmessage).toString();
				}
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}


	/**
	 * Returns the real path of the file (sdcard path)
	 * @param Uri contentUri
	 * Unique Uri of the file
	 * @return String
	 * Real path of the file
	 * @author Prateek
	 */
	public String getRealPathFromURI(Uri contentUri) {  

		// can post image  

		try{
			String [] proj={MediaStore.Images.Media.DATA};
			Cursor cursor = managedQuery( contentUri,  
					proj, // Which columns to return  
					null,       // WHERE clause; which rows to return (all rows)  
					null,       // WHERE clause selection arguments (none)  
					null); // Order-by clause (ascending by name)  
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);  
			cursor.moveToFirst();  
			return cursor.getString(column_index);  
		}

		catch(Exception e)
		{
			return null;
		}
	}
}
