/**
 * ChangePasswordActivity.java
 * @author Nitin Singh
 */
package com.mobiibook.android.activity;

import java.net.URLEncoder;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.Session;

/**
 * Class responsible for handling the Change Password Screen and user.changepassword WS calls
 * @author Nitin Singh
 *
 */
public class ChangePasswordActivity extends Activity {

	/**
	 * String containing the data entered in the New Password TextField by the user
	 */
	String newpassword="";

	/**
	 * String containing the data entered in the Confirm Password TextField by the user
	 */
	String confirmpassword="";

	/**
	 * String containing the response from the user.changepassword WS
	 */
	String changepasswordresponse="";

	/**
	 * Object handling the Progress Dialog shown on the Change Password Screen
	 */
	ProgressDialog progressDialog;

	/**
	 * Flag indicating whether the password was changed succesfully or not
	 */
	boolean passwordchangedflag=false;

	/**
	 * The response code returned by the user.chagnepassword WS
	 */
	int responsecode=-1;

	/**
	 * Flag to be set depending on whether or not the stored session id in Session.java is null nor not , if null the appliacation is deemed as crashed and the user needs to login again to establish a valid session 
	 */
	boolean crashflag=false;

	/**
	 * The string storing the Preference file name used for storing user credentials
	 */
	public static final String PREFS_NAME = "MobiibookPrefsFile";

	/**
	 * The string used to refer the password in the Preference file used to store user credentials
	 */
	private static final String PREF_PASSWORD = "password";



	/**
	 * Called when the activity is first created. 
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.changepasswordactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Change Temporary Password");
		}
	}



	/**
	 * 
	 * @param view
	 * view object gives all the layout elements of the screen.
	 */
	public void onChangePasswordClick(View view){
		changepasswordresponse="";

		EditText temp;
		//		temp= (EditText)findViewById(R.id.EditText01);
		//		oldpassword=temp.getText().toString().trim();

		temp= (EditText)findViewById(R.id.EditText02);
		newpassword=temp.getText().toString().trim();

		temp= (EditText)findViewById(R.id.EditText03);
		confirmpassword=temp.getText().toString().trim();


		if(validateFields())
		{

			changepasswordWS(newpassword, confirmpassword);
		}
		else {
			showResponse(changepasswordresponse);	
		}
	}



	/**
	 * Function responsible for validating the data entered by the user
	 * @return true/false depending on whether the validation was successful or not
	 */
	public boolean validateFields() {
		//		if(oldpassword.length()==0){
		//			changepasswordresponse=getText(R.string.emptywarning).toString()+" "+getText(R.string.oldpassword).toString();
		//			return false;
		//		}

		if(newpassword.length()==0){
			changepasswordresponse=getText(R.string.newpassword).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if(confirmpassword.length()==0){
			changepasswordresponse=getText(R.string.confirmpassword).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if(!(newpassword.equals(confirmpassword))){
			changepasswordresponse=getText(R.string.newpassword).toString()+" and "+getText(R.string.confirmpassword).toString()+" "+getText(R.string.mismatch);
			return false;
		}

		else return true;
	}



	/**
	 * Function responsible for showing the Dialogs on the screen
	 * @param response 
	 * The response returned by the user.chagnepassword WS
	 */
	public void showResponse(String response){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setInverseBackgroundForced(true)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.fi
				if(passwordchangedflag){
					getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
					.edit()
					.putString(PREF_PASSWORD,newpassword)
					.commit();


					Intent explicitintent=new Intent(ChangePasswordActivity.this,HomeScreenActivity.class);
					startActivity(explicitintent);

				}
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}



	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			progressDialog.dismiss();

			if(crashflag){
				AlertDialog alert;

				AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
				builder.setMessage(getText(R.string.oncrashmessage))
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent explicitIntent= new Intent(ChangePasswordActivity.this,login.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(explicitIntent);
					}
				});
				alert = builder.create();
				alert.setTitle("Notification");
				alert.show();
			}

			else
				showResponse(changepasswordresponse);

		}	
	};



	/**
	 * Function for user.changepassword WS call
	 * @param newpass 
	 * The new password entered by the user
	 * @param confirmpass
	 * The re-entered new password entered by the user
	 */
	public void changepasswordWS(final String newpass,final String confirmpass){

		crashflag=false;

		progressDialog = ProgressDialog.show(this, "", "Working...");

		new Thread() {
			public void run() {
				try {
					if(Session.getSessionId()!=null){
						String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.changepassword", "UTF-8");
						data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
						//					data += "&" + URLEncoder.encode("oldpassword", "UTF-8")+"="+URLEncoder.encode(oldpass, "UTF-8");
						data += "&" + URLEncoder.encode("newpassword", "UTF-8")+"="+URLEncoder.encode(newpass, "UTF-8");
						data += "&" + URLEncoder.encode("confirmpassword", "UTF-8")+"="+URLEncoder.encode(confirmpass, "UTF-8");
						WebServiceCall wsCall = new WebServiceCall(data);
						//System.out.println("Starting Thread");
						//wsCall.start();
						System.out.println("calling forgotpassword....");
						//wsCall.join();
						//WebServiceCall castedThread = (WebServiceCall) wsCall;
						boolean threadStatus = wsCall.isThreadSucceeded();

						System.out.println("threadstatus is :"+threadStatus);

						if (threadStatus) {
							System.out.println("Success");
							System.out.println("The response " + wsCall.getExecutionResponse());

							//JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
							JSONArray jArray = wsCall.getExecutionResponse().getJSONArray("#data");

							System.out.println(jArray.toString());

							responsecode=jArray.getInt(0);

							if(responsecode==0)
								passwordchangedflag=true;//to be used in showresponse() to navigate to login screen

							System.out.println("the pass flag is :"+passwordchangedflag);

							changepasswordresponse=jArray.getString(1);

							System.out.println("calling dialog");


							//			if(obj.has("#error"))
							//			{
							//				//System.out.println("Error in restoreFileWS");
							//				obj.get("#message").toString();
							//				//continueRestore = false;
							//			}
							//			else if(!(obj.getInt("response_code") == 0))
							//			{
							//				//continueRestore = false;
							//				System.out.println("RESPONSE : " + obj.getString("response_message"));
							//			}




						}
						else {
							System.out.println("The value is " + getText(R.string.network_message).toString());
							changepasswordresponse = getText(R.string.network_message).toString();
							//showResponse((getText(R.string.network_message)).toString());
						}
					}

					else
						crashflag=true;
				}catch(Exception e)
				{
					e.printStackTrace();
				}

				/** Message handler is being called */
				messageHandler.sendEmptyMessage(0);
			}

		}.start();
	}


	/**
	 * Overriding the default action when the user presses the back button on the device
	 * This makes sure the user can't go back
	 */
	@Override
	public void onBackPressed()
	{

	}
}


