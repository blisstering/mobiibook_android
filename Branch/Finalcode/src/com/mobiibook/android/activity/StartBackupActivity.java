/**
 * StartBackupActivity.java
 * @author Nitin Singh
 */
package com.mobiibook.android.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.mobiibook.android.utils.Session;


/**
 * Class responsible for handling the Backup Now screen and bundling contacts to a file called contacts.vcf on the SD card if the selected data types to be bacekd up contains Contacts
 * @author Nitin Singh
 *
 */
public class StartBackupActivity extends OptionsActivity {

	/**
	 * Object reference to the EditText where the user enters name of the backup
	 */
	private EditText backupName;
	
	/**
	 * Flag indicating whether the preparation to start the backup i.e. exporting of contacts to contacts.vcf has finished or not
	 */
	boolean isBackupPreparationFinished = false;
	
	/**
	 * Flag indicating whether the preparation to start the backup i.e. exporting of contacts to contacts.vcf has started or not
	 */
	boolean isBackupPreparationStarted = false;
	
	/**
	 * The MIME types of the content chosen to be backed up
	 */
	String contenttypetobebackedup;
	
	/**
	 * The content types of the content chosen to be backed up
	 */
	String contentnametobebackedup;
	
	/**
	 * The type of the backup
	 */
	String backuptype;
	
	
	
	/** Called when the activity is first created. 
	 * @author Prateek*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.startbackupactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Backup Now");
		}

		Bundle bundle=this.getIntent().getExtras();
		contenttypetobebackedup=bundle.getString("contenttypetobebackedup");
		contentnametobebackedup=bundle.getString("contentnametobebackedup");
		backuptype=bundle.getString("backuptype");
	}


	//	@Override 
	//	protected void onSaveInstanceState(Bundle outState) { 

	//	// remove any showing alert dialogs as we don't want 
	//	// them saved and available if the Activity instance is resumed. 

	////	outState.putBoolean("isBackupPreparationFinished", isBackupPreparationFinished);
	////	outState.putString("backupName", backupName.getText().toString().trim());

	//	// continue with the normal instance state save 
	//	super.onSaveInstanceState(outState); 
	//	} 

	//	@Override
	//	public void onRestoreInstanceState(Bundle savedInstanceState) {
	//	// Restore UI state from the savedInstanceState.
	//	// This bundle has also been passed to onCreate.
	//	boolean isBackPrepFinished = savedInstanceState.getBoolean("isBackupPreparationFinished");
	//	if(!isBackPrepFinished)
	//	progressDialog = ProgressDialog.show(this, "", "Preparing for the backup....");
	//	super.onRestoreInstanceState(savedInstanceState);


	//	}
	
	

	/** Called when Activity is in Paused state 
	 * @author Prateek
	 * */
	@Override
	public void onPause()
	{
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		isBackupPreparationFinished = false;
		super.onPause();
	}

	
	
	/** When the activity is resumed, progressdialog is to be shown or not is decided here. 
	 * @author Prateek
	 * */
	@Override
	public void onResume()
	{
		System.out.println("values are: isBackupPreparationFinished :"+isBackupPreparationFinished+"\n isBackupPreparationStarted :"+isBackupPreparationStarted+" \n session: "+Session.isBackupServiceRunning());
		if(!isBackupPreparationFinished && isBackupPreparationStarted && !Session.isBackupServiceRunning())
		{
			progressDialog = ProgressDialog.show(this, "", getText(R.string.backuppreparation).toString());
		}
		else
		{
			System.out.println("values are: isBackupPreparationFinished :"+isBackupPreparationFinished+"\n isBackupPreparationStarted :"+isBackupPreparationStarted+" \n session: "+Session.isBackupServiceRunning());
		}
		super.onResume();
	}


	
	/** Starts the backup if its not already running and also check for validations of backup name
	 * @param view
	 * view object gives all the layout elements of the screen. 
	 * @author Prateek,modified by Nitin
	 * */
	public void startBackup(View view) {
		if(Session.isBackupServiceRunning())
		{
			Intent explicitIntent;
			explicitIntent = new Intent(StartBackupActivity.this,BackupServiceActivity.class);
			startActivity(explicitIntent);
		}
		else
		{
			backupName = (EditText)findViewById(R.id.EditText01);
			String bName = backupName.getText().toString().trim();
			if(bName.length() == 0)
				showResponse(getText(R.string.enterbackupname).toString());
			else
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Are you sure you want to start backup?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						//check for network availability first
						ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
					    NetworkInfo netInfo = cm.getActiveNetworkInfo();
					    
					    if (netInfo != null && netInfo.isConnected()) {
					        
						
						if(isexternalStorageAvailable()){
							if(contenttypetobebackedup.contains("contacts")||contenttypetobebackedup.contains("all"))
							{
								System.out.println("calling export contacts");
								exportContact();
							}

							else{
								System.out.println("switching to backupserviceactivity");
								Bundle bundle = new Bundle();
								bundle.putString("backupName",backupName.getText().toString().trim());
								bundle.putString("contenttypetobebackedup",contenttypetobebackedup);
								bundle.putString("contentnametobebackedup", contentnametobebackedup);
								bundle.putString("backuptype",backuptype);
								//Intent explicitIntent = new Intent(StartBackupActivity.this,BackupActivity.class);
								Intent explicitIntent = new Intent(StartBackupActivity.this,BackupServiceActivity.class);

								explicitIntent.putExtras(bundle);
								startActivity(explicitIntent);


							}
						}
						else
							showResponse(getText(R.string.nosdcardfound).toString());
					    }
					    
					    else
					    	showResponse(getText(R.string.network_message).toString());
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent explicitintent=new Intent(StartBackupActivity.this,HomeScreenActivity.class);
						startActivity(explicitintent);
					}
				});
				AlertDialog alert = builder.create();
				alert.show();


			}
		}
	}

	
	
	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			isBackupPreparationFinished = true;
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			Bundle bundle = new Bundle();
			bundle.putString("backupName",backupName.getText().toString().trim());
			bundle.putString("contenttypetobebackedup",contenttypetobebackedup);
			bundle.putString("contentnametobebackedup",contentnametobebackedup);
			bundle.putString("backuptype",backuptype);
			//Intent explicitIntent = new Intent(StartBackupActivity.this,BackupActivity.class);
			Intent explicitIntent = new Intent(StartBackupActivity.this,BackupServiceActivity.class);

			explicitIntent.putExtras(bundle);
			startActivity(explicitIntent);
		}
	};

	
	
	/**
	 * Exports all the contact to a contacts.vcf file saved in the externalStorage 
	 * @author Prateek
	 */
	public void exportContact()
	{
		isBackupPreparationStarted = true;
		progressDialog = ProgressDialog.show(this, "", getText(R.string.backuppreparation).toString());
		new Thread() {
			public void run() {
				ContentResolver cr = getContentResolver();
				Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
						null, null, null, null);
				System.out.println("The count of contacts is " + cur.getCount());
				if (cur.getCount() > 0) {
					try{
						String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
						File vCardFile = new File(extStorageDirectory,"/contacts.vcf");
						if (!vCardFile.exists()) {
							try {
								vCardFile.createNewFile();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						OutputStream os = new FileOutputStream(vCardFile);

						while (cur.moveToNext()) {
							try{
								String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
								Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
								System.out.println("The value is " + cr.getType(uri));
								AssetFileDescriptor fd = cr.openAssetFileDescriptor(uri, "r");
								FileInputStream fis = fd.createInputStream();
								byte[] b = new byte[1024*4];  
								int read;  
								while ((read = fis.read(b)) != -1) {  
									os.write(b, 0, read);  
								} 
								//								byte[] buf = new byte[(int)fd.getDeclaredLength()];
								//								if (0 < fis.read(buf))
								//								{
								//								String vCard = new String(buf);
								//								System.out.println("The vCard value is " + vCard);
								//								}
								fis.close();
							}
							catch(Exception e)
							{
								System.out.println(e.getStackTrace());
							}
						}
						os.flush();
						os.close();	
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}

				}
				//isBackupPreparationFinished = true;
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}
}

