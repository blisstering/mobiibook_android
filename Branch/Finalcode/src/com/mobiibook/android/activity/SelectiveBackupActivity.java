/**
 * SelectiveBackupActivity.java
 * @author Nitin Singh
 */
package com.mobiibook.android.activity;

import com.mobiibook.android.activity.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


/**
 * Class responsible for showing the screen when Backup Now is selected
 * @author Nitin Singh
 *
 */
public class SelectiveBackupActivity extends OptionsActivity{

	/**
	 * The MIME type of the content to be backedup
	 */
	String contenttypetobebackedup="";
	
	/**
	 * The types of data items to be backed up
	 */
	String contentnametobebackedup="";
	//String mimenames[]={"all","audio","video","image","calendar","contacts","textmessages","applications"};
	
	/**
	 * The list of MIME names of the data that can be backed up by the application
	 */
	String mimenames[]={"all","calendar","contacts","image","audio","video","textmessages","applications"};
	
	/**
	 * The list of types of data items that can be backed up by the application
	 */
	String contentlist[]={"All","Calendar","Contacts","Pictures","Music","Video Clips","Text Messages","Applications and Games "};

	/**
	 * Object for showing the list being shown on the screen
	 */
	ListView list;
	
	
	
	/**
	 * Called first when the activity is started
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.selectivebackupactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Select Items to Backup");
		}

		list = (ListView) findViewById(android.R.id.list);
		list.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_multiple_choice, contentlist));
		list.setCacheColorHint(00000000);
		list.setItemsCanFocus(false);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		list.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,int position, long id) {

				System.out.println("The ID is " + id+"position is "+position);

				if(id == 0 && list.getCheckedItemPositions().get(0))
				{
					for(int i =1;i<=contentlist.length;i++)
					{
						list.setItemChecked(i, true);
					}
				}
				else if(id == 0 && !list.getCheckedItemPositions().get(0))
				{
					for(int i =1;i<=contentlist.length;i++)
					{
						list.setItemChecked(i, false);
					}
				}
				else if(id !=0)
				{
					String checkall="";

					if(list.getCheckedItemPositions().get(position)){

						for(int i=1;i<contentlist.length;i++)
							checkall+=list.getCheckedItemPositions().get(i);
						
						System.out.println("checkall"+checkall);

						if(checkall.contains("false"))
							list.setItemChecked(0, false);

						else 
							list.setItemChecked(0, true);
					}
					else if(!list.getCheckedItemPositions().get(position))
						list.setItemChecked(0, false);

				}
			}
		});

		////		contenttype=new CheckBox[8];
		////		contenttype[0] = (CheckBox)findViewById(R.id.CheckBox01);
		////		contenttype[0].setOnCheckedChangeListener(this);
		////
		////		contenttype[1] = (CheckBox)findViewById(R.id.CheckBox02);
		////		contenttype[1].setOnCheckedChangeListener(this);
		////
		////		contenttype[2] = (CheckBox)findViewById(R.id.CheckBox03);
		////		contenttype[2].setOnCheckedChangeListener(this);
		////
		////		contenttype[3] = (CheckBox)findViewById(R.id.CheckBox04);
		////		contenttype[3].setOnCheckedChangeListener(this);
		////
		////		contenttype[4] = (CheckBox)findViewById(R.id.CheckBox05);
		////		contenttype[4].setOnCheckedChangeListener(this);
		////
		////		contenttype[5] = (CheckBox)findViewById(R.id.CheckBox06);
		////		contenttype[5].setOnCheckedChangeListener(this);
		////
		////		contenttype[6] = (CheckBox)findViewById(R.id.CheckBox07);
		////		contenttype[6].setOnCheckedChangeListener(this);
		////
		////		contenttype[7] = (CheckBox)findViewById(R.id.CheckBox08);
		////		contenttype[7].setOnCheckedChangeListener(this);
		//
		//
		//	}
		//
		//	public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		//
		//		if(buttonView.getText().toString().equals("All"))
		//		{
		//			if(buttonView.isChecked()){
		//				for(int i=1;i<8;i++){
		//					contenttype[i].setChecked(false); 
		//				}
		//			}
		//
		//		}
		//
		//		else{
		//			contenttype[0].setChecked(false);
		//		}
		//
		//
	} 

	
	
	/**
	 * Function called when the Proceed button is clicked on the Schedule backup screen
	 * @param view
	 * view object gives all the layout elements of the screen.
	 */
	public void onSelectiveBackupClick(View view){

		contenttypetobebackedup="";
		contentnametobebackedup="";

		if(list.isItemChecked(0)){
			contenttypetobebackedup="all";
			contentnametobebackedup=getText(R.string.contenttypes).toString();
		}

		else{
			for(int i=1;i<contentlist.length;i++){
				if(list.isItemChecked(i)){
					contenttypetobebackedup+=":"+mimenames[i];
					contentnametobebackedup+=contentlist[i]+",";
				}
			}
		}
		System.out.println("to be backed up "+contenttypetobebackedup);

		if(contenttypetobebackedup.trim().equals("")){
			showResponse("Nothing selected for back up");
		}

		//		else if (contenttypetobebackedup.contains("applications")){
		//			showResponse("Currently backup of Applications is not supported. Please uncheck these and try again.");
		//		}

		else{
			Intent explicitintent = new Intent (SelectiveBackupActivity.this,StartBackupActivity.class);
			explicitintent.putExtra("contenttypetobebackedup", contenttypetobebackedup);
			explicitintent.putExtra("contentnametobebackedup",contentnametobebackedup.substring(0, contentnametobebackedup.length()-1));
			explicitintent.putExtra("backuptype","Manual");
			startActivity(explicitintent);
		}
	}



	/**
	 * This function is responsible for displaying a dialog box in case any notification is to be shown to the user
	 * @author Nitin
	 * @param String response 
	 * response is the message that has to be displayed to the user in the dialog box
	 *   
	 * passwordchangedflag is set in the method forgotpasswordWS() when the user.forgotpassword web-service returns a success 
	 * This flag allows to handle this special case where after displaying the dialog box the login activity needs to be shown
	 */
	public void showResponse(String response){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setInverseBackgroundForced(true)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}

		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}
}
