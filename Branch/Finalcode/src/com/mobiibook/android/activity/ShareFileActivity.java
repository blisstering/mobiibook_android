/**
 * ShareFileActivity.java
 * @author Prateek Jain
 */
package com.mobiibook.android.activity;

import java.io.File;
import java.net.URLEncoder;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.Session;

/**
 * Class responsible for showing the screen when user clicks share button after choosing friends to share with and handling calls to share.file WS
 * @author Prateek Jain
 *
 */
public class ShareFileActivity extends Activity{

	/**
	 * Flag to be set depending on whether or not the stored session id in Session.java is null nor not , if null the appliacation is deemed as crashed and the user needs to login again to establish a valid session 
	 */
	boolean crashflag=false;

	/**
	 * Flag set when the currently logged in user enters his email id in the ppl to share the data with
	 */
	boolean invitingselfflag=false;


	/** Called when the activity is first created. 
	 * @author Prateek*/
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = this.getIntent().getExtras();
		final String path = bundle.getString("path");
		System.out.println("The path " + path);
		final String emailList = bundle.getString("emailList");
		System.out.println("The emailList " + emailList);
		shareWS(path, emailList);
	}

	//	public void showResponse(String response)
	//	{
	//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	//		builder.setMessage(response)
	//		.setCancelable(false)
	//		.setPositiveButton("Ok", null);
	//		AlertDialog alert = builder.create();
	//		alert.setTitle("Notification");
	//		alert.show();
	//
	//	}

	
	
	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response)
	{

		if(crashflag){
			AlertDialog alert;

			AlertDialog.Builder builder = new AlertDialog.Builder(ShareFileActivity.this);
			builder.setMessage(getText(R.string.oncrashmessage))
			.setCancelable(false)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					Intent explicitIntent= new Intent(ShareFileActivity.this,login.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			});
			alert = builder.create();
			alert.setTitle("Notification");
			alert.show();
		}

		else{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					if(invitingselfflag){
						Intent explicitIntent = new Intent(ShareFileActivity.this,ShareWithMoreFriendsActivity.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //HomeScreen is made top of the stack. No other activity reside on the top of HomeScreen 
						startActivity(explicitIntent);
					}

					else {
						Intent explicitIntent = new Intent(ShareFileActivity.this,HomeScreenActivity.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //HomeScreen is made top of the stack. No other activity reside on the top of HomeScreen 
						startActivity(explicitIntent);
					}
				}
			});
			AlertDialog alert = builder.create();
			alert.setTitle("Notification");
			alert.show();
		}
	}


	
	/**
	 * share.file web service call
	 * Used to share the file on the web
	 * @param filePath
	 * the absolute path of the file being shared
	 * @param listOfEmailId
	 * the comma seperated list of email ids to share the content with
	 * @author Prateek
	 */
	public String shareWS(String filePath, String listOfEmailId)
	{
		crashflag=false;
		invitingselfflag=false;

		File file = new File(filePath);
		//File file = new File("data/data/com.android.createFileTest/test.txt");
		if (!file.exists()) {
			showResponse("File does not exist");
		}
		else
		{
			try {
				if(Session.getSessionId()!=null){
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("share.file", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					data += "&" + URLEncoder.encode("file_name", "UTF-8")+"="+URLEncoder.encode(file.getName(), "UTF-8");
					String length = Long.toString(file.length());
					data += "&" + URLEncoder.encode("file_size", "UTF-8")+"="+URLEncoder.encode(length, "UTF-8");
					data += "&" + URLEncoder.encode("file_path", "UTF-8")+"="+URLEncoder.encode(file.getAbsolutePath(), "UTF-8");		
					data += "&" + URLEncoder.encode("file_last_modified_time", "UTF-8")+"="+URLEncoder.encode(Long.toString(file.lastModified()), "UTF-8");
					data += "&" + URLEncoder.encode("email_id", "UTF-8")+"="+URLEncoder.encode(listOfEmailId, "UTF-8");
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Starting Thread");
					System.out.println("Waiting for thread to die");
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							System.out.println("Error in startBackupWS");
						}
						else
						{
							if(obj.getString("response_code").equals("10"))
								invitingselfflag=true;

							showResponse(obj.getString("response_message"));
						}

					} else {
						System.out.println("Failure");
					}
				}

				else{
					crashflag=true;
					showResponse(getText(R.string.oncrashmessage).toString());
				}
			}catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}
}
