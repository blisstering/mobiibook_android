/**
 * BackupTypeSelectionActivity.java
 * @author Nitin Singh
 */

package com.mobiibook.android.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.Session;

/**
 * Class responsible for handling the screen when Backup is selected from Home Screen or the bottom bar
 * @author Nitin Singh
 *
 */
public class BackupTypeSelectionActivity extends OptionsActivity{
	private ListView list;

	/**
	 * List items to be shown on the screen(namely "Backup Now" and "Schedule Backup")
	 */
	static final String[] LISTITEM = new String[] {
		"1.Backup Now",  "2.Schedule Backup"
	};
	

	
	/** Called when the activity is first created. 
	 * @author Nitin Singh*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Backup");
		}

		list=(ListView)findViewById(R.id.ListView01);
		list.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		list.setCacheColorHint(00000000); // On scrolling the background of the list view doesn't change

		list.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {

				switch(position)
				{
				case 0:
					backupNow();
					break;
				case 1:
					scheduleBackup();


					//					AlertDialog.Builder builder = new AlertDialog.Builder(BackupTypeSelectionActivity.this);
					//					builder.setMessage("This feature is not yet available")
					//					.setCancelable(false)
					//					.setInverseBackgroundForced(true)
					//					.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					//						public void onClick(DialogInterface dialog, int id) {
					//						}
					//					});
					//					AlertDialog alert = builder.create();
					//					alert.setTitle("Notification");
					//					alert.show();

					break;
				default:
					break;
				}
			}
		});
	}

	
	/**
	 * Function that is called when the Backup Now option is selected
	 */
	public void backupNow()
	{
		if(Session.isBackupServiceRunning())
		{
			Intent explicitIntent;
			explicitIntent = new Intent(BackupTypeSelectionActivity.this,BackupServiceActivity.class);
			startActivity(explicitIntent);
		}
		else if(Session.isRestoreServiceRunning())
		{
			showResponse(getText(R.string.restoreinprogress).toString());
		}
		else
		{
			Intent explicitintent=new Intent(BackupTypeSelectionActivity.this,SelectiveBackupActivity.class);
			startActivity(explicitintent);

		}

	}

	
	/**
	 * Function that is called when Schedule Backup option is selected
	 */
	public void scheduleBackup(){
		//			Intent explicitintent=new Intent(BackupTypeSelectionActivity.this,ScheduleBackupActivity.class);
		//			startActivity(explicitintent);

		final CharSequence[] items = {"1.Schedule a backup","2.List all schedules"};
	

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Select");

		builder.setItems(items, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int item) {

				if(item==0){
					 
					Intent explicitintent=new Intent(BackupTypeSelectionActivity.this,ScheduleBackupActivity.class);
					startActivity(explicitintent);
				}
				
				if(item==1){
					//code to list all the activites
					Intent explicitintent=new Intent(BackupTypeSelectionActivity.this,ListSchedulesActivity.class);
					startActivity(explicitintent);
				}

			}

		});

		AlertDialog alert = builder.create();

		alert.show();
	}
}