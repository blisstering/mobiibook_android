/**
 * login.java
 * @author Prateek Jain/Nitin Singh
 */

package com.mobiibook.android.activity;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.Session;
import com.mobiibook.android.utils.Validation;


/**
 * Class responsible for the handling the login process
 * @author Prateek Jain/Nitin Singh
 */
public class login extends Activity {

	/**
	 * The URL object to store the URL of the server.
	 */
	URL url;

	/**
	 * The HttpURLConnection object that uses the URL to connect to the server.
	 */
	HttpURLConnection conn;

	/**
	 * @username The EditText object for username.
	 * @password The EditText object for password.
	 */
	private EditText username,password;


	/**
	 * The object responsible for showing the Progress dialog when user logs in
	 */
	ProgressDialog progressDialog;

	/**
	 *The response message returned by the user.login WS 
	 */
	String loginResponse;

	/**
	 * The flag that shows whether the call to user.login resulted in successfull login or not
	 */
	boolean loginSuccess = false;

	/**
	 * The string storing the Preference file name used for storing user credentials
	 */
	public static final String PREFS_NAME = "MobiibookPrefsFile";

	/**
	 * The string used to refer the username in the Preference file used to store user credentials
	 */
	private static final String PREF_USERNAME = "username";

	/**
	 * The string used to refer the password in the Preference file used to store user credentials
	 */
	private static final String PREF_PASSWORD = "password";

	/**
	 * The flag that stores whether the user checked the Remember Me option while logging in or not
	 */
	boolean rememberMe = true;

	/**
	 * String containing the sesssion id returned by the call to system.connect WS
	 */
	String systemconnectsessionid=null;


	/** Called when the activity is first created. 
	 * @author Prateek*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.main);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Login");
		}


		/** To enable the Remember Me option on the login screen a private file is created */ 
		SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);   
		String username1 = pref.getString(PREF_USERNAME, null);
		String password1 = pref.getString(PREF_PASSWORD, null);

		if((username1 != null) && (password1 != null))
		{
			((EditText)findViewById(R.id.EditText01)).setText(username1);
			((EditText)findViewById(R.id.EditText02)).setText(password1);
		}

		//		getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
		//		.edit()
		//		.putString(PREF_USERNAME, "vivek")
		//		.putString(PREF_PASSWORD, "vivek")
		//		.commit();

		if(Session.getSessionId()!=null){
			Intent explicitIntent= new Intent(login.this,HomeScreenActivity.class);
			startActivity(explicitIntent);
		}
	}



	/**
	 * The login button clicked event is handled in this function
	 * @author Prateek
	 * @param View view
	 * view object gives all the layout elements of the screen.
	 *
	 */
	public void login_method(View view) {
		switch (view.getId()) {
		case R.id.Button01:
			if(Session.getSessionId()!=null){
				Intent explicitIntent= new Intent(login.this,HomeScreenActivity.class);
				startActivity(explicitIntent);
			}
			else{
				username = (EditText)findViewById(R.id.EditText01);
				password = (EditText)findViewById(R.id.EditText02);
				if(validateFields(username.getText().toString().trim(),password.getText().toString()))
					systemConnectWS();
				else
				{
					showResponse(loginResponse);
				}
				//progressDialog.dismiss();
			}
			break;

		}
	}



	/**
	 * Registeration process starts from here
	 * @author Prateek
	 * @param View view
	 * view object gives all the layout elements of the screen.
	 *
	 */
	public void registerMethod(View view)
	{
		//showResponse( getText(R.string.registermessage).toString());
		//		Intent myIntent  = new Intent(login.this,RegisterUserActivity.class);
		//		startActivity(myIntent);

		AlertDialog alert;

		AlertDialog.Builder builder = new AlertDialog.Builder(login.this);
		builder.setMessage(getText(R.string.registrationmessage))
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://m.mobiibook.com/user/register"));
				startActivity(browserIntent);
			}
		});
		alert = builder.create();
		alert.setTitle("Notification");
		alert.show();




	}



	/**
	 * Forgot Password process starts from here
	 * @author Prateek
	 * @param View view
	 * view object gives all the layout elements of the screen.
	 *
	 */
	public void forgotPasswordMethod(View view)
	{
		//showResponse( getText(R.string.forgotpasswordmessage).toString());
		//Intent myIntent = new Intent(view.getContext(),ForgotPasswordActivity.class);
		Intent myIntent  = new Intent(login.this,ForgotPasswordActivity.class);
		startActivity(myIntent);

	}



	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog.dismiss();
			if(!loginSuccess)
				showResponse(loginResponse);
		}
	};



	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", null)
		.setInverseBackgroundForced(true);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}



	/**
	 * system.connect web service call
	 * @author Prateek
	 */
	public void systemConnectWS()
	{
		progressDialog = ProgressDialog.show(this, "", "Logging in.....");
		//		progressDialog = new ProgressDialog(this); 
		//		progressDialog.setTitle(""); 
		//		progressDialog.setMessage("Loading. Please wait..."); s
		//		progressDialog.setIndeterminate(true); 
		//		progressDialog.setProgressStyle(R.style.MobiiBookProgressDialog);
		//		progressDialog.show();
		new Thread() {
			public void run() {
				try {
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("system.connect", "UTF-8");
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Calling web service");
					//wsCall.start();
					//System.out.println("Waiting for thread to die");
					//wsCall.join();
					//WebServiceCall castedThread = (WebServiceCall) wsCall;
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							System.out.println("Error in system.connect");
						}
						else
						{
							String sessionId = obj.getString("sessid");
							//							Session.setSessionId(sessionId);
							//							if(Session.getSessionId() != null)

							systemconnectsessionid=sessionId;
							if(systemconnectsessionid!=null)
							{
								//progressDialog.dismiss();
								loginWS(username.getText().toString().trim(),password.getText().toString().trim());
							}
							else
							{
								System.out.println("session id not available");
							}
						}
					} else {
						System.out.println("The value is " + getText(R.string.network_message).toString());
						loginResponse = getText(R.string.network_message).toString();
						//showResponse((getText(R.string.network_message)).toString());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/** Message handler is being called */
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}



	/**
	 * user.login web service call
	 * @param String uname
	 * Username of the user
	 * @param String pwd
	 * Password of the user
	 * @author Prateek
	 * 
	 */
	public void loginWS(final String uname,final String pwd)
	{
		//progressDialog = ProgressDialog.show(this, "", "Logging in.....");

		try {
			String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.login", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(systemconnectsessionid, "UTF-8");
			data += "&" + URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(uname, "UTF-8");
			data += "&" + URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(pwd, "UTF-8");
			WebServiceCall wsCall = new WebServiceCall(data);
			//System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Calling login....");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());

				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					//System.out.println(obj.get("#message"));
					loginResponse = obj.get("#message").toString();
				}
				else
				{
					System.out.println("the object returned " + obj);
					Session.setSessionId(new String(obj.getString("sessid")));
					System.out.println("Session Id(Logged In)"+Session.getSessionId());

					//				    JSONObject temp= obj.getJSONObject("user");
					//				    System.out.println("flag is :"+temp.getString("force_password_change").toString());


					System.out.println("#data(Login Success)  : " + obj);
					JSONObject userRole=obj.getJSONObject("user").getJSONObject("roles");
					System.out.println("user Role : "+userRole);
					//Intent explicitIntent = new Intent(login.this,HomeScreen.class);
					loginSuccess = true;

					//Remember me
					String prefUsername = uname;
					String prefPassword = pwd;
					rememberMe = ((CheckBox)findViewById(R.id.CheckBox01)).isChecked();
					if(!rememberMe)
					{
						//prefUsername = null;
						prefPassword = null;
					}

					getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
					.edit()
					.putString(PREF_USERNAME, prefUsername)
					.putString(PREF_PASSWORD, prefPassword)
					.commit();


					//if the login is the first time login after registration then redirect the user to the change password screen else to the home screen.
					if((obj.getJSONObject("user")).getString("force_password_change").toString().equals("1")){

						Intent explicitIntent=new Intent(login.this,ChangePasswordActivity.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						startActivity(explicitIntent);
					}
					else{
						Intent explicitIntent = new Intent(login.this,HomeScreenActivity.class);
						startActivity(explicitIntent);
					}
				}
			} else {
				System.out.println("The value is " + getText(R.string.network_message).toString());
				loginResponse = getText(R.string.network_message).toString();
				//showResponse((getText(R.string.network_message)).toString());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}



	/**
	 * Validates all the fields of the screen
	 * @param String uname
	 * Username of the user
	 * @param String pwd
	 * Password of the user
	 * @return 
	 * True or false
	 * @author Prateek
	 * 
	 */
	public boolean validateFields(String uname, String pwd)
	{
		if(uname.length() == 0 && pwd.length() == 0)
		{
			loginResponse = getText(R.string.enteremailorpassword).toString()+" "+getText(R.string.email)+" address and "+getText(R.string.password);
			return false;
		}


		if(uname.length() == 0)
		{
			loginResponse = getText(R.string.enteremailorpassword).toString()+" "+getText(R.string.email)+" address";
			return false;
		}


		if(pwd.length() == 0)
		{
			loginResponse = getText(R.string.enteremailorpassword).toString()+" "+getText(R.string.password);
			return false;
		}

		Validation v = new Validation();
		if(v.validateEmail(uname))
		{
			return true;
		}
		else
		{
			loginResponse = getText(R.string.entervalidemail).toString();
			return false;
		}
	}

	//	private void executeHttpGet(String uname,String pwd) {


	//	try {
	//	url=new URL("http://www.bliss-dev.com/mobiibook/services/json");

	//	data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("system.connect", "UTF-8");

	//	conn=(HttpURLConnection)url.openConnection();
	//	conn.setDoOutput(true);						
	//	wr=new OutputStreamWriter(conn.getOutputStream());
	//	wr.write(data);
	//	wr.flush();

	//	rd=new BufferedReader(new InputStreamReader(conn.getInputStream()));
	//	response = "";
	//	while((line=rd.readLine())!=null)
	//	{
	//	response=response+line;
	//	}
	//	wr.close();
	//	rd.close();
	//	System.out.println(response);
	//	conn.disconnect();

	//	JSONObject obj;

	//	obj = new JSONObject(response);
	//	obj=obj.getJSONObject("#data");

	//	String sessionId = obj.getString("sessid");
	//	//displaytext.setText(obj.toString());
	//	//////////////////// USER.LOGIN //////////////////

	//	data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.login", "UTF-8");
	//	data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(sessionId, "UTF-8");
	//	data += "&" + URLEncoder.encode("username", "UTF-8")+"="+URLEncoder.encode(uname, "UTF-8");
	//	data += "&" + URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(pwd, "UTF-8");

	//	conn=(HttpURLConnection)url.openConnection();
	//	conn.setDoOutput(true);

	//	wr=new OutputStreamWriter(conn.getOutputStream());
	//	wr.write(data);
	//	wr.flush();

	//	rd=new BufferedReader(new InputStreamReader(conn.getInputStream()));
	//	response = "";
	//	while((line=rd.readLine())!=null)
	//	{
	//	response=response+line;
	//	}
	//	wr.close();
	//	rd.close();
	//	System.out.println(response);
	//	conn.disconnect();

	//	obj = new JSONObject(response);
	//	obj=obj.getJSONObject("#data");
	//	if(obj.has("#error"))
	//	{
	//	displaytext.setText(obj.getString("#message"));
	//	}
	//	else
	//	{
	//	sessionId = obj.getString("sessid");
	//	displaytext.setText(obj.getString("sessid"));
	//	Intent explicitIntent = new Intent(login.this,HomeScreen.class);
	//	startActivity(explicitIntent);
	//	}

	//	///displaytext.setText(obj.toString());
	//	} 


	//	catch (Exception e) {
	//	e.printStackTrace();
	//	}
	//	finally{
	//	progressDialog.dismiss();
	//	if(conn != null)
	//	conn.disconnect();

	//	wr = null;
	//	rd = null;
	//	}

	//	}



}