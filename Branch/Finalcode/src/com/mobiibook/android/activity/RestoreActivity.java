/**
 * RestoreActivity.java
 * @author Prateek Jain/Nitin Singh
 */
package com.mobiibook.android.activity;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import java.text.DateFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.Session;

/**
 * Class responsible for showing the screen when Retrieve Now is selected from Home screen or bottom bar
 * @author Prateek Jain/ Nitin Singh
 *
 */
public class RestoreActivity extends OptionsActivity{

	/**
	 * List of all the backups taken by the currently logged in user and returned by backup.getlist WS
	 */
	String[] LISTITEM = new String[0];

	/**
	 * Flag indicating whether the call to backup.getlist WS was successful or not
	 */
	boolean backupGetListStatus = false;

	/**
	 * The response message returned by backup.getlist WS
	 */
	String responseMessage;

	/**
	 * The JSON array returned by the backup.getlist WS
	 */
	JSONArray jArray;

	/**
	 * The listview object used to display the backup list on the screen
	 */
	private ListView lv1;

	/**
	 * The list of Content Details of the backups retrieved by backup.getlist WS
	 */
	String[] contentname;

	/**
	 * The list of start time of the backups retrieved by backup.getlist WS
	 */
	String[] starttime;

	/**
	 * Flag to be set depending on whether or not the stored session id in Session.java is null nor not , if null the appliacation is deemed as crashed and the user needs to login again to establish a valid session 
	 */
	boolean crashflag=false;



	/** Called when the activity is first created. 
	 * If the backup service is already running fetch do not start the service again
	 * If not then start the backup service
	 * @author Prateek
	 * */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setListAdapter(new ArrayAdapter(this, R.layout.homescreenactivity, WORLDCUP2010));
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		backupGetListWS();
		if(LISTITEM == null)
		{
			LISTITEM = new String[1];
			LISTITEM[0] = responseMessage;
		}
		lv1=(ListView)findViewById(R.id.ListView01);
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		lv1.setCacheColorHint(00000000);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Retrieve old Backups");
		}
		//getListView().setTextFilterEnabled(true);
		lv1.setOnItemClickListener(new OnItemClickListener() {  

			public void onItemClick(AdapterView parent, View v,int position, long id) {

				//ask the user for confirmation of starting the retrieval of backed up data
				AlertDialog choicealert;

				final int pos=position;
				final View view=v;

				AlertDialog.Builder choicebuilder = new AlertDialog.Builder(RestoreActivity.this);
				choicebuilder.setMessage("Are you sure you want to retrieve the selected backup?")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						startResotre(pos, view);

					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();

					}
				});
				choicealert = choicebuilder.create();
				choicealert.setTitle("Notification");
				choicealert.show();


			}
		});
		//		progressDialog.dismiss();
	}



	/**
	 * Function called when a backup is tapped for restoring on the screen 
	 * @param position 
	 * The position of the item tapped on the list
	 * @param v
	 * v object gives all the layout elements of the screen.
	 */
	public void startResotre(final int position,final View v){

		// When clicked, show a toast with the TextView text
		Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
				Toast.LENGTH_SHORT).show();
		System.out.println("The position is " + position);

		//Intent explicitIntent = new Intent(RestoreActivity.this,RestoreBackupActivity.class);
		if(isexternalStorageAvailable())
		{
			//since text messages are not restorable ask the user to choose another backup in a dialog message
			if(contentname[position].equalsIgnoreCase("Text Messages")){
				AlertDialog alert;

				AlertDialog.Builder builder = new AlertDialog.Builder(RestoreActivity.this);
				builder.setMessage(getText(R.string.nonrestorablecontent))
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
				alert = builder.create();
				alert.setTitle("Notification");
				alert.show();
			}

			else{
				Intent explicitIntent = new Intent(RestoreActivity.this,RestoreServiceActivity.class);
				Bundle bundle = new Bundle();
				try{
					JSONObject jObj = jArray.getJSONObject(position); 
					bundle.putString("backup_id",jObj.getString("backup_id"));
					bundle.putString("backup_name", jObj.getString("backup_name"));
					bundle.putString("content_categories", contentname[position]);
					bundle.putString("start_time", starttime[position]);

					explicitIntent.putExtras(bundle);
					startActivity(explicitIntent);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			showResponse(getText(R.string.nosdcardfound).toString(),false);
		}
	}



	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @param boolean finishActivity
	 * If true, the activity is finished after dialog box is shown
	 * else just a dialog box is shown on the screen
	 * @author Prateek
	 */
	public void showResponse(String response,final boolean finishActivity)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.finish();
				if(finishActivity)
				{
					setResult(RESULT_OK);
					finish();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}



	/** Called when Activity is in Paused state 
	 * @author Prateek
	 * */
	@Override
	public void onPause()
	{
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		super.onPause();
	}



	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();

			if(crashflag){
				AlertDialog alert;

				AlertDialog.Builder builder = new AlertDialog.Builder(RestoreActivity.this);
				builder.setMessage(getText(R.string.oncrashmessage))
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent explicitIntent= new Intent(RestoreActivity.this,login.class);
						explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(explicitIntent);
					}
				});
				alert = builder.create();
				alert.setTitle("Notification");
				alert.show();

			}
			else if(!backupGetListStatus)
			{
				showResponse(responseMessage,true);
			}
			else
			{
				updateListView();
			}
		}
	};



	/**
	 * Updates the list on the screen
	 * @author Prateek
	 */
	public void updateListView()
	{
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
	}

	/**
	 * Get the list of backups from the backup.getlist web service
	 * @author Prateek
	 */
	public void backupGetListWS()
	{
		crashflag=false;

		progressDialog = ProgressDialog.show(this, "", "Getting backup list...");
		new Thread() {
			public void run() {
				try {
					if(Session.getSessionId()!=null){
						String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.getlist", "UTF-8");
						data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
						//HardCoding for Android Client ID
						data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
						WebServiceCall wsCall = new WebServiceCall(data);
						System.out.println("Calling web service");
						backupGetListStatus = wsCall.isThreadSucceeded();
						if (backupGetListStatus) {
							System.out.println("Success");
							System.out.println("The response " + wsCall.getExecutionResponse());
							System.out.println("There is no error");
							JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
							if(obj.has("#error"))
							{
								backupGetListStatus = false;
								responseMessage = obj.get("#message").toString();
								
								//if access is denied set the crashflag so that the user is asked to login again
								 if(obj.has("#message")&&(obj.getString("#message")).equalsIgnoreCase("access denied")){
									crashflag=true;
									Session.setSessionId(null);		
								}
							}

							

							else
							{
								if((obj.getInt("response_code")) != 0)
								{
									backupGetListStatus = false;
									responseMessage = obj.getString("response_message");
								}
								else
								{
									jArray = obj.getJSONArray("backups");
									System.out.println("jArray values " + jArray.toString());
									LISTITEM = new String[jArray.length()];

									contentname=new String[jArray.length()];
									starttime=new String[jArray.length()];


									JSONObject jObj; 
									for(int i=0;i<jArray.length();i++)
									{
										jObj = jArray.getJSONObject(i);

										LISTITEM[i] = (i+1)+"."+jObj.getString("backup_name");

										//saving the time and content type for every backup 
										contentname[i]=jObj.getString("content_categories");
										starttime[i]=jObj.getString("start_time");

										//append the time of the backup returned by the WS to the list of backup names
										Calendar cal=new GregorianCalendar();
										cal.setTimeInMillis(Long.parseLong(starttime[i])*1000);

										LISTITEM[i]+=" ["+DateFormat.getDateInstance().format(new Date(Long.parseLong(starttime[i])*1000));

										LISTITEM[i]+="  "+cal.get(Calendar.HOUR_OF_DAY)+":";

										if(cal.get(Calendar.MINUTE)<10)
											LISTITEM[i]+="0"+cal.get(Calendar.MINUTE)+":";

										else
											LISTITEM[i]+=cal.get(Calendar.MINUTE)+":";

										if(cal.get(Calendar.SECOND)<10)
											LISTITEM[i]+="0"+cal.get(Calendar.SECOND)+"]";

										else
											LISTITEM[i]+=cal.get(Calendar.SECOND)+"]";

										LISTITEM[i]+="\n"+contentname[i];

									}

								}
							}
						} else {
							responseMessage = getText(R.string.network_message).toString();
						}
					} 

					else
						crashflag=true;
				}catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				messageHandler.sendEmptyMessage(0);
				//				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}
}