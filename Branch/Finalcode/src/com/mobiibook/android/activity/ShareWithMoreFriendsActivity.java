/**
 * ShareWithMoreFriendsActivity.java
 * @author Prateek Jain
 */
package com.mobiibook.android.activity;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.utils.Validation;


/**
 * Class responsible for handling the screen shown when "Add friends" is selected on the Share screen
 * @author Nitin Singh
 *
 */
public class ShareWithMoreFriendsActivity extends OptionsActivity {

	/**
	 * Object reference to the EditText where the user enters the comma seperated list of friends to share the data with
	 */
	private EditText emailList;
	
	/**
	 * The JSON Array of email ids of the list of friends to share the data with
	 */
	JSONArray jArray;
	
	
	
	/** Called when the activity is first created. 
	 * @author Prateek*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.sharewithmorefriends);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Add Friends To Share");
		}
	}
	
	
	
	/** Generates the list of email id's to which file is to be shared 
	 * @param view
	 * view object gives all the layout elements of the screen.
	 * @author Prateek*/
	public void share_method(View view)
	{
		switch (view.getId()) {
		case R.id.Button01:
			Bundle bundle = this.getIntent().getExtras();
			final String path = bundle.getString("path");
			System.out.println("The path " + path);
			final String emailList = bundle.getString("emailList");
			try {
				if(emailList == null)
				{
					jArray = new JSONArray();
				}
				else
				{
					jArray = new JSONArray(emailList);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(separateByComma())
			{
				Intent explicitIntent = new Intent(ShareWithMoreFriendsActivity.this,ShareFileActivity.class);
				explicitIntent.putExtra("path", path);
				explicitIntent.putExtra("emailList", jArray.toString());
				explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(explicitIntent);
			}
			else
			{
				showResponse(response);
			}
			break;
		}
	}

	
	
	/** Generates an array of emailId's for comma separated values.
	 * Checks for the validation of emailId 
	 * @return boolean 
	 * returns true if email ids are valid else false
	 * @author Prateek*/
	public boolean separateByComma()
	{
		emailList = (EditText)findViewById(R.id.EditText01);
		String list = emailList.getText().toString().trim();
		String[] splitArray = null;
		if (list != null || !list.equalsIgnoreCase("")){
			splitArray = list.split(",");
			System.out.println(list + " " + splitArray);
		}
		Validation v = new Validation();
		for(int i = 0;i<splitArray.length;i++)
		{
			if(v.validateEmail(splitArray[i].trim()))
				jArray.put(splitArray[i].trim());
			else
			{
				response = getText(R.string.entervalidemail).toString();
				return false;
			}
		}
		return true;
	}
}