/**
 * ListSchedulesActivity.java
 * @author Nitin Singh
 */

package com.mobiibook.android.activity;

import java.util.List;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mobiibook.android.activity.R;
import com.mobiibook.android.service.DaemonBackupService;
import com.mobiibook.android.utils.SchedulesDataHelper;

/**
 * Class responsible for handling the screen showing the list of scheduled back ups
 * @author Nitin
 *
 */
public class ListSchedulesActivity extends OptionsActivity{

	/**
	 * Object for listview used to show the list of scheduled backups on the screen
	 */
	private ListView listview;
	
	/**
	 * Array of names of all the Scheduled backups under a particular user
	 */
	String schedulelist[];
	
	/**
	 * Array of details of all the Scheduled backups under a particular user
	 */
	String scheduledetails[];
	
	/**
	 * The message to be shown to the user on screen in case of some errorneous condition
	 */
	String response="";
	
	/**
	 * Flag that is set when either the schedules can't be retrieved or there are no schedules so that clicking on  OK on the dialog shown on screen takes him to the previous screen
	 */
	boolean goToPreviousScreen = false;
	
	/**
	 * Object for an entry point into the SchedulesDataHelper class under utils category which handles all interactions with the database
	 */
	SchedulesDataHelper dataHelper;

	/**
	 * Object used to cancel the corresponding alarm that has been set in case the user chooses to remove a particular schedule
	 */
	PendingIntent pendingIntent;
	
	/**
	 * The string storing the Preference file name used for storing user credentials
	 */
	public static final String PREFS_NAME = "MobiibookPrefsFile";
	
	/**
	 * The string used to refer the username in the Preference file used to store user credentials
	 */
	private static final String PREF_USERNAME = "username";
	
	/**
	 * Contains the user name of the currently logged in user
	 */
	String username;

	
	
	/**
	 * This is the first function to be called when activity is created
	 */
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		setContentView(R.layout.bottombar);

		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Schedules");
		}

		if(!inflateListFromDB()){
			showResponse(response);
		}
	}


	
	/**
	 *This function gets all the data from the db and uses it to inflate the list on the screen
	 * @return  false if the database has no entries in it or read on it fails else returns true 
	 */
	public boolean inflateListFromDB(){
		int count=0;

		SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);   
		username = pref.getString(PREF_USERNAME, null);
		
		dataHelper=new SchedulesDataHelper(this);
		List<String> names = dataHelper.query(username);

		schedulelist=new String[names.size()];
		scheduledetails=new String[names.size()];

		dataHelper.closeDB();

		if(names==null || schedulelist==null)
		{
			response="Schedules could not be retreived";
			goToPreviousScreen = true;
			return false; 
		} 
		else if (names.size() < 1)
		{
			response="No schedules added";
			goToPreviousScreen = true;
			return false;
		}

		else{
			int i=1;
			for (String temp : names){
				//System.out.println(temp+"\n");
				scheduledetails[count]=temp;
				schedulelist[count]=(i++)+"."+temp.subSequence(temp.indexOf("\n")+1,temp.indexOf("\n", temp.indexOf("\n")+1)).toString();
				count++;
			}

			listview=(ListView)findViewById(R.id.ListView01);
			listview.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , schedulelist));
			listview.setCacheColorHint(00000000); // On scrolling the background of the list view doesn't change

			listview.setOnItemClickListener(new OnItemClickListener() {  

				public void onItemClick(AdapterView parent, View v,int position, long id) {

					final int selecteditemposition=position;

					AlertDialog.Builder builder = new AlertDialog.Builder(ListSchedulesActivity.this);
					String details = "Schedule Backup Details:\n\n";
					String[] arr = scheduledetails[position].split("\n");
					if(scheduledetails[position].contains("Daily"))
					{
						details += "Name : " + arr[1] + "\n";
						details += "Type : " + arr[3] + "\n";
						details += "Time : " + arr[2] + "\n";
						details += "Content : ";
						
						if(arr[4].contains("all"))
							details+="All,";
						if(arr[4].contains("audio"))
							details+="Music,";
						if(arr[4].contains("video"))
							details+="Video,";
						if(arr[4].contains("image"))
							details+="Pictures,";
						if(arr[4].contains("calendar"))
							details+="Calendar,";
						if(arr[4].contains("contacts"))
							details+="Contacts,";
						if(arr[4].contains("textmessages"))
							details+="Text Messages,";
						if(arr[4].contains("applications"))
							details+="Applications,";
					}
					else if(scheduledetails[position].contains("Weekly"))
					{
						details += "Name : " + arr[1] + "\n";
						details += "Type : " + arr[3] + "\n";
						details += "Day : " + arr[5] + "\n";
						details += "Time : " + arr[2] + "\n";
						details += "Content : ";
						
						if(arr[4].contains("all"))
							details+="All,";
						if(arr[4].contains("audio"))
							details+="Music,";
						if(arr[4].contains("video"))
							details+="Video,";
						if(arr[4].contains("image"))
							details+="Pictures,";
						if(arr[4].contains("calendar"))
							details+="Calendar,";
						if(arr[4].contains("contacts"))
							details+="Contacts,";
						if(arr[4].contains("textmessages"))
							details+="Text Messages,";
						if(arr[4].contains("applications"))
							details+="Applications,";
						
					}
					builder.setMessage("" + details.substring(0, details.length()-1))
					.setCancelable(false)
					.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							//function call to delete selected item from db 
							dataHelper=new SchedulesDataHelper(ListSchedulesActivity.this);
							int sch_id=Integer.parseInt(scheduledetails[selecteditemposition].substring(0,scheduledetails[selecteditemposition].indexOf("\n")));

							System.out.println("sch id >>>>>>>>>>>>>>>>>"+sch_id);

							if(dataHelper.deleteEntryUsingID(sch_id)){
								dataHelper.closeDB();

								//remove alarm
								removeAlarm(sch_id);
							}
							else {
								response="Unable to delete schedule ... Try again";
								showResponse(response);
							}


						}
					})
					.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							//just cancel the dialog
							dialog.cancel();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();

				}

			});

		}

		return true;
	}



	/**
	 * This function is responsible for displaying a dialog box in case any notification is to be shown to the user
	 * @author Nitin
	 * @param String response 
	 */
	public void showResponse(String response){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		if(goToPreviousScreen)
		{
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					Intent explicitIntent = new Intent(ListSchedulesActivity.this,BackupTypeSelectionActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //HomeScreen is made top of the stack. No other activity reside on the top of HomeScreen 
					startActivity(explicitIntent);
				}
			});

		}
		else
		{
			builder.setMessage(response)
			.setCancelable(false)
			.setInverseBackgroundForced(true)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {

				}
			});
		}

		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();


	}


//	/**
//	 * Override the Back button pressed event
//	 * If backup / restore is running, prompt for the backup/ restore cancellation message\
//	 * logout from the application
//	 * @author Prateek
//	 */
//	@Override
//	public void onBackPressed()
//	{
//		dataHelper.closeDB();
//		Intent explicitIntent=new Intent(ListSchedulesActivity.this,BackupTypeSelectionActivity.class);
//		startActivity(explicitIntent);
//	}


	/**
	 * Function responsible for removing the alarm that was set corresponding to a particular scheduled backup in case the user has chosen to remove that scheduled backup
	 * @param id 
	 * Each alarm that is set has a unique ID associated with it, this field contains the ID for the alarm that needs to be removed
	 */
	public void removeAlarm(final int id){


		Intent myIntent = new Intent(getApplicationContext(),DaemonBackupService.class);
		myIntent.putExtra("backupname","");
		myIntent.putExtra("contenttype","" );
		myIntent.putExtra("contentname","");
		myIntent.putExtra("backuptype","");

		//myIntent.putExtra("smallstring","I come from another activity...again and yet again"+count);

		pendingIntent = PendingIntent.getService(getApplicationContext(),id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmManager.cancel(pendingIntent);

		System.out.println("alarm with id>>>>>>>>>> "+id+"has been deleted");

		//repopulate the list
		//inflateListFromDB();
		if(!inflateListFromDB()){
			showResponse(response);
		}
	}

}