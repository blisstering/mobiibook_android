package com.blisstering.mobiibook.aidl;

interface BackupService {
  boolean cancelBackup();
  void resumeBackup();
  void sendUpdates();
}
