/*
 * Used Android Source Code
 */
package com.blisstering.mobiibook.android.pim.vcard.exception;

/**
 * VCardException used only when the version of the vCard is different.
 */
public class VCardVersionException extends VCardException {
    public VCardVersionException() {
        super();
    }
    public VCardVersionException(String message) {
        super(message);
    }
}