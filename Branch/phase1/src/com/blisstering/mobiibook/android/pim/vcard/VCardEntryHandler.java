/*
 * Used Android Source Code
 */
package com.blisstering.mobiibook.android.pim.vcard;

/**
 * The interface called by {@link VCardEntryConstructor}. Useful when you don't want to
 * handle detailed information as what {@link VCardParser} provides via {@link VCardInterpreter}.
 */
public interface VCardEntryHandler {
    /**
     * Called when the parsing started.
     */
    public void onStart();

    /**
     * The method called when one VCard entry is successfully created
     */
    public void onEntryCreated(final VCardEntry entry);

    /**
     * Called when the parsing ended.
     * Able to be use this method for showing performance log, etc.
     */
    public void onEnd();
}