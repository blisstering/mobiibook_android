package com.blisstering.mobiibook.utils;
/*
 * All the sessions variables are maintained over here
 */
public class Session {
	private static String sessionId;
	private static String userRole;
	private static boolean isBackupServiceRunning = false;
	private static boolean isRestoreServiceRunning = false;
	//private static UserProfile userProfile;
	//private static ManualBackUpScreen manualBackUpScreen;

	/**
	 * Sets the session Id for the session
	 * @param String sessionId
	 * Session Id
	 * @author Prateek
	 */
	public static void setSessionId(String sessionId) {
		Session.sessionId = sessionId;
	}

	/**
	 * Gets the session Id for the session
	 * @return String
	 * Returns the session id
	 * @author Prateek
	 */
	public static String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the user role for the session
	 * @param String userRole
	 * User role, given by login web service
	 * @author Prateek
	 */
	public static void setUserRole(String userRole) {
		Session.userRole = userRole;
	}

	/**
	 * Returns the userRole of the session
	 * @return String userRole
	 * User Role
	 * @author Prateek
	 */
	public static String getUserRole() {
		return userRole;
	}
	
	/**
	 * Returns whether backup is running or not
	 * @return boolean
	 * @author Prateek
	 */
	public static boolean isBackupServiceRunning()
	{
		return isBackupServiceRunning;
	}
	
	/**
	 * Returns whether restore is running or not
	 * @return boolean
	 * @author Prateek
	 */
	public static boolean isRestoreServiceRunning()
	{
		return isRestoreServiceRunning;
	}
	
	/**
	 * Sets the boolean for backup is in progress
	 * @param boolean value
	 * true or false
	 * @author Prateek
	 */
	public static void setBackupService(boolean value)
	{
		isBackupServiceRunning = value;
	}
	
	/**
	 * Sets the boolean for restore is in progress
	 * @param boolean value
	 * true or false
	 * @author Prateek
	 */
	public static void setRestoreService(boolean value)
	{
		isRestoreServiceRunning = value;
	}

//	public static void setUserProfile(UserProfile userProfile) {
//		Session.userProfile = userProfile;
//	}
//
//	public static UserProfile getUserProfile() {
//		return userProfile;
//	}

	/*public static void setManualBackUpScreen(ManualBackUpScreen manualBackUpScreen) {
		Session.manualBackUpScreen = manualBackUpScreen;
	}

	public static ManualBackUpScreen getManualBackUpScreen() {
		return manualBackUpScreen;
	}*/
	

	
}
