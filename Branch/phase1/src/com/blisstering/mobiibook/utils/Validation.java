package com.blisstering.mobiibook.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

	
	 private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	 private static final String PHONE_NUMBER_PATTERN="[0-9]*";
	/**
	 * Validate hex with regular expression
	 * @param String email
	 * email address
	 * @return true valid hex, false invalid hex
	 */
	public boolean validateEmail(final String email){
		 Pattern pattern;
		 Matcher matcher;

		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();

	}
	
	public boolean containsOnlyDigits(final String phonenumber){
		Pattern pattern;
		Matcher matcher;

		pattern = Pattern.compile(PHONE_NUMBER_PATTERN);
		matcher = pattern.matcher(phonenumber);
		return matcher.matches();
	}
}
