package com.blisstering.mobiibook.activity;

import java.net.URLEncoder;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;

import com.blisstering.mobiibook.service.BackupService;
import com.blisstering.mobiibook.service.RestoreService;
import com.blisstering.mobiibook.utils.Session;

public class ListOptionsActivity extends ListActivity{
	private static final int MENU_LOGOUT = Menu.FIRST;
	ProgressDialog progressDialog;
	String response = "";
	boolean logoutSuccessful = false;
	//private static final int MENU_QUIT = Menu.FIRST + 1;
	public boolean onCreateOptionsMenu(Menu menu) {    
		menu.add(0, MENU_LOGOUT, 0, "Logout");    
		//menu.add(0, MENU_QUIT, 0, "Quit");    
		return true;
	}
	/* Handles item selections */
	public boolean onOptionsItemSelected(MenuItem item) {    
		switch (item.getItemId()) {    
		case MENU_LOGOUT:        
			logout();        
			return true;    
		}    
		return false;
	}

	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog.dismiss();
			if(!logoutSuccessful)
				showResponse(response);
			else
			{
				Intent explicitIntent = new Intent(ListOptionsActivity.this,login.class);
				explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(explicitIntent);
			}

		}
	};

	public void showResponse(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}
	public void logout()
	{
		String msg;
		boolean showDialog = false;
		if(Session.isBackupServiceRunning())
		{
			msg = getText(R.string.logoutmessagewithbackup).toString();
			showDialog = true;
		}
		else if(Session.isRestoreServiceRunning())
		{
			msg = getText(R.string.logoutmessagewithrestore).toString();
			showDialog = true;
		}
		else
			msg = "";

		if(showDialog)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(msg)
			.setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					if(Session.isBackupServiceRunning())
					{
						Intent i = new Intent(ListOptionsActivity.this,BackupService.class);
						stopService( i );
					}
					else if(Session.isRestoreServiceRunning())
					{
						Intent i = new Intent(ListOptionsActivity.this,RestoreService.class);
						stopService( i );	
					}
					logoutWS();
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
		}
		else
			logoutWS();
	}
	public void logoutWS()
	{
		progressDialog = ProgressDialog.show(this, "", "Please wait... Logging out");
		new Thread() {	
			public void run() {
				try {
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.logout", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Calling web service");
					//wsCall.start();
					//System.out.println("Waiting for thread to die");
					//wsCall.join();
					//WebServiceCall castedThread = (WebServiceCall) wsCall;
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						logoutSuccessful = true;
						Session.setSessionId(null);
					} else {
						System.out.println("The value is " + getText(R.string.network_message).toString());
						response = getText(R.string.network_message).toString();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}
}
