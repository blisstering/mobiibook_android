package com.blisstering.mobiibook.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blisstering.mobiibook.service.BackupService;
import com.blisstering.mobiibook.utils.Session;

public class BackupServiceActivity extends OptionsActivity{

	private com.blisstering.mobiibook.aidl.BackupService backupService;
	private static final String LOG_TAG = "BACKUPSERVICECLIENT";
	private BackupServiceConnection conn;
	private boolean serviceStarted = false;
	ProgressBar progressBar;
	ProgressDialog progressDialog;
	String response;
	boolean isNetworkException = false;
	TextView dateTV, timeTV, contentsTV,contentDetailsTV;
	
	/** Called when the activity is first created. 
	 * If the backup service is already running fetch do not start the service again
	 * If not then start the backup service
	 * @author Prateek
	 * */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.backupserviceactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Backup");
		}
		serviceStarted = Session.isBackupServiceRunning();
		dateTV = (TextView)findViewById(R.id.TextView02);
		timeTV = (TextView)findViewById(R.id.TextView04);
		contentsTV = (TextView)findViewById(R.id.TextView06);
		contentDetailsTV = (TextView)findViewById(R.id.TextView08);
		dateTV.setText("Updating...");
		timeTV.setText("Updating...");
		contentsTV.setText("Updating...");
		contentDetailsTV.setText("Updating...");
		if(!serviceStarted)
		{
			startService();
			//		invokeService();
		}
		initService();
		//Receiver is registerd with the service here
		registerReceiver(receiver,new IntentFilter(BackupService.BROADCAST_ACTION));
		ImageButton cancel = (ImageButton)findViewById(R.id.Button01);
		cancel.setOnClickListener(cancelListener);
		progressBar = (ProgressBar)findViewById(R.id.ProgressBar01);
		progressBar.setClickable(false);
		progressBar.setProgress(0);
	}

	/** 
	 * On activity destruction unregister the receiver
	 * Unbind / Release the backup service
	 * @author Prateek
	 * */
	protected void onDestroy()
	{
		unregisterReceiver(receiver);
		releaseService();
		super.onDestroy();
	}

	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * Displays the message and dismiss the progressdialog 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try{

				if(isNetworkException)
					showResponse(getText(R.string.backupresume).toString(), 1);
				else
				{
					releaseService();
					stopService();
					showResponse(response, 0);
				}
				if(progressDialog != null)
					progressDialog.dismiss();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	};

	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response,int value)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		AlertDialog alert;
		switch(value)
		{
		case 0:
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					Intent explicitIntent = new Intent(BackupServiceActivity.this,HomeScreenActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			});
			alert = builder.create();
			alert.setTitle("Notification");
			alert.show();
			break;
		case 1:
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					try{
						backupService.resumeBackup();
					}catch(Exception e)
					{
						showResponse(getText(R.string.backupabort).toString(), 0);
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					releaseService();
					stopService();
					dialog.cancel();
					Intent explicitIntent = new Intent(BackupServiceActivity.this,HomeScreenActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			});
			alert = builder.create();
			alert.show();

		}
	}

	/**
	 * On click listener to cancel backup
	 * @author Prateek
	 */
	private OnClickListener cancelListener = new OnClickListener() {
		public void onClick(View v){
			if(conn != null)
			{
				showCancelSplash();
				try {
					if(backupService == null)
					{
						showResponse("No backup in progress",0);
					}
					else if(!backupService.cancelBackup())
					{
						if(progressDialog != null && progressDialog.isShowing())
							progressDialog.dismiss();
						showResponse(getText(R.string.backupabort).toString(),0);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}	        	
	};

	/**
	 * Shows the cancelling backup progress dialog
	 * @author Prateek
	 */
	public void showCancelSplash()
	{
		progressDialog = ProgressDialog.show(this, "", "Cancelling.....");
	}

	/**
	 * Class to create a connetion to the backup service
	 * @author Prateek
	 *
	 */
	class BackupServiceConnection implements ServiceConnection {
		/**
		 * When service is connected this function is called
		 * @author Prateek
		 */
		public void onServiceConnected(ComponentName className, 
				IBinder boundService ) {
			backupService = com.blisstering.mobiibook.aidl.BackupService.Stub.asInterface((IBinder)boundService);
			Log.d( LOG_TAG,"onServiceConnected" );	
			try {
				backupService.sendUpdates();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			invokeService();
		}

		/**
		 * When service is dis-connected this function is called
		 * @author Prateek
		 */
		public void onServiceDisconnected(ComponentName className) {
			backupService = null;
			//unregisterReceiver(receiver);
			Log.d( LOG_TAG,"onServiceDisconnected" );
			//updateServiceStatus();
		}
	};

	/**
	 * Binds the service with the connection object
	 * @author Prateek
	 */
	private void initService() {
		if( conn == null ) {
			conn = new BackupServiceConnection();
//			Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
//			i.putExtra("key", "value");
			Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
			//i.setClassName("com.blisstering.mobiibook.service", "com.blisstering.mobiibook.service.BackupService");
			//boolean result = this.bindService( i, conn,0);
			boolean result = getApplicationContext().bindService( i, conn,0);
			Log.d( LOG_TAG, "BindService:" + result);
			if(conn == null)
				Log.d( LOG_TAG, "conn is null" );
			else
				Log.d( LOG_TAG, "Conn not null" );	

			Log.d( LOG_TAG, "bindService()" );
		} else {
//			NotificationManager nm = (NotificationManager)
//			getSystemService( Context.NOTIFICATION_SERVICE);
//			nm.notifyWithText(123, "Cannot bind - service already bound",
//			NotificationManager.LENGTH_LONG, null );
		}
	}

	/**
	 * Unbinds the service from the connection object
	 * @author Prateek
	 */
	private void releaseService() {
		try{
			if( conn != null ) {
				getApplicationContext().unbindService( conn );
				conn = null;
				//updateServiceStatus();
				Log.d( LOG_TAG, "unbindService()" );
			} else {
//				NotificationManager nm = (NotificationManager)
//				getSystemService( Context.NOTIFICATION_SERVICE);	
//				nm.notifyWithText(123, "Cannot unbind - service not bound",
//				NotificationManager.LENGTH_LONG, null );
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Starts the backup service
	 * @author Prateek
	 */
	private void startService() {
//		if( started ) {
//		NotificationManager nm = (NotificationManager)
//		getSystemService( Context.NOTIFICATION_SERVICE);
//		nm.notifyWithText(123, "Service already started",
//		NotificationManager.LENGTH_LONG, null );
//		} else {
//		exportContact();
		Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
		Bundle bundle = this.getIntent().getExtras();
		final String backupName = bundle.getString("backupName");
		i.putExtra("backupName", backupName);
		//i.setClassName( "com.blisste;ring.mobiibook.service", "BackupService" );
		//Bundle b = new Bundle();
		//startService( i,b );
		startService(i);
		Log.d( LOG_TAG, "startService()" );
//		started = true;
//		updateServiceStatus();
		//}
	}

	/**
	 * Stops the backup service
	 * @author Prateek
	 */
	private void stopService() {
//		if( !started ) {
//		NotificationManager nm = (NotificationManager)
//		getSystemService( Context.NOTIFICATION_SERVICE);
//		nm.notifyWithText(123, "Service not yet started",
//		NotificationManager.LENGTH_LONG, null );
//		} else {
		Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
		//i.setClassName( "aexp.dualservice", 
		//	"aexp.dualservice.DualService" );
		stopService( i );
		Log.d( LOG_TAG, "stopService()" );
//		started = false;
//		updateServiceStatus();
//		}
	}

//	private void invokeService() {
////	if( conn == null ) {
////	NotificationManager nm = (NotificationManager)
////	getSystemService( Context.NOTIFICATION_SERVICE);
////	nm.notifyWithText(123, "Cannot invoke - service not bound",
////	NotificationManager.LENGTH_LONG, null );
////	} else {
//	Bundle bundle = this.getIntent().getExtras();
//	final String backupName = bundle.getString("backupName");
//	new Thread() {
//	public void run() {
//	try {
//	exportContact();
//	backupService.startBackup(backupName);
//	} catch( Exception ex ) {
//	Log.e( LOG_TAG, "DeadObjectException" );
//	}
//	}
//	}.start();
//	}
//	}

	/**
	 * Receiver to get the details broadcasted by Backup Service
	 * @author Prateek
	 */
	private BroadcastReceiver receiver=new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			final int value = bundle.getInt("progress");
			System.out.println("the value is " + value);
			updateProgressBar(value);
			updateDisplay(bundle.getString("date"), bundle.getString("time"), bundle.getString("contents"), bundle.getString("contentDetails"));
			if(bundle.getBoolean("backupover") || bundle.getBoolean("isNetworkException"))
			{
				response = bundle.getString("response");
				isNetworkException = bundle.getBoolean("isNetworkException");
				messageHandler.sendEmptyMessage(0);
			}
		}

	};
	
	/**
	 * To update the progress bar on the screen
	 * @author Prateek
	 */
	public void updateProgressBar(int value)
	{
		progressBar.setProgress(value);
	}

	/**
	 * To update the other details of the backup on the screen
	 * @author Prateek
	 */
	public void updateDisplay(String date, String time, String contents, String contentDetails)
	{
		dateTV.setText(date);
		timeTV.setText(time);
		contentsTV.setText(contents);
		contentDetailsTV.setText(contentDetails);
	}

}
