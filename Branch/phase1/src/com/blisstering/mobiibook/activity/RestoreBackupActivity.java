package com.blisstering.mobiibook.activity;

import com.blisstering.mobiibook.restore.RestoreContentsThread;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class RestoreBackupActivity extends OptionsActivity{
	
	ProgressDialog progressDialog;
	RestoreContentsThread restoreContentsThread;
	String threadResponse;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startRestore();
	}
	
	
	public void showResponse(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		       .setCancelable(false)
		       .setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();
		
	}
	
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog.dismiss();
			showResponse(threadResponse);
		}
	};

	public void startRestore() {
		Bundle bundle = this.getIntent().getExtras();
		final String backup_id = bundle.getString("backup_id");
		System.out.println("The backup_id " + backup_id);
		progressDialog = ProgressDialog.show(this, "", "Restoring data.....");
		new Thread() {
			public void run() {
				//Hardcoding
				restoreContentsThread = new RestoreContentsThread(backup_id);
				//threadResponse = restoreContentsThread.startRestore();
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}
	
}
