package com.blisstering.mobiibook.activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListViewTest extends ListActivity {

	final String[] listContent = { "0", "1", "2", "3", "1", "2", "3", "1", "2", "3" };
	ListView list;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_list);
		list = (ListView) findViewById(android.R.id.list);
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_multiple_choice, listContent));
		list.setItemsCanFocus(false);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		SparseBooleanArray checked = list.getCheckedItemPositions();
		for (int i = 0; i < listContent.length; i++) {
			Log.i("ListViewTest", listContent[i] + ": " + checked.get(i));
		}
	}

}