package com.blisstering.mobiibook.activity;

import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.blisstering.mobiibook.utils.Session;

public class RestoreActivity extends OptionsActivity{
	String[] LISTITEM = new String[0];
	//ProgressDialog progressDialog;
	boolean backupGetListStatus = false;
	String responseMessage;
	JSONArray jArray;
	private ListView lv1;
	
	
	/** Called when the activity is first created. 
	 * If the backup service is already running fetch do not start the service again
	 * If not then start the backup service
	 * @author Prateek
	 * */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setListAdapter(new ArrayAdapter(this, R.layout.homescreenactivity, WORLDCUP2010));
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		backupGetListWS();
		if(LISTITEM == null)
		{
			LISTITEM = new String[1];
			LISTITEM[0] = responseMessage;
		}
		lv1=(ListView)findViewById(R.id.ListView01);
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		lv1.setCacheColorHint(00000000);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Backup List");
		}
		//getListView().setTextFilterEnabled(true);
		lv1.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {
				// When clicked, show a toast with the TextView text
				Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
						Toast.LENGTH_SHORT).show();
				System.out.println("The position is " + position);
				System.out.println("The id is " + id);
				//Intent explicitIntent = new Intent(RestoreActivity.this,RestoreBackupActivity.class);
				if(isexternalStorageAvailable())
				{
					Intent explicitIntent = new Intent(RestoreActivity.this,RestoreServiceActivity.class);
					Bundle bundle = new Bundle();
					try{
						JSONObject jObj = jArray.getJSONObject(position); 
						bundle.putString("backup_id",jObj.getString("backup_id"));
						bundle.putString("backup_name", jObj.getString("backup_name"));
						explicitIntent.putExtras(bundle);
						startActivity(explicitIntent);
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					showResponse(getText(R.string.nosdcardfound).toString(),false);
				}
			}
		});
//		progressDialog.dismiss();
	}

	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @param boolean finishActivity
	 * If true, the activity is finished after dialog box is shown
	 * else just a dialog box is shown on the screen
	 * @author Prateek
	 */
	public void showResponse(String response,final boolean finishActivity)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.finish();
				if(finishActivity)
				{
					setResult(RESULT_OK);
					finish();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}

	/** Called when Activity is in Paused state 
	 * @author Prateek
	 * */
	@Override
	public void onPause()
	{
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		super.onPause();
	}
	
	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if(progressDialog != null && progressDialog.isShowing())
				progressDialog.dismiss();
			if(!backupGetListStatus)
			{
				showResponse(responseMessage,true);
			}
			else
			{
				updateListView();
			}
		}
	};

	/**
	 * Updates the list on the screen
	 * @author Prateek
	 */
	public void updateListView()
	{
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
	}
	
	/**
	 * Get the list of backups from the backup.getlist web service
	 * @author Prateek
	 */
	public void backupGetListWS()
	{
		progressDialog = ProgressDialog.show(this, "", "Getting backup list...");
		new Thread() {
			public void run() {
				try {
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.getlist", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					//HardCoding for Android Client ID
					data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Calling web service");
					backupGetListStatus = wsCall.isThreadSucceeded();
					if (backupGetListStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							backupGetListStatus = false;
							responseMessage = obj.get("#message").toString();
						}
						else
						{
							if((obj.getInt("response_code")) != 0)
							{
								backupGetListStatus = false;
								responseMessage = obj.getString("response_message");
							}
							else
							{
								jArray = obj.getJSONArray("backups");
								System.out.println("jArray values " + jArray.toString());
								LISTITEM = new String[jArray.length()];
								JSONObject jObj; 
								for(int i=0;i<jArray.length();i++)
								{
									jObj = jArray.getJSONObject(i);
									LISTITEM[i] = jObj.getString("backup_name");
								}
								//setListAdapter(new ArrayAdapter<String>(null, 0, LISTITEM));
//								System.out.println(jArray.get(0));

//								System.out.println("backup id " + jObj.get("backup_id"));
//								System.out.println("backup name " + jObj.get("backup_name"));
							}
						}
					} else {
						responseMessage = getText(R.string.network_message).toString();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				messageHandler.sendEmptyMessage(0);
//				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}
}