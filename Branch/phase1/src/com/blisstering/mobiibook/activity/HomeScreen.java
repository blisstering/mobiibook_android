package com.blisstering.mobiibook.activity;


import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class HomeScreen extends Activity{


	static final String BOUNDARY = "----------V2ymHFg03ehbqgZCaKO6jy";
	public static final int PICK_CONTACT  = 1;
	private ListView lv1;
	private String lv_arr[]={"Android","iPhone","BlackBerry","AndroidPeople","Android","iPhone","BlackBerry","AndroidPeople","Android","iPhone","BlackBerry","AndroidPeople"};
	@Override
	public void onCreate(Bundle icicle)
	{
		super.onCreate(icicle);
		setContentView(R.layout.homescreen);
		lv1=(ListView)findViewById(R.id.ListView01);
		// By using setAdpater method in listview we an add string array in list.
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.homescreenactivity , lv_arr));
	}



	/* To display Contact List */
	public void readContactsIntent(View view)
	{

		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
		startActivityForResult(intent, PICK_CONTACT);
	}


	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
		super.onActivityResult(reqCode, resultCode, data);  

		switch (reqCode) {  
		case (PICK_CONTACT):  
			if (resultCode == Activity.RESULT_OK) {  
				Uri contactData = data.getData();  
				Cursor c = managedQuery(contactData, null, null, null, null);  
				if (c.moveToFirst()) {  
					String name = c.getString(c.getColumnCount());  
					System.out.println(name);  
				}  
			}  
		break;  
		}  
	}  

	/* Read the contacts of the Phone */

	public void readContacts(View view) {
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		if (cur.getCount() > 0) {
			try{
				String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
				File vCardFile = new File(extStorageDirectory,"/contacts.vcf");
				OutputStream os = new FileOutputStream(vCardFile);

				while (cur.moveToNext()) {
					try{
						String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
						Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
						System.out.println("The value is " + cr.getType(uri));
						AssetFileDescriptor fd = this.getContentResolver().openAssetFileDescriptor(uri, "r");
						FileInputStream fis = fd.createInputStream();
						byte[] b = new byte[1024*4];  
						int read;  
						while ((read = fis.read(b)) != -1) {  
							os.write(b, 0, read);  
						} 
//						byte[] buf = new byte[(int)fd.getDeclaredLength()];
//						if (0 < fis.read(buf))
//						{
//						String vCard = new String(buf);
//						System.out.println("The vCard value is " + vCard);
//						}
						fis.close();
					}
					catch(Exception e)
					{
						System.out.println(e.getStackTrace());
					}
					finally {
						cur.close();
					}
				}
				os.flush();
				os.close();	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}
	}

	/* Read audios from the phone */

	public void readAudio(View view)
	{
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		if(mExternalStorageAvailable)
		{
			File file[] = Environment.getExternalStorageDirectory().listFiles();  
			System.out.println("The length of files is " + file.length);

			for(int i=0;i<file.length;i++)
			{
				System.out.println("The file paths " + i + "=" + file[i].getAbsolutePath());
				if(!file[i].isDirectory())
					transfer(file[i].getAbsolutePath());
			}

		}
		else
		{
			System.out.println("The Media is not available to read");
		}

	}


	//To Restore the contact

	public void restoreContact(View view)
	{
		ContentValues values = new ContentValues(); 
//		values.put(RawContacts.ACCOUNT_TYPE, null); 
//		values.put(RawContacts.ACCOUNT_NAME, null); 
		Uri rawContactUri = getContentResolver().insert(RawContacts.CONTENT_URI, 
				values); 
		long rawContactId = ContentUris.parseId(rawContactUri); 
		values.clear(); 
		values.put(Data.RAW_CONTACT_ID, rawContactId); 
		values.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE); 
		values.put(StructuredName.DISPLAY_NAME, "Prateek1"); 
		getContentResolver().insert(Data.CONTENT_URI, values);  

		values.clear();
		values.put(Data.RAW_CONTACT_ID, rawContactId);
		values.put(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
		values.put(Data.DATA1, "383839");
		//values.put(Data., Phone.TYPE_MOBILE);
		getContentResolver().insert(Data.CONTENT_URI, values);
//		values.put(Data.RAW_CONTACT_ID, rawContactId);
//		values.put(Data.MIMETYPE, );
//		getContentResolver().insert(Data.CONTENT_URI, values);
	}



	public void transfer(String fileAbsolutePath)
	{

		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;

		String pathToOurFile = fileAbsolutePath;
		String urlServer = "http://bliss-dev.com/mobiibook_android/index.php";
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;

		try
		{
			FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

			outputStream = new DataOutputStream( connection.getOutputStream() );
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile +"\"" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0)
			{
				outputStream.write(buffer, 0, bufferSize);
				outputStream.flush();
				buffer = null;
				System.gc();
				buffer = new byte[bufferSize];
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			System.gc();
			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = connection.getResponseCode();
			String serverResponseMessage = connection.getResponseMessage();

			System.out.println("Response code :" + serverResponseCode);
			System.out.println("Response Message : " + serverResponseMessage);

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
			connection.disconnect();
			connection = null;
			System.gc();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}	

	public void manualBackupScreen(View view)
	{
		Intent explicitIntent = new Intent(HomeScreen.this,ManualBackup.class);
		startActivity(explicitIntent);
	}

	public void transfer1(String fileAbsolutePath)
	{

		Sender sender = new Sender(fileAbsolutePath);
		sender.start();
	}


	class Sender extends Thread {
		private String pathToOurFile;
		Sender(String fileAbsolutePath)
		{
			pathToOurFile = fileAbsolutePath;
		}
		public void run() { 
			String msg = ""; 
			//Hardcoding
			String url = "http://bliss-dev.com/mobiibook_android/index.php";
			File file = new File(pathToOurFile); 
//			File(Environment.getExternalStorageDirectory(), 
//			"test2.dat"); 
			try { 
				HttpClient httpclient = new DefaultHttpClient(); 
				HttpPost httppost = new HttpPost(url); 
				Log.i("SendLargeFile", "before send"); 
				Log.i("SendLargeFile", "file length = " + 
						file.length()); 
				InputStreamEntity reqEntity = new InputStreamEntity( 
						new FileInputStream(file), -1); 
				reqEntity.setContentType("binary/octet-stream"); 
				reqEntity.setChunked(true); 
				httppost.setEntity(reqEntity); 
				Log.i("SendLargeFile", "before execute"); 
				HttpResponse response = httpclient.execute(httppost); 
				Log.i("SendLargeFile", "response = " + 
						response.getStatusLine()); 
				HttpEntity resEntity = 
					response.getEntity(); 
				StringBuilder sb = new StringBuilder(); 
				if (resEntity != null) { 
					byte[] buf = new byte[512]; 
					InputStream is = resEntity.getContent(); 
					int n = 0; 
					while ((n = is.read(buf)) > 0) { 
						sb.append(new String(buf, 0, n)); 
					} 
					is.close(); 
					resEntity.consumeContent(); 
				} 
				msg = sb.toString(); 
			} catch (Exception e) { 
				msg = e.toString(); 
			} 
			System.out.println("The message value" + msg); 
		} 


	}
}
