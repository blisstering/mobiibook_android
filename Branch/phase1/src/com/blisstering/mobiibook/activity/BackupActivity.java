package com.blisstering.mobiibook.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;

import com.blisstering.mobiibook.backup.BackupContentsThread;

public class BackupActivity extends Activity {

	JSONArray jsonFiles = null;
	int fileNumber = 0;
	BackupContentsThread backupContentThread;
	ProgressDialog progressDialog;
	String threadResponse = "";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.backupactivity);
		exportContact();
		startBackup();
	}

	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog.dismiss();
			showResponse(threadResponse);
		}
	};

	public void startBackup() {
		Bundle bundle = this.getIntent().getExtras();
		final String backupName = bundle.getString("backupName");
		progressDialog = ProgressDialog.show(this, "", "Storing data.....");
		new Thread() {
			public void run() {
				//Hardcoding
				backupContentThread = new BackupContentsThread(backupName,"Manual");
				threadResponse = backupContentThread.startBackup();
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}
	
	public void showResponse(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		       .setCancelable(false)
		       .setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();
		
	}
	
	public void exportContact()
	{
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		System.out.println("The count of contacts is " + cur.getCount());
		if (cur.getCount() > 0) {
			try{
				String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
				File vCardFile = new File(extStorageDirectory,"/contacts.vcf");
				if (!vCardFile.exists()) {
                    try {
                            vCardFile.createNewFile();
                    } catch (IOException e) {
                            e.printStackTrace();
                    }
            }
				OutputStream os = new FileOutputStream(vCardFile);

				while (cur.moveToNext()) {
					try{
						String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
						Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, lookupKey);
						System.out.println("The value is " + cr.getType(uri));
						AssetFileDescriptor fd = this.getContentResolver().openAssetFileDescriptor(uri, "r");
						FileInputStream fis = fd.createInputStream();
						byte[] b = new byte[1024*4];  
						int read;  
						while ((read = fis.read(b)) != -1) {  
							os.write(b, 0, read);  
						} 
//						byte[] buf = new byte[(int)fd.getDeclaredLength()];
//						if (0 < fis.read(buf))
//						{
//						String vCard = new String(buf);
//						System.out.println("The vCard value is " + vCard);
//						}
						fis.close();
					}
					catch(Exception e)
					{
						System.out.println(e.getStackTrace());
					}
				}
				os.flush();
				os.close();	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}
	}
}
