package com.blisstering.mobiibook.activity;

import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.blisstering.mobiibook.utils.Session;

public class FriendListActivity extends ListOptionsActivity{
	String[] LISTITEM;
	ProgressDialog progressDialog;
	boolean wsStatus = false;
	String responseMessage;
	JSONArray jArray;
	ListView list;
	
	/** Called when the activity is first created. 
	 * @author Prateek*/
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.friendlistactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Select Friends to Share");
		}
		//setListAdapter(new ArrayAdapter(this, R.layout.homescreenactivity, WORLDCUP2010));
		friendListWS();
		list = (ListView) findViewById(android.R.id.list);
		setListAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_multiple_choice, LISTITEM));
		list.setCacheColorHint(00000000);
		list.setItemsCanFocus(false);
		list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		//setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, LISTITEM));
//		View footer = View.inflate(this, R.layout.sharefooter, null);
//		getListView().addFooterView(footer, null, false);
		//getListView().addFooterView(v, data, isSelectable);
//		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, LISTITEM));
//		//final CheckBox checkbox = (CheckBox) findViewById(R.id.checkbox);
//		final ListView listView = getListView();
//		listView.setItemsCanFocus(false);
//		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//		listView.setTextFilterEnabled(true);
		getListView().setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {
				System.out.println("THe ID is " + id);
				if(id == 0 && list.getCheckedItemPositions().get(0))
				{
					for(int i =1;i<=jArray.length();i++)
					{
						list.setItemChecked(i, true);
					}
				}
				else if(id == 0 && !list.getCheckedItemPositions().get(0))
				{
					for(int i =1;i<=jArray.length();i++)
					{
						list.setItemChecked(i, false);
					}
				}
				else if(id !=0)
				{
					list.setItemChecked(0, false);
				}
			}
		});
		progressDialog.dismiss();
	}


	/** Sharing web service is called 
	 * @author Prateek
	 * */
	public void share_method(View view)
	{
		Intent explicitIntent; 
		Bundle bundle = this.getIntent().getExtras();
		final String path = bundle.getString("path");
		System.out.println("The path is " + path);
		String emailList = getSelectedEmailIdList();
		System.out.println("The emailList is " + emailList);
		switch (view.getId()) {
		case R.id.Button01:
			if(jArray == null)
			{
				showResponse(getText(R.string.nofriend_message).toString(),false);
			}
			else if(emailList == null)
			{
				showResponse(getText(R.string.nofriendselected).toString(),false);
			}
			else
			{
				System.out.println("BuTTON 1 clicked");
				explicitIntent = new Intent(FriendListActivity.this,ShareFileActivity.class);
				explicitIntent.putExtra("path", path);
				explicitIntent.putExtra("emailList", emailList);
				explicitIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(explicitIntent);
			}
			break;
		case R.id.Button02:
			System.out.println("BuTTON 2 clicked");
			explicitIntent = new Intent(FriendListActivity.this,ShareWithMoreFriendsActivity.class);
			explicitIntent.putExtra("path", path);
			explicitIntent.putExtra("emailList", emailList);
			startActivity(explicitIntent);
			break;
		}
	}

	/** Get the list of all the selected checkbox from the list 
	 * @author Prateek
	 * */
	public String getSelectedEmailIdList()
	{
		SparseBooleanArray checked = list.getCheckedItemPositions();
		JSONArray emailList = new JSONArray();
		if(jArray != null)
		{
			for (int i = 0; i < jArray.length(); i++) {
				if(checked.get(i+1))
				{
					try {
						emailList.put(jArray.getJSONObject(i).getString("mail"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				Log.i("ListViewTest", (i+1) + ": " + checked.get(i+1));
			}
			if(emailList.length() > 0)
				return emailList.toString();
			else 
				return null;
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @param boolean finishActivity
	 * If true, the activity is finished after dialog box is shown
	 * else just a dialog box is shown on the screen
	 * @author Prateek
	 */
	public void showResponse(String response,final boolean finishActivity)
	{
		LISTITEM = new String[0];
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.finish();
				if(finishActivity)
				{
					setResult(RESULT_OK);
					finish();
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();
	}

//	private Handler messageHandler = new Handler() {
//
//		public void handleMessage(Message msg) {
//			super.handleMessage(msg);
//			progressDialog.dismiss();
//			if(!wsStatus)
//			{
//				showResponse(responseMessage,false);
//			}
//			else
//			{
//				setListAdapter(new ArrayAdapter<String>(null, 0, LISTITEM));
//			}
//		}
//	};

	/** Called when Activity is in Paused state 
	 * @author Prateek
	 * */
	@Override
	public void onPause()
	{
		if(progressDialog != null && progressDialog.isShowing())
			progressDialog.dismiss();
		super.onPause();
	}
	

	/** To get the list of friends from the web service
	 * @author Prateek
	 * */
	public void friendListWS()
	{
		progressDialog = ProgressDialog.show(this, "", "Getting friend list.....");
//		new Thread() {
//		public void run() {
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("friend.list", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Calling web service");
			wsStatus = wsCall.isThreadSucceeded();
			if (wsStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				//JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				JSONArray jArr = wsCall.getExecutionResponse().getJSONArray("#data");

				if(jArr.getInt(0) == 0)
				{
					jArray = jArr.getJSONArray(1);
					if(jArray.length() == 0)
					{
						showResponse(getText(R.string.nofriend_message).toString(),false);
					}
					else
					{
						LISTITEM = new String[jArray.length() + 1];
						LISTITEM[0] = "All";
						for(int i=0;i<jArray.length();i++)
						{
							JSONObject jOb = jArray.getJSONObject(i);
							String name = jOb.getString("first_name") + ' ' + jOb.getString("last_name");
							//String name = jOb.getString("name");
							LISTITEM[i+1] = name;
						}
					}
				}
				else
				{
					showResponse(jArr.getString(1),false);
				}
			} else {
//				System.out.println("Failure");
				showResponse(getText(R.string.network_message).toString(),true);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			showResponse(getText(R.string.exceptionmessage).toString(),true);
		}
//		messageHandler.sendEmptyMessage(0);
//		}
//		}.start();
	}
}

