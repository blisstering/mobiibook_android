package com.blisstering.mobiibook.activity;

import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView;

import com.blisstering.mobiibook.utils.Session;
import com.blisstering.mobiibook.utils.Validation;

public class RegisterUserActivity extends Activity implements AdapterView.OnItemSelectedListener{

	String registeruserresponse="";
	String emailid,firstname,lastname,phonenumber;
	String countrycode="",country="";

	ProgressDialog progressDialog,progressDialog2;

	JSONArray jArrayCountryCodeList;

	boolean successfullyregisteredflag=false,servicedownflag=false;

	Spinner countrycodespinner,countryspinner;

	private static  String[] countrycodelist;
	private static   String[] countrylist;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/** Custom title bar of the screen */
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.registeruseractivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Registration ");
		}


		systemConnectWS();
	}

	/**
	 * The get the value from country and gender when the spinners for them are selected by the user.
	 * @author Nitin
	 * @param AdapterView
	 * parent object specifies the view object that triggered the event
	 * @param View
	 * The view within the AdapterView that was clicked
	 * @param position
	 * The position of the view in the adapter
	 * @param id
	 *The row id of the item that is selected
	 */

	public void onItemSelected(AdapterView parent,View v, int position, long id) {
		if(parent==countrycodespinner){
			//System.out.println("the gender is "+countrycodelist[position]);
			//countrycode=countrycodelist[position];
			try
			{
				countrycode=jArrayCountryCodeList.getString(position);
				System.out.println("country code is "+countrycode);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		if(parent==countryspinner)
		{
			System.out.println("the country is "+countrylist[position]);
			country=countrylist[position];
		}	
	}


	/**
	 * This method is triggered in case the spinner is not selected by the user
	 * @author Nitin
	 * @param AdapterView
	 * parent object specifies the view object that triggered the event
	 *  
	 */
	public void onNothingSelected(AdapterView parent) {
		if(parent==countrycodespinner)
			registeruserresponse="Please select gender.";

		if(parent==countryspinner)
			registeruserresponse="Please select country.";
	}


	/**
	 * The proceed button clicked event is handled in this function
	 * @author Nitin
	 * @param View view
	 * view object gives all the layout elements of the screen.
	 *
	 */
	public void onRegisterUserClick(View view){

		System.out.println("user register button clicked");

		EditText temp;
		temp = (EditText)findViewById(R.id.EditText01);
		emailid=temp.getText().toString().trim();

		temp = (EditText)findViewById(R.id.EditText02);
		firstname=temp.getText().toString().trim();

		temp = (EditText)findViewById(R.id.EditText03);
		lastname=temp.getText().toString().trim();

		temp = (EditText)findViewById(R.id.EditText04);
		phonenumber=temp.getText().toString().trim();


		System.out.println(emailid+firstname+lastname+phonenumber+countrycode+country);


		//if the email entered is valid call registeruser web services
		if(validateFields()){

			//systemConnectWS();
			registerUserWS(emailid,firstname,lastname,phonenumber,countrycode,country);

		}

		else {
			showResponse(registeruserresponse);	
		}

	}


	/**
	 * This function validates the email entered by the user i.e it should not be empty and should be a valid mail id and all other fields should not be empty
	 * @author Nitin
	 * @param String mailid
	 * mailid has the trimmed email-id entered by the user
	 *
	 */
	public boolean validateFields(){
		Validation v = new Validation();	

		registeruserresponse="";

		if(emailid.length()==0){
			registeruserresponse+=getText(R.string.email).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if(v.validateEmail(emailid)==false){
			registeruserresponse= getText(R.string.entervalidemail).toString();
			return false;
		}

		if (firstname.length()==0){
			registeruserresponse+=getText(R.string.firstname).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if (lastname.length()==0){
			registeruserresponse+=getText(R.string.lastname).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if (phonenumber.length()==0){
			registeruserresponse+=getText(R.string.phonenumber).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if(v.containsOnlyDigits(phonenumber)==false){
			registeruserresponse=getText(R.string.onlydigitsallowed).toString()+(getText(R.string.phonenumber).toString());
			return false;
		}

		if (countrycode.length()==0){
			registeruserresponse+=getText(R.string.countryccode).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

		if (country.length()==0){
			registeruserresponse+=getText(R.string.country).toString()+" "+getText(R.string.emptywarning).toString();
			return false;
		}

//		if(emailid.length()==0||firstname.length()==0||lastname.length()==0||phonenumber.length()==0||countrycode.length()==0||country.length()==0){

//		// to be improvised...hardcoding done
//		registeruserresponse="You left field/s blank.All fields are mandatory.";
//		return false;
//		}

		else return true;
	}





	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog.dismiss();

			if(registeruserresponse.trim().length()!=0){
				servicedownflag=true;
				showResponse(registeruserresponse);

			}	
		}	
	};


	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * There are two main uses for a Handler: 
	 * (1) to schedule messages and runnables to be executed as some point in the future; and 
	 * (2) to enqueue an action to be performed on a different thread than your own. 
	 * @author Prateek
	 */
	private Handler messageHandler2 = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			progressDialog2.dismiss();

			showResponse(registeruserresponse);

		}	
	};


	/**
	 * This function is responsible for displaying a dialog box in case any notification is to be shown to the user
	 * @author Nitin
	 * @param String response 
	 * response is the message that has to be displayed to the user in the dialog box
	 *   
	 * passwordchangedflag is set in the method forgotpasswordWS() when the user.forgotpassword web-service returns a success 
	 * This flag allows to handle this special case where after displaying the dialog box the login activity needs to be shown
	 */
	public void showResponse(String response){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setInverseBackgroundForced(true)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.fi
				if(successfullyregisteredflag==true||servicedownflag==true){
					Intent explicitIntent = new Intent(RegisterUserActivity.this,login.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}

	/**
	 * system.connect web service call
	 * @author Nitin
	 */
	public void systemConnectWS()
	{
		progressDialog = ProgressDialog.show(this, "", "Connecting...");

		//		progressDialog = new ProgressDialog(this); 
		//		progressDialog.setTitle(""); 
		//		progressDialog.setMessage("Loading. Please wait..."); s
		//		progressDialog.setIndeterminate(true); 
		//		progressDialog.setProgressStyle(R.style.MobiiBookProgressDialog);
		//		progressDialog.show();
		new Thread() {
			public void run() {
				try {
					String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("system.connect", "UTF-8");
					WebServiceCall wsCall = new WebServiceCall(data);
					System.out.println("Calling web service");
					//wsCall.start();
					//System.out.println("Waiting for thread to die");
					//wsCall.join();
					//WebServiceCall castedThread = (WebServiceCall) wsCall;
					boolean threadStatus = wsCall.isThreadSucceeded();
					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());
						System.out.println("There is no error");
						JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						if(obj.has("#error"))
						{
							System.out.println("Error in system.connect");
						}
						else
						{
							String sessionId = obj.getString("sessid");
							Session.setSessionId(sessionId);
							if(Session.getSessionId() != null)
							{
								//progressDialog.dismiss();
								//default arguments to be changed.....IMPORTANT
								//registerUserWS(emailid,firstname,lastname,phonenumber,"+91","India");
								getCountryListWS();
								getPhoneCountryListWS();
								//getPhoneCountryListWS();
							}
							else
							{
								System.out.println("session id not available");
							}
						}
					} else {
						System.out.println("The value is " + getText(R.string.network_message).toString());
						registeruserresponse = getText(R.string.network_message).toString();
						//showResponse((getText(R.string.network_message)).toString());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/** Message handler is being called */
				messageHandler.sendEmptyMessage(0);
			}
		}.start();
	}


	/**
	 * user.registration web service call
	 * @author Nitin
	 */

	public void getCountryListWS(){

		//progressDialog2 = ProgressDialog.show(this, "", "Registering...");
		//		new Thread() {
		//			public void run() {
		try {
			String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("general.getcountrylist", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			//			data += "&" + URLEncoder.encode("email_id", "UTF-8")+"="+URLEncoder.encode(emailid, "UTF-8");
			//			data += "&" + URLEncoder.encode("first_name", "UTF-8")+"="+URLEncoder.encode(firstname, "UTF-8");
			//			data += "&" + URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(lastname, "UTF-8");
			//			data += "&" + URLEncoder.encode("phonenumber", "UTF-8")+"="+URLEncoder.encode(phonenumber, "UTF-8");
			//			data += "&" + URLEncoder.encode("phonecountry", "UTF-8")+"="+URLEncoder.encode(countrycode, "UTF-8");
			//			data += "&" + URLEncoder.encode("country", "UTF-8")+"="+URLEncoder.encode(country, "UTF-8");

			WebServiceCall wsCall = new WebServiceCall(data);
			//System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("calling getcountrylist....");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();

			System.out.println("threadstatus is :"+threadStatus);

			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());

				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				//JSONArray jArray = wsCall.getExecutionResponse().getJSONArray("#data");

				JSONArray jArray=obj.names();

				System.out.println(jArray.toString());
				System.out.println("length is :"+jArray.length());


				countrylist=new String[jArray.length()];

				//countrylist=new String[jArray.length()];
				for(int i=0;i<jArray.length();i++)
				{
					countrylist[i]=jArray.getString(i);
					//					System.out.println(countrylist[i]+"\n");

				}


				countryspinner=(Spinner)findViewById(R.id.spinner2);

				countryspinner.setOnItemSelectedListener(this);
				ArrayAdapter<String> countryadapter=new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item,
						countrylist);

				countryadapter.setDropDownViewResource(
						android.R.layout.simple_spinner_dropdown_item);
				countryspinner.setAdapter(countryadapter);

				System.out.println("calling dialog");


				//			if(obj.has("#error"))
				//			{
				//				//System.out.println("Error in restoreFileWS");
				//				obj.get("#message").toString();
				//				//continueRestore = false;
				//			}
				//			else if(!(obj.getInt("response_code") == 0))
				//			{
				//				//continueRestore = false;
				//				System.out.println("RESPONSE : " + obj.getString("response_message"));
				//			}




			}
			else {
				System.out.println("The value is " + getText(R.string.network_message).toString());
				registeruserresponse = getText(R.string.network_message).toString();

				//servicedownflag=true;

				//showResponse((getText(R.string.network_message)).toString());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		//		messageHandler2.sendEmptyMessage(0);
		//	}
		//		}.start();


	}




	public void getPhoneCountryListWS(){

		//progressDialog2 = ProgressDialog.show(this, "", "Registering...");
		//		new Thread() {
		//			public void run() {
		try {
			String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("general.getphonecountrylist", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			//			data += "&" + URLEncoder.encode("email_id", "UTF-8")+"="+URLEncoder.encode(emailid, "UTF-8");
			//			data += "&" + URLEncoder.encode("first_name", "UTF-8")+"="+URLEncoder.encode(firstname, "UTF-8");
			//			data += "&" + URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(lastname, "UTF-8");
			//			data += "&" + URLEncoder.encode("phonenumber", "UTF-8")+"="+URLEncoder.encode(phonenumber, "UTF-8");
			//			data += "&" + URLEncoder.encode("phonecountry", "UTF-8")+"="+URLEncoder.encode(countrycode, "UTF-8");
			//			data += "&" + URLEncoder.encode("country", "UTF-8")+"="+URLEncoder.encode(country, "UTF-8");

			WebServiceCall wsCall = new WebServiceCall(data);
			//System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("calling getphonecountrylist....");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();

			System.out.println("threadstatus is :"+threadStatus);

			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());

				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				//JSONArray jArray = wsCall.getExecutionResponse().getJSONArray("#data");

				jArrayCountryCodeList=obj.names();

				countrycodelist=new String[jArrayCountryCodeList.length()];


				JSONObject tempobject;

				for (int i=0;i<jArrayCountryCodeList.length();i++)
				{
					tempobject=obj.getJSONObject(jArrayCountryCodeList.getString(i));
					//					System.out.println(tempo.get("code"));
					//					System.out.println(tempo.get("country"));
					//System.out.println(tempo.names());
					countrycodelist[i]=tempobject.get("code").toString()+" "+tempobject.get("country").toString();

					//System.out.println(countrycodelist[i]+"\n");



				}


				countrycodespinner=(Spinner)findViewById(R.id.spinner1);
				countrycodespinner.setOnItemSelectedListener(this);




				ArrayAdapter<String> countrycodeadapter=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,countrycodelist);


				countrycodeadapter.setDropDownViewResource(
						android.R.layout.simple_spinner_dropdown_item);
				countrycodespinner.setAdapter(countrycodeadapter);


				//registeruserresponse=jArray.getString(1);

				System.out.println("calling dialog");


				//			if(obj.has("#error"))
				//			{
				//				//System.out.println("Error in restoreFileWS");
				//				obj.get("#message").toString();
				//				//continueRestore = false;
				//			}
				//			else if(!(obj.getInt("response_code") == 0))
				//			{
				//				//continueRestore = false;
				//				System.out.println("RESPONSE : " + obj.getString("response_message"));
				//			}




			}
			else {
				System.out.println("The value is " + getText(R.string.network_message).toString());
				registeruserresponse = getText(R.string.network_message).toString();
				//servicedownflag=true;
				//showResponse((getText(R.string.network_message)).toString());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}

		//		messageHandler2.sendEmptyMessage(0);
		//	}
		//		}.start();


	}


	public void registerUserWS(final String emailid,final String firstname,final String lastname,final String phonenumber,final String countrycode,final String country){

		progressDialog2 = ProgressDialog.show(this, "", "Registering...");
		new Thread() {
			public void run() {
				try {
					String data = URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("user.registration", "UTF-8");
					data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
					data += "&" + URLEncoder.encode("email_id", "UTF-8")+"="+URLEncoder.encode(emailid, "UTF-8");
					data += "&" + URLEncoder.encode("first_name", "UTF-8")+"="+URLEncoder.encode(firstname, "UTF-8");
					data += "&" + URLEncoder.encode("last_name", "UTF-8")+"="+URLEncoder.encode(lastname, "UTF-8");
					data += "&" + URLEncoder.encode("phonenumber", "UTF-8")+"="+URLEncoder.encode(phonenumber, "UTF-8");
					data += "&" + URLEncoder.encode("phonecountry", "UTF-8")+"="+URLEncoder.encode(countrycode, "UTF-8");
					data += "&" + URLEncoder.encode("country", "UTF-8")+"="+URLEncoder.encode(country, "UTF-8");

					WebServiceCall wsCall = new WebServiceCall(data);
					//System.out.println("Starting Thread");
					//wsCall.start();
					System.out.println("calling forgotpassword....");
					//wsCall.join();
					//WebServiceCall castedThread = (WebServiceCall) wsCall;
					boolean threadStatus = wsCall.isThreadSucceeded();

					System.out.println("threadstatus is :"+threadStatus);

					if (threadStatus) {
						System.out.println("Success");
						System.out.println("The response " + wsCall.getExecutionResponse());

						//JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
						JSONArray jArray = wsCall.getExecutionResponse().getJSONArray("#data");

						System.out.println(jArray.toString());


						registeruserresponse=jArray.getString(1);

						//if successfully registered redirect the user to login screen
						if(jArray.getInt(0)==0)
							successfullyregisteredflag=true;

						System.out.println("calling dialog");


						//			if(obj.has("#error"))
						//			{
						//				//System.out.println("Error in restoreFileWS");
						//				obj.get("#message").toString();
						//				//continueRestore = false;
						//			}
						//			else if(!(obj.getInt("response_code") == 0))
						//			{
						//				//continueRestore = false;
						//				System.out.println("RESPONSE : " + obj.getString("response_message"));
						//			}




					}
					else {
						System.out.println("The value is " + getText(R.string.network_message).toString());
						registeruserresponse = getText(R.string.network_message).toString();
						//showResponse((getText(R.string.network_message)).toString());
					}
				}catch(Exception e)
				{
					e.printStackTrace();
				}

				messageHandler2.sendEmptyMessage(0);
			}
		}.start();


	}


}

