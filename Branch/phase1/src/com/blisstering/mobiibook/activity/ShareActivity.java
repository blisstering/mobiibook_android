package com.blisstering.mobiibook.activity;


import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ShareActivity extends OptionsActivity{
	
	private ListView lv1;
	
	/** Called when the activity is first created. 
	 * @author Prateek*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setListAdapter(new ArrayAdapter(this, R.layout.homescreenactivity, WORLDCUP2010));
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.bottombar);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Select Content to Share");
		}
		lv1=(ListView)findViewById(R.id.ListView01);
		lv1.setAdapter(new ArrayAdapter<String>(this,R.layout.listviewactivity , LISTITEM));
		lv1.setCacheColorHint(00000000);
		lv1.setOnItemClickListener(new OnItemClickListener() {  
			public void onItemClick(AdapterView parent, View v,
					int position, long id) {
				// When clicked, show a toast with the TextView text
				Toast.makeText(getApplicationContext(), ((TextView) v).getText(),
						Toast.LENGTH_SHORT).show();
				System.out.println("The position is " + position);
				System.out.println("The id is " + id);
				switch(position)
				{
				case 0:
					/* To open the image gallery */
					openIntent("image/*");
					break;
				case 1:
					/* To open the music gallery */
					openIntent("audio/*");
					break;
				case 2:
					/* To open the video gallery */
					openIntent("video/*");
					break;
				default:
					break;
				}
			}
		});
	}

	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", null);
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}
	
	/**
	 * List items to be shown on the screen
	 * @author Prateek
	 */
	static final String[] LISTITEM = new String[] {
		"Picture",  "Music", "Video" 
	};

	/**
	 * Opens the intent for selecting image,  video or music file
	 * @param String type
	 * Type of file which is to be selected (image, video or music)
	 * @author Prateek
	 */
	public void openIntent(String type)
	{
		Intent intent = new Intent();
		intent.setType(type);  
		intent.setAction(Intent.ACTION_GET_CONTENT);  
		/* Starts the activity for result and return back after selecting the image. Calls onActivityResult function */
		startActivityForResult(Intent.createChooser(intent, "Select"),1); 
	}

	/**
	 * Called after returng from activityForResult
	 * @param int requestCode
	 * Code of the request for the intent
	 * @param int resultCode
	 * Result code of the response
	 * @param Intent data
	 * Data which comes from the activity
	 * @author Prateek
	 */
	@Override  
	public void onActivityResult(int requestCode, int resultCode, Intent data) {   

		if (resultCode == RESULT_OK) {  

			if (requestCode == 1) {  

				// currImageURI is the global variable I'm using to hold the content:// URI of the image  
				Uri currImageURI = data.getData(); 

				System.out.println("THE URI IS " + getRealPathFromURI(currImageURI));
				//shareWS(getRealPathFromURI(currImageURI));
				Intent explicitIntent = new Intent(ShareActivity.this,FriendListActivity.class);
				explicitIntent.putExtra("path", getRealPathFromURI(currImageURI));
				startActivity(explicitIntent);
			}  
		}  
	}  

	/**
	 * Returns the real path of the file (sdcard path)
	 * @param Uri contentUri
	 * Unique Uri of the file
	 * @return String
	 * Real path of the file
	 * @author Prateek
	 */
	public String getRealPathFromURI(Uri contentUri) {  

		// can post image  

		String [] proj={MediaStore.Images.Media.DATA};
		Cursor cursor = managedQuery( contentUri,  
				proj, // Which columns to return  
				null,       // WHERE clause; which rows to return (all rows)  
				null,       // WHERE clause selection arguments (none)  
				null); // Order-by clause (ascending by name)  
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);  
		cursor.moveToFirst();  
		return cursor.getString(column_index);  
	}
}
