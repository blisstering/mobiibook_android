package com.blisstering.mobiibook.activity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
 
import org.apache.http.util.ByteArrayBuffer;
 
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
 
public class download extends Activity{
 ProgressDialog progressDialog;
        //private final String PATH = "/data/data/com.helloandroid.imagedownloader/";  //put the downloaded file here
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DownloadFromUrl("Swades.mkv", Environment.getExternalStorageDirectory() + "/Swades.mkv");
        TextView tv = new TextView(this);	
        tv.setText("Hello, Android");
        setContentView(tv);
    }
 
        public void DownloadFromUrl(String imageURL, String fileName) {  //this is the downloader method
                try {
                	progressDialog = ProgressDialog.show(this, "", "Downloading.....");
                        URL url = new URL("http://192.168.1.12/" + imageURL); //you can write here any link
                        File file = new File(fileName);
                        //File file = new File("data/data/com.android.createFileTest/test.txt");
                        if (!file.exists()) {
                                try {
                                        file.createNewFile();
                                } catch (IOException e) {
                                        e.printStackTrace();
                                }
                        }
 
                        long startTime = System.currentTimeMillis();
                        Log.d("ImageManager", "download begining");
                        Log.d("ImageManager", "download url:" + url);
                        Log.d("ImageManager", "downloaded file name:" + fileName);
                        /* Open a connection to that URL. */
                        URLConnection ucon = url.openConnection();
 
                        /*
                         * Define InputStreams to read from the URLConnection.
                         */
                        InputStream is = ucon.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);
 
                        /*
                         * Read bytes to the Buffer until there is nothing more to read(-1).
                         */
                        ByteArrayBuffer baf = new ByteArrayBuffer(50);
                        int current = 0;
                        FileOutputStream fos = new FileOutputStream(file);
                        //fos.write(baf.toByteArray());
//                        while ((current = bis.read()) != -1) {
//                                //baf.append((byte) current);
//                        		fos.write(current);
//                        }
                        int size = 1024*1024;
                        byte[] buf = new byte[size];
                        int byteRead;
                        while ((byteRead = is.read(buf)) != -1) {
                        fos.write(buf, 0, byteRead);
                        //ByteWritten += ByteRead;
                        }
                        /* Convert the Bytes read to a String. */
                        
                        fos.close();
                        Log.d("ImageManager", "download ready in"
                                        + ((System.currentTimeMillis() - startTime) / 1000)
                                        + " sec");
                        progressDialog.dismiss();
 
                } catch (IOException e) {
                        Log.d("ImageManager", "Error: " + e);
                }
 
        }
}