package com.blisstering.mobiibook.activity;

import java.text.DateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blisstering.mobiibook.service.RestoreService;
import com.blisstering.mobiibook.utils.Session;

public class RestoreServiceActivity extends OptionsActivity {

	private com.blisstering.mobiibook.aidl.RestoreService restoreService;
	private static final String LOG_TAG = "RESTORESERVICECLIENT";
	private RestoreServiceConnection conn;
	private boolean serviceStarted = false;
	ProgressBar progressBar;
	ProgressDialog progressDialog;
	String response;
	boolean isNetworkException = false;
	boolean isOutOfMemoryException = false;
	String backupName ="";
	TextView dateTV, timeTV, contentsTV,contentDetailsTV;
	
	/** Called when the activity is first created. 
	 * If the restore service is already running fetch do not start the service again
	 * If not then start the restore service
	 * @author Prateek
	 * */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.restoreserviceactivity);
		if ( customTitleSupported ) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
//			TextView title  = (TextView)findViewById(R.id.screenTitle);
			Bundle bundle = this.getIntent().getExtras();
			if(bundle != null)
				backupName = bundle.getString("backup_name");
			TextView title  = (TextView)findViewById(R.id.screenTitle);
			title.setText("Restoring " + backupName);
		}
		serviceStarted = Session.isRestoreServiceRunning();
		dateTV = (TextView)findViewById(R.id.TextView02);
		timeTV = (TextView)findViewById(R.id.TextView04);
		contentsTV = (TextView)findViewById(R.id.TextView06);
		contentDetailsTV = (TextView)findViewById(R.id.TextView08);
//		dateTV.setText(DateFormat.getDateInstance().format(new Date()).toString());
//		timeTV.setText(DateFormat.getTimeInstance().format(new Date()).toString());
//		contentsTV.setText("All");
//		contentDetailsTV.setText("Calendars,Contacts,Photos,Music,Videos");
		if(!serviceStarted)
		{
			startService();
			//		invokeService();
		}
		initService();
		registerReceiver(receiver,new IntentFilter(RestoreService.BROADCAST_ACTION));
		ImageButton cancel = (ImageButton)findViewById(R.id.Button01);
		cancel.setOnClickListener(cancelListener);
		progressBar = (ProgressBar)findViewById(R.id.ProgressBar01);
		progressBar.setClickable(false);
		progressBar.setProgress(0);
	}

	/** 
	 * On activity destruction unregister the receiver
	 * Unbind / Release the restore service
	 * @author Prateek 
	 */
	protected void onDestroy()
	{
		unregisterReceiver(receiver);
		releaseService();
		super.onDestroy();
	}

	/**
	 * Handler allows you to send and process Message and Runnable objects associated with a thread's MessageQueue.
	 * Displays the message and dismiss the progressdialog 
	 * @author Prateek
	 */
	private Handler messageHandler = new Handler() {

		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try{
				if(isNetworkException)
					showResponse(getText(R.string.restoreresume).toString(), 1);
				else if(isOutOfMemoryException)
				{
					showResponse(getText(R.string.sdcardnotavailable).toString(), 1);
				}
				else
				{
					showResponse(response, 0);
				}
				if(progressDialog != null)
					progressDialog.dismiss();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	};

	/**
	 * Show alert dialog box on the screen
	 * @param String response  
	 * The message which is displayed
	 * @author Prateek
	 */
	public void showResponse(String response,int value)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		AlertDialog alert;
		switch(value)
		{
		case 0:
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					releaseService();
					stopService();
					Intent explicitIntent = new Intent(RestoreServiceActivity.this,HomeScreenActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			});
			alert = builder.create();
			alert.setTitle("Notification");
			alert.show();
			break;
		case 1:
			builder.setMessage(response)
			.setCancelable(false)
			.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					//MyActivity.this.finish();
					try{
						restoreService.resumeRestore();
					}catch(Exception e)
					{
						showResponse(getText(R.string.restoreabort).toString(), 0);
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					releaseService();
					stopService();
					dialog.cancel();
					Intent explicitIntent = new Intent(RestoreServiceActivity.this,HomeScreenActivity.class);
					explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(explicitIntent);
				}
			});
			alert = builder.create();
			alert.show();

		}
	}
//	private Handler messageHandler1 = new Handler() {

//	public void handleMessage(Message msg) {
//	super.handleMessage(msg);
//	try{
//	//unregisterReceiver(receiver);
//	releaseService();
//	stopService();
////	try{
////	progressDialog.dismiss();
////	}catch(Exception e)
////	{
////	e.printStackTrace();
////	}
//	if(progressDialog != null)
//	progressDialog.dismiss();

//	showResponse(response);
//	}catch(Exception e)
//	{
//	e.printStackTrace();
//	}
//	}
//	};

	public void showResponse1(String response)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(response)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//MyActivity.this.finish();
				Intent explicitIntent = new Intent(RestoreServiceActivity.this,HomeScreenActivity.class);
				explicitIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(explicitIntent);
			}
		});
		AlertDialog alert = builder.create();
		alert.setTitle("Notification");
		alert.show();

	}


	/**
	 * On click listener to cancel restore
	 * @author Prateek
	 */
	private OnClickListener cancelListener = new OnClickListener() {
		public void onClick(View v){
			if(conn != null)
			{
				showCancelSplash();
				try {
					if(restoreService == null)
					{
						showResponse("No restore in progress",0);
					}
					else if(!restoreService.cancelRestore())
					{
						if(progressDialog != null && progressDialog.isShowing())
							progressDialog.dismiss();
						showResponse(getText(R.string.restoreabort).toString(),0);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					messageHandler.sendEmptyMessage(0);
				}
			}
		}	        	
	};

	/**
	 * Shows the cancelling restore progress dialog
	 * @author Prateek
	 */
	public void showCancelSplash()
	{
		progressDialog = ProgressDialog.show(this, "", "Cancelling.....");
	}

	/**
	 * Class to create a connetion to the restore service
	 * @author Prateek
	 *
	 */
	class RestoreServiceConnection implements ServiceConnection {
		/**
		 * When service is connected this function is called
		 * @author Prateek
		 */
		public void onServiceConnected(ComponentName className, 
				IBinder boundService ) {
			restoreService = com.blisstering.mobiibook.aidl.RestoreService.Stub.asInterface((IBinder)boundService);
			Log.d( LOG_TAG,"onServiceConnected" );	
			try {
				restoreService.sendUpdates();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			invokeService();
		}

		/**
		 * When service is dis-connected this function is called
		 * @author Prateek
		 */
		public void onServiceDisconnected(ComponentName className) {
			restoreService = null;
			//unregisterReceiver(receiver);
			Log.d( LOG_TAG,"onServiceDisconnected" );
			//updateServiceStatus();
		}
	};


	/**
	 * Binds the service with the connection object
	 * @author Prateek
	 */
	private void initService() {
		if( conn == null ) {
			conn = new RestoreServiceConnection();
//			Intent i = new Intent(BackupServiceActivity.this,BackupService.class);
//			i.putExtra("key", "value");
			Intent i = new Intent(RestoreServiceActivity.this,RestoreService.class);
			//i.setClassName("com.blisstering.mobiibook.service", "com.blisstering.mobiibook.service.BackupService");
			//boolean result = this.bindService( i, conn,0);
			boolean result = getApplicationContext().bindService( i, conn,0);
			Log.d( LOG_TAG, "BindService:" + result);
			if(conn == null)
				Log.d( LOG_TAG, "conn is null" );
			else
				Log.d( LOG_TAG, "Conn not null" );	

			Log.d( LOG_TAG, "bindService()" );
		} else {
//			NotificationManager nm = (NotificationManager)
//			getSystemService( Context.NOTIFICATION_SERVICE);
//			nm.notifyWithText(123, "Cannot bind - service already bound",
//			NotificationManager.LENGTH_LONG, null );
		}
	}

	/**
	 * Unbinds the service from the connection object
	 * @author Prateek
	 */
	private void releaseService() {
		try{
			if( conn != null ) {
				getApplicationContext().unbindService( conn );
				conn = null;
				//updateServiceStatus();
				Log.d( LOG_TAG, "unbindService()" );
			} else {
//				NotificationManager nm = (NotificationManager)
//				getSystemService( Context.NOTIFICATION_SERVICE);	
//				nm.notifyWithText(123, "Cannot unbind - service not bound",
//				NotificationManager.LENGTH_LONG, null );
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Starts the restore service
	 * @author Prateek
	 */
	private void startService() {
//		if( started ) {
//		NotificationManager nm = (NotificationManager)
//		getSystemService( Context.NOTIFICATION_SERVICE);
//		nm.notifyWithText(123, "Service already started",
//		NotificationManager.LENGTH_LONG, null );
//		} else {
		Intent i = new Intent(RestoreServiceActivity.this,RestoreService.class);
		Bundle bundle = this.getIntent().getExtras();
		final String backupId = bundle.getString("backup_id");
		System.out.println("The backup_id " + backupId);
//		dateTV.setText(DateFormat.getDateInstance().format(new Date()).toString());
//		timeTV.setText(DateFormat.getTimeInstance().format(new Date()).toString());
//		contentsTV.setText("All");
//		contentDetailsTV.setText("Calendars,Contacts,Photos,Music,Videos");
		i.putExtra("backupId",backupId);
		i.putExtra("date",DateFormat.getDateInstance().format(new Date()).toString());
		i.putExtra("time",DateFormat.getTimeInstance().format(new Date()).toString());
		i.putExtra("contents","All");
		i.putExtra("contentDetails","Calendars,Contacts,Photos,Music,Videos");
		i.putExtra("backupName",backupName);
		//i.setClassName( "com.blisste;ring.mobiibook.service", "BackupService" );
		//Bundle b = new Bundle();
		//startService( i,b );
		startService(i);
		Log.d( LOG_TAG, "startService()" );
//		started = true;
//		updateServiceStatus();
		//}
	}

	/**
	 * Stops the restore service
	 * @author Prateek
	 */
	private void stopService() {
//		if( !started ) {
//		NotificationManager nm = (NotificationManager)
//		getSystemService( Context.NOTIFICATION_SERVICE);
//		nm.notifyWithText(123, "Service not yet started",
//		NotificationManager.LENGTH_LONG, null );
//		} else {
		Intent i = new Intent(RestoreServiceActivity.this,RestoreService.class);
		//i.setClassName( "aexp.dualservice", 
		//	"aexp.dualservice.DualService" );
		stopService( i );
		Log.d( LOG_TAG, "stopService()" );
//		started = false;
//		updateServiceStatus();
//		}
	}

//	private void invokeService() {
////	if( conn == null ) {
////	NotificationManager nm = (NotificationManager)
////	getSystemService( Context.NOTIFICATION_SERVICE);
////	nm.notifyWithText(123, "Cannot invoke - service not bound",
////	NotificationManager.LENGTH_LONG, null );
////	} else {
//	Bundle bundle = this.getIntent().getExtras();
//	final String backupName = bundle.getString("backupName");
//	new Thread() {
//	public void run() {
//	try {
//	exportContact();
//	backupService.startBackup(backupName);
//	} catch( Exception ex ) {
//	Log.e( LOG_TAG, "DeadObjectException" );
//	}
//	}
//	}.start();
//	}
//	}

	/**
	 * Receiver to get the details broadcasted by Restore Service
	 * @author Prateek
	 */
	private BroadcastReceiver receiver=new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {
			Bundle bundle = intent.getExtras();
			final int value = bundle.getInt("progress");
			System.out.println("the value is " + value);
			updateProgressBar(value);
			updateDisplay(bundle.getString("date"), bundle.getString("time"), bundle.getString("contents"), bundle.getString("contentDetails"),bundle.getString("backupName"));
			if(bundle.getBoolean("restoreover") || bundle.getBoolean("isNetworkException") || bundle.getBoolean("isOutOfMemoryException"))
			{
				response = bundle.getString("response"); 
				isNetworkException = bundle.getBoolean("isNetworkException");
				isOutOfMemoryException = bundle.getBoolean("isOutOfMemoryException");
				sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()))); 
				messageHandler.sendEmptyMessage(0);
			}
		}

	};
	
	/**
	 * To update the progress bar on the screen
	 * @author Prateek
	 */
	public void updateProgressBar(int value)
	{
		progressBar.setProgress(value);
	}
	
	/**
	 * To update the other details of the restore on the screen
	 * @author Prateek
	 */
	public void updateDisplay(String date, String time, String contents, String contentDetails, String backupName)
	{
		dateTV.setText(date);
		timeTV.setText(time);
		contentsTV.setText(contents);
		contentDetailsTV.setText(contentDetails);
		TextView title  = (TextView)findViewById(R.id.screenTitle);
		title.setText("Restoring " + backupName);
	}
}
