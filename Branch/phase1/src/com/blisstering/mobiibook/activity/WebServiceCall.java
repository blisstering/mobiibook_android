package com.blisstering.mobiibook.activity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;


public class WebServiceCall {

	HttpURLConnection conn;
	OutputStreamWriter wr;
	BufferedReader rd;
	JSONObject obj;
	boolean threadSucceeded = false;
	int count=0;

	/**
	 * Constructor of the WebServiceCall.
	 * @param String data
	 * The data which will be passed to the web service via POST method 
	 * @author Prateek
	 */
	public WebServiceCall(String data) {
		// TODO Auto-generated constructor stub
		WSCall(data);
	}

	/**
	 * Calls the web service
	 * @param String data
	 * The data which will be passed to the web service via POST method 
	 * @author Prateek
	 */
	public void WSCall(String data){
		try{
			//String url = "http://www.bliss-dev.com/mobiibook/services/json";
			String url = "http://staging.mobiibook.com/services/json";
			String line,response;
			//HttpURLConnection is created
			conn=(HttpURLConnection)(new URL(url)).openConnection();
			conn.setDoOutput(true);	
			//Sets the connection timeout of 8sec
			conn.setConnectTimeout(8000);
			wr=new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();
			rd=new BufferedReader(new InputStreamReader(conn.getInputStream()));
			response = "";
			line = "";
			//Read the response from the server
			while((line=rd.readLine())!=null)
			{
				response=response+line;
			}
			wr.close();
			rd.close();
			obj = new JSONObject(response);
			System.out.println("--" + obj.get("#error") + "---");
			if(obj.get("#error").toString().equalsIgnoreCase("false"))	
			{
				threadSucceeded = true;
			}
		}catch(JSONException e)
		{
			//org.json.JSONException: End of input at character 0 of 
			// If the function is called again it works
			count++;
			if(count<=1)
				WSCall(data);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			rd = null;
			wr = null;
		}
	}

	/**
	 * Returns the JSONObject of the response
	 * @return JSONObject 
	 * @author Prateek
	 */
	public JSONObject getExecutionResponse()
	{
		return obj;
	}

	/**
	 * Returns if the web service call is successful or not
	 * @return boolean
	 * @author Prateek
	 */
	public boolean isThreadSucceeded()
	{
		return threadSucceeded;
	}

}