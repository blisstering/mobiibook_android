package com.blisstering.mobiibook.activity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.RawContacts.Data;
import android.util.Log;

public class DeleteContactActivity extends Activity{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);
		//deleteContacts();
		//exportContact();
		insertContacts();
	}

	public void deleteContacts()
	{
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		while (cur.moveToNext()) {
			try{
				String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
				Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
				System.out.println("The uri is " + uri.toString());
				cr.delete(uri, null, null);
			}
			catch(Exception e)
			{
				System.out.println(e.getStackTrace());
			}
		}
	}

	public void exportContact()
	{
		ContentResolver cr = getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
				null, null, null, null);
		System.out.println("The count of contacts is " + cur.getCount());
		if (cur.getCount() > 0) {
			try{
				String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
				File vCardFile = new File(extStorageDirectory,"/contacts1.vcf");
				if (!vCardFile.exists()) {
					try {
						vCardFile.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				OutputStream os = new FileOutputStream(vCardFile);

				while (cur.moveToNext()) {
					try{
						String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
						Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
						System.out.println("The value is " + cr.getType(uri));
						AssetFileDescriptor fd = this.getContentResolver().openAssetFileDescriptor(uri, "r");
						FileInputStream fis = fd.createInputStream();
						byte[] b = new byte[1024*4];  
						int read;  
						while ((read = fis.read(b)) != -1) {  
							os.write(b, 0, read);  
						} 
//						byte[] buf = new byte[(int)fd.getDeclaredLength()];
//						if (0 < fis.read(buf))
//						{
//						String vCard = new String(buf);
//						System.out.println("The vCard value is " + vCard);
//						}
						fis.close();
					}
					catch(Exception e)
					{
						System.out.println(e.getStackTrace());
					}
				}
				os.flush();
				os.close();	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		}
	}

//	public void insertContacts()
//	{
//		ContentValues values = new ContentValues();
//		values.put(RawContacts.ACCOUNT_TYPE, accountType);
//		values.put(RawContacts.ACCOUNT_NAME, accountName);
//		Uri rawContactUri = getContentResolver().insert(RawContacts.CONTENT_URI, values);
//		long rawContactId = ContentUris.parseId(rawContactUri);
//
//		values.clear();
//		values.put(Data.RAW_CONTACT_ID, rawContactId);
//		values.put(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE);
//		values.put(StructuredName.DISPLAY_NAME, "Mike Sullivan");
//		getContentResolver().insert(Data.CONTENT_URI, values);
//
//	}

	public void insertContacts()
	{
		try{
		ArrayList<ContentProviderOperation> op_list = new
		ArrayList<ContentProviderOperation>();
		int rawContactInsertOperationBackRefIndex = 0;

		AccountManager am = AccountManager.get(getApplicationContext());
		Account[] accounts = am.getAccounts();

//		if(accounts != null && accounts.length > 0) {
//			for(int i = 0; i < accounts.length; i++) {
//				if(!accounts[i].name.contains(":")) {

					//Log.i("MYTAG", "Adding contact of name = "+accounts[i].name+"type = "+accounts[i].type);
						// insert null account information (Android will find it)

						op_list.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)


//								.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE,
//										accounts[i].type)
//
//
//										.withValue(ContactsContract.RawContacts.ACCOUNT_NAME,
//												accounts[i].name)

												.build());

					// insert new contact display name

					op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)


							.withValueBackReference(Data.RAW_CONTACT_ID,
									rawContactInsertOperationBackRefIndex)


									.withValue(Data.MIMETYPE,
											StructuredName.CONTENT_ITEM_TYPE)


											.withValue(StructuredPostal.TYPE,
													StructuredPostal.TYPE_HOME)


													.withValue(StructuredName.DISPLAY_NAME,
															"new1")


															.withValue(StructuredName.GIVEN_NAME,
																	"new2")

																	.build());

					// insert formatted postal address

					op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)


							.withValueBackReference(Data.RAW_CONTACT_ID,
									rawContactInsertOperationBackRefIndex)


									.withValue(Data.MIMETYPE,
											StructuredPostal.CONTENT_ITEM_TYPE)


											.withValue(StructuredPostal.FORMATTED_ADDRESS,
													"Johari Bazar")

													.build());

					rawContactInsertOperationBackRefIndex =
						rawContactInsertOperationBackRefIndex + 3;
//				}
//			}
//		}

		ContentProviderResult[] results =
			getApplicationContext().getContentResolver().applyBatch(ContactsContract.AUTHORITY,
					op_list);
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	}

}
