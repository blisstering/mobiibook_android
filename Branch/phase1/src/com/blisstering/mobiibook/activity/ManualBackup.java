package com.blisstering.mobiibook.activity;

import org.json.JSONArray;
import com.blisstering.mobiibook.backup.BackupContentsThread;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ManualBackup extends Activity{

	JSONArray jsonFiles = null;
	int fileNumber = 0;
	BackupContentsThread backupContentThread;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manualbackup);
	}
	
	public void startManualBackup(View view) {
		//Hardcoding
		backupContentThread = new BackupContentsThread("BackupName","Manual");
		//backupContentThread.run();
	}
	
}
