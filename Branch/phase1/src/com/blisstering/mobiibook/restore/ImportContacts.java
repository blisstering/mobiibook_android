/*
 * Used Android Source Code
 */

package com.blisstering.mobiibook.restore;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import android.accounts.Account;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.util.Log;
import com.blisstering.mobiibook.android.pim.vcard.VCardConfig;
import com.blisstering.mobiibook.android.pim.vcard.VCardEntryCommitter;
import com.blisstering.mobiibook.android.pim.vcard.VCardEntryConstructor;
import com.blisstering.mobiibook.android.pim.vcard.VCardEntryCounter;
import com.blisstering.mobiibook.android.pim.vcard.VCardInterpreter;
import com.blisstering.mobiibook.android.pim.vcard.VCardInterpreterCollection;
import com.blisstering.mobiibook.android.pim.vcard.VCardParser_V21;
import com.blisstering.mobiibook.android.pim.vcard.VCardParser_V30;
import com.blisstering.mobiibook.android.pim.vcard.VCardSourceDetector;
import com.blisstering.mobiibook.android.pim.vcard.exception.VCardException;
import com.blisstering.mobiibook.android.pim.vcard.exception.VCardNestedException;
import com.blisstering.mobiibook.android.pim.vcard.exception.VCardNotSupportedException;
import com.blisstering.mobiibook.android.pim.vcard.exception.VCardVersionException;

class VCardFile {
	private String mName;
	private String mCanonicalPath;
	private long mLastModified;
	public VCardFile(String name, String canonicalPath, long lastModified) {
		mName = name;
		mCanonicalPath = canonicalPath;
		mLastModified = lastModified;
	}

	public String getName() {
		return mName;
	}

	public String getCanonicalPath() {
		return mCanonicalPath;
	}

	public long getLastModified() {
		return mLastModified;
	}
}

public class ImportContacts{

	private static final String LOG_TAG = "ImportContactsActivity";
	private Account mAccount;
	private VCardReadThread mVCardReadThread;
	Context context; 

	private class VCardReadThread{
		private ContentResolver mResolver;
		private VCardParser_V21 mVCardParser;
		private boolean mCanceled;
		//private PowerManager.WakeLock mWakeLock;
		private Uri mUri;
		private File mTempFile;
		private boolean mNeedReview = false;
		private List<VCardFile> mSelectedVCardFileList;
		private List<String> mErrorFileNameList;

		public VCardReadThread(Uri uri) {
			mUri = uri;
			//Context context = getContext();
			mResolver = context.getContentResolver();
			init();
		}

		public VCardReadThread(final List<VCardFile> selectedVCardFileList) {
			mSelectedVCardFileList = selectedVCardFileList;
			mErrorFileNameList = new ArrayList<String>();
			init();
		}

//		private void init() {
//			Context context = ImportContactsActivity.this;
//			mResolver = context.getContentResolver();
//			PowerManager powerManager = (PowerManager)context.getSystemService(
//					Context.POWER_SERVICE);
//			mWakeLock = powerManager.newWakeLock(
//					PowerManager.SCREEN_DIM_WAKE_LOCK |
//					PowerManager.ON_AFTER_RELEASE, LOG_TAG);
//		}

		@Override
		public void finalize() {
//			if (mWakeLock != null && mWakeLock.isHeld()) {
//				mWakeLock.release();
//			}
		}

		public void init() {
			boolean shouldCallFinish = true;
			//mWakeLock.acquire();
			Uri createdUri = null;
			mTempFile = null;
			// Some malicious vCard data may make this thread broken
			// (e.g. OutOfMemoryError).
			// Even in such cases, some should be done.
			try {
				if (mUri != null) {  // Read one vCard expressed by mUri
					final Uri targetUri = mUri;
					//mProgressDialogForReadVCard.setProgressNumberFormat("");
					//mProgressDialogForReadVCard.setProgress(0);

					// Count the number of VCard entries
					//mProgressDialogForReadVCard.setIndeterminate(true);
					long start;
//					if (DO_PERFORMANCE_PROFILE) {
//					start = System.currentTimeMillis();
//					}
					VCardEntryCounter counter = new VCardEntryCounter();
					VCardSourceDetector detector = new VCardSourceDetector();
					VCardInterpreterCollection builderCollection = new VCardInterpreterCollection(
							Arrays.asList(counter, detector));
					boolean result;
					try {
						result = readOneVCardFile(targetUri,
								VCardConfig.DEFAULT_CHARSET, builderCollection, null, true, null);
					} catch (VCardNestedException e) {
						try {
							// Assume that VCardSourceDetector was able to detect the source.
							// Try again with the detector.
							result = readOneVCardFile(targetUri,
									VCardConfig.DEFAULT_CHARSET, counter, detector, false, null);
						} catch (VCardNestedException e2) {
							result = false;
							Log.e(LOG_TAG, "Must not reach here. " + e2);
						}
					}
//					if (DO_PERFORMANCE_PROFILE) {
//					long time = System.currentTimeMillis() - start;
//					Log.d(LOG_TAG, "time for counting the number of vCard entries: " +
//					time + " ms");
//					}
					if (!result) {
						shouldCallFinish = false;
						return;
					}

//					mProgressDialogForReadVCard.setProgressNumberFormat(
//					getString(R.string.reading_vcard_contacts));
//					mProgressDialogForReadVCard.setIndeterminate(false);
//					mProgressDialogForReadVCard.setMax(counter.getCount());
					String charset = detector.getEstimatedCharset();
					createdUri = doActuallyReadOneVCard(targetUri, null, charset, true, detector,
							mErrorFileNameList);
				} 
			} finally {
				//mWakeLock.release();
				//mProgressDialogForReadVCard.dismiss();
				if (mTempFile != null) {
					if (!mTempFile.delete()) {
						Log.w(LOG_TAG, "Failed to delete a cache file.");
					}
					mTempFile = null;
				}
				// finish() is called via mCancelListener, which is used in DialogDisplayer.
				if (shouldCallFinish) {
					if (mErrorFileNameList == null || mErrorFileNameList.isEmpty()) {
						//finish();
						if (mNeedReview) {
							mNeedReview = false;
							Log.v("importVCardActivity", "Prepare to review the imported contact");

							if (createdUri != null) {
								// get contact_id of this raw_contact
								final long rawContactId = ContentUris.parseId(createdUri);
//								Uri contactUri = RawContacts.getContactLookupUri(
//										getContentResolver(), ContentUris.withAppendedId(
//												RawContacts.CONTENT_URI, rawContactId));
//
//								Intent viewIntent = new Intent(Intent.ACTION_VIEW, contactUri);
//								startActivity(viewIntent);
							}
						}
					} else {
						StringBuilder builder = new StringBuilder();
						boolean first = true;
						for (String fileName : mErrorFileNameList) {
							if (first) {
								first = false;
							} else {
								builder.append(", ");
							}
							builder.append(fileName);
						}

//						runOnUIThread(new DialogDisplayer(
//						getString(R.string.fail_reason_failed_to_read_files,
//						builder.toString())));
					}
				}
	//			messageHandler.sendEmptyMessage(0);
			}
		}

		private Uri doActuallyReadOneVCard(Uri uri, Account account,
				String charset, boolean showEntryParseProgress,
				VCardSourceDetector detector, List<String> errorFileNameList) {
			//final Context context = ImportContactsActivity.this;
			VCardEntryConstructor builder;
			final String currentLanguage = Locale.getDefault().getLanguage();
//			int vcardType = VCardConfig.getVCardTypeFromString(
//			context.getString(R.string.config_import_vcard_type));
			int vcardType = VCardConfig.getVCardTypeFromString("default");
			if (charset != null) {
				builder = new VCardEntryConstructor(charset, charset, false, vcardType, mAccount);
			} else {
				charset = VCardConfig.DEFAULT_CHARSET;
				builder = new VCardEntryConstructor(null, null, false, vcardType, mAccount);
			}
			VCardEntryCommitter committer = new VCardEntryCommitter(mResolver);
			builder.addEntryHandler(committer);
//			if (showEntryParseProgress) {
//			builder.addEntryHandler(new ProgressShower(mProgressDialogForReadVCard,
//			context.getString(R.string.reading_vcard_message),
//			ImportVCardActivity.this,
//			mHandler));
//			}

			try {
				if (!readOneVCardFile(uri, charset, builder, detector, false, null)) {
					return null;
				}
			} catch (VCardNestedException e) {
				Log.e(LOG_TAG, "Never reach here.");
			}
			final ArrayList<Uri> createdUris = committer.getCreatedUris();
			return (createdUris == null || createdUris.size() != 1) ? null : createdUris.get(0);
		}

		private boolean readOneVCardFile(Uri uri, String charset,
				VCardInterpreter builder, VCardSourceDetector detector,
				boolean throwNestedException, List<String> errorFileNameList)
		throws VCardNestedException {
			InputStream is;
			try {
				is = mResolver.openInputStream(uri);
				mVCardParser = new VCardParser_V21(detector);

				try {
					mVCardParser.parse(is, charset, builder, mCanceled);
				} catch (VCardVersionException e1) {
					try {
						is.close();
					} catch (IOException e) {
					}
					if (builder instanceof VCardEntryConstructor) {
						// Let the object clean up internal temporal objects,
						((VCardEntryConstructor)builder).clear();
					}
					is = mResolver.openInputStream(uri);

					try {
						mVCardParser = new VCardParser_V30();
						mVCardParser.parse(is, charset, builder, mCanceled);
					} catch (VCardVersionException e2) {
						throw new VCardException("vCard with unspported version.");
					}
				} finally {
					if (is != null) {
						try {
							is.close();
						} catch (IOException e) {
						}
					}
				}
			} catch (IOException e) {
				Log.e(LOG_TAG, "IOException was emitted: " + e.getMessage());

				//mProgressDialogForReadVCard.dismiss();

				if (errorFileNameList != null) {
					errorFileNameList.add(uri.toString());
				} else {
//					runOnUIThread(new DialogDisplayer(
//					getString(R.string.fail_reason_io_error) +
//					": " + e.getLocalizedMessage()));
				}
				return false;
			} catch (VCardNotSupportedException e) {
				if ((e instanceof VCardNestedException) && throwNestedException) {
					throw (VCardNestedException)e;
				}
				if (errorFileNameList != null) {
					errorFileNameList.add(uri.toString());
				} else {
//					runOnUIThread(new DialogDisplayer(
//					getString(R.string.fail_reason_vcard_not_supported_error) +
//					" (" + e.getMessage() + ")"));
				}
				return false;
			} catch (VCardException e) {
				if (errorFileNameList != null) {
					errorFileNameList.add(uri.toString());
				} else {
//					runOnUIThread(new DialogDisplayer(
//					getString(R.string.fail_reason_vcard_parse_error) +
//					" (" + e.getMessage() + ")"));
				}
				return false;
			}
			return true;
		}

		public void cancel() {
			mCanceled = true;
			if (mVCardParser != null) {
				mVCardParser.cancel();
			}
		}

		public void onCancel(DialogInterface dialog) {
			cancel();
		}
	}















	public void startImport(File vCardFile,Context context1) {
		context = context1;
//		Intent intent = getIntent();
//		final String action = intent.getAction();
		Uri uri;
		//String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		//File vCardFile = new File(extStorageDirectory,"/new.vcf");
		uri = Uri.fromFile(vCardFile);
		//uri = vCardFile.toURI();
		Log.v(LOG_TAG, " ; path = " + uri);
//		if (Intent.ACTION_VIEW.equals(action)) {
//			// Import the file directly and then go to EDIT screen
//			//     mNeedReview = true;
//		}
		importOneVCardFromSDCard(uri);

//		if (uri != null) {
//		importOneVCardFromSDCard(uri);
//		} else {
//		doScanExternalStorageAndImportVCard();
//		}
	}

	private void importOneVCardFromSDCard(final Uri uri) {
//		runOnUIThread(new Runnable() {
	//	progressDialog = ProgressDialog.show(this, "", "Restoring contacts.....");
//		new Thread() {
//			public void run() {
				mVCardReadThread = new VCardReadThread(uri);
				//mVCardReadThread.init();
//			}
		//}.start();
	}
}



