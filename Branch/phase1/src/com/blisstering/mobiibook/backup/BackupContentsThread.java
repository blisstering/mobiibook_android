package com.blisstering.mobiibook.backup;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import android.os.Environment;
import com.blisstering.mobiibook.activity.WebServiceCall;
import com.blisstering.mobiibook.utils.Session;

/**
 * Class responsible for running the backup process in the background 
 * @author Prateek
 */
public class BackupContentsThread{

	JSONArray jsonFiles = null;
	int fileNumber = 0;
	String backUpName;
	String backUpType;
	JSONArray jsonReturnedFiles = null;
	String backUpId,returnMessage = "";
	long bytesToTransfer,bytesTransferred = 0;
	boolean backupCancel = false;
	boolean networkException = false;
	boolean noFileToTransfer = false;
	HttpPost httppost;

	/**
	 * Constructor of the class
	 * @param String name 
	 * Name of the backup
	 * @param String type
	 * Type of Backup (Manual, Scheduled)
	 * @author Prateek
	 */
	public BackupContentsThread(String name,String type) {
		// TODO Auto-generated constructor stub
		backUpName = name;
		backUpType = type;
	}

	/**
	 * Starts the backup procedure
	 * @return String
	 * Returns the resonse message of the backup process
	 * @author Prateek
	 */
	public String startBackup()
	{
		boolean filesAvailableToRead = getAllFiles();
		if(filesAvailableToRead)
		{
			System.out.println("Files are ready");
			System.out.println("The jsonFiles " + jsonFiles.toString());
			startBackupWS();
		}
		else
		{	
			//System.out.println("Cannot read files");
			//HardCoding
			returnMessage = "Files do not have read permissions. Try again";
		}

		if(backupCancel)
			backupCancelWS();

		return returnMessage;
	}

	/**
	 * Returns if backup was stopped due to any network exception or not
	 * @return boolean
	 * @author Prateek
	 */
	public boolean isNetworkException()
	{
		return networkException;
	}
	
	/**
	 * Checks if the external storage is available or not
	 * Also, generates the list of files to be backedup
	 * @return boolean
	 * @author Prateek
	 */
	public boolean getAllFiles()
	{	
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		if(mExternalStorageAvailable)
		{
			jsonFiles = new JSONArray();
			getAllFilesFromDirectory(Environment.getExternalStorageDirectory());
		}
		else
		{
			System.out.println("The Media is not available to read");
		}
		return mExternalStorageAvailable;

	}


	/**
	 * Get all the files from the particular directory
	 * @param File directory
	 * directory from which file list to be generated
	 * @author Prateek
	 */
	public void getAllFilesFromDirectory(File directory)
	{
		try{

			File file[] = directory.listFiles();  
			System.out.println("The length of files is " + file.length);

			for(int i=0;i<file.length;i++)
			{
				System.out.println("The file paths " + i + "=" + file[i].getAbsolutePath());
				if(file[i].isDirectory())
				{
					System.out.println(file[i].getName() + " is directory");
					getAllFilesFromDirectory(file[i]);
					//revisit
				}
				else
				{
					JSONArray singleFile = new JSONArray();
					singleFile.put(file[i].getName());
					System.out.println("File Name " + file[i].getName());
					singleFile.put(file[i].length() + "");
					singleFile.put(file[i].getAbsolutePath());
					singleFile.put(file[i].lastModified() + "");
					jsonFiles.put(singleFile);
					fileNumber++;
				}
				//transfer(file[i].getAbsolutePath());
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * backup.start web service is called from here
	 * @author Prateek
	 */
	public void startBackupWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.start", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			//HardCoding for Android Client ID
			data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
			data += "&" + URLEncoder.encode("backup_name", "UTF-8")+"="+URLEncoder.encode(backUpName, "UTF-8");
			data += "&" + URLEncoder.encode("backup_type", "UTF-8")+"="+URLEncoder.encode(backUpType, "UTF-8");
			data += "&" + URLEncoder.encode("files", "UTF-8")+"="+URLEncoder.encode(jsonFiles.toString(), "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					returnMessage = obj.get("#message").toString();
				}
				else if(obj.has("response_code") && obj.getInt("response_code") != 0)
				{
					returnMessage = obj.getString("response_message");
				}
				else
				{
					System.out.println("@startBackupWS : The object value " + obj.toString());
					startFileTransferProcedure(obj);
				}
			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * File transfer procedure starts from here, the list of files to be backed up is passed to it as an JSONObject
	 * and then they are transferred.
	 * @param JSONObject obj
	 * The json object of files
	 * @author Prateek
	 */
	public void startFileTransferProcedure(JSONObject obj)
	{
		try{
			noFileToTransfer = false;
			jsonReturnedFiles = obj.getJSONArray("files");
			backUpId = obj.getString("backup_id");
			if (jsonReturnedFiles.length() < 1) {
				System.out.println("No files to transfer");
				returnMessage = obj.getString("response_message");
			} else {
				if(!(bytesToTransfer > 0))
					bytesToTransfer = 0;
				//calculate the number of bytes to be transferred
				for(int i=0;i<jsonReturnedFiles.length();i++)
				{
					bytesToTransfer += jsonReturnedFiles.getJSONArray(i).getInt(1);
				}
				for(int i=0;i<=jsonReturnedFiles.length();i++)
				{
					if(backupCancel || networkException)
						break;
					if(i==jsonReturnedFiles.length())
						noFileToTransfer = true;
					boolean returnValue = backUpFileTransferWS(i);
					if(!returnValue)
						break;
					//	break;
				}
			}	
		}catch(Exception e)
		{
			System.out.println("Error in startFileTransferProcedure");
			e.printStackTrace();
		}
	}

	/**
	 * backup.filetransfer is called from here
	 * @author Prateek
	 */
	private boolean backUpFileTransferWS(int current) {

		System.out.println("Entering backup.filetransfer service for "+current);
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.filetransfer", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backUpId, "UTF-8");
			if(current == 0)
				data += "&" + URLEncoder.encode("file_previous", "UTF-8")+"="+URLEncoder.encode("", "UTF-8");
			else
				data += "&" + URLEncoder.encode("file_previous", "UTF-8")+"="+URLEncoder.encode(((JSONArray)jsonReturnedFiles.get(current-1)).toString(), "UTF-8");

			if(current == jsonReturnedFiles.length())
				data += "&" + URLEncoder.encode("file_current", "UTF-8")+"="+URLEncoder.encode("", "UTF-8");
			else
				data += "&" + URLEncoder.encode("file_current", "UTF-8")+"="+URLEncoder.encode(((JSONArray)jsonReturnedFiles.get(current)).toString(), "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = castedThread.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + castedThread.getExecutionResponse());
				System.out.println("There is no error");
				//JSONObject obj = castedThread.getExecutionResponse().getJSONObject("#data");
				JSONObject obj = castedThread.getExecutionResponse();
				int resp = -1;

				try{
					JSONArray dataResArray = obj.getJSONArray("#data");
					resp=dataResArray.getInt(0);
					returnMessage = dataResArray.getString(1);
					if(resp != 0)
					{
						return false;
					}
				}
				catch (Exception e) {
					return false;
				}
				if(resp == 0 && !noFileToTransfer)
				{
					JSONArray jsonCurrentFile;
					System.out.println("@startBackupWS : The object value " + obj.toString());
					jsonCurrentFile = (JSONArray)jsonReturnedFiles.get(current);
					transfer(jsonCurrentFile.getString(2),jsonCurrentFile.getString(0));
					System.out.println(jsonCurrentFile.getString(0) + " is transferred");
					bytesTransferred += jsonCurrentFile.getInt(1);
				}
			} else {
				networkException = true;
				System.out.println("Failure");
				return false;
			}

		} catch (Exception e) {
			System.out.println("@startBackupFileTransfer : " + e.getMessage());
			e.printStackTrace();
			return false;
		}
		System.out.println("Exiting backup.filetransfer service for "+current);
		return true;
	}
	
	/**
	 * To cancel the backup
	 * @author Prateek
	 */
	public void backupCancel()
	{
		backupCancel = true;
		if(httppost != null)
			httppost.abort();
	}
	
	/**
	 * backup.cancel web service is called from here
	 * @author Prateek
	 */
	public void backupCancelWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.cancel", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backUpId, "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONArray jArr = wsCall.getExecutionResponse().getJSONArray("#data");
				System.out.println("In backup Cancel " + jArr.getString(1));
				returnMessage = jArr.getString(1);
			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * To resume the backup which was aborted due to some network issues
	 * @return String
	 * Returns the response message
	 * @author Prateek
	 */
	public String resumeBackup()
	{
		networkException = false;
		backupCancel = false;
		if(backUpId != null)
			resumeBackupWS();
		else
			startBackup();
		return returnMessage;
	}
	
	/**
	 * backup.resume web service is called from here
	 * @author Prateek
	 */
	public void resumeBackupWS()
	{
		try {
			String data=URLEncoder.encode("method", "UTF-8")+"="+URLEncoder.encode("backup.resume", "UTF-8");
			data += "&" + URLEncoder.encode("sessid", "UTF-8")+"="+URLEncoder.encode(Session.getSessionId(), "UTF-8");
			//HardCoding for Android Client ID
			data += "&" + URLEncoder.encode("client_id", "UTF-8")+"="+URLEncoder.encode("1", "UTF-8");
			data += "&" + URLEncoder.encode("backup_id", "UTF-8")+"="+URLEncoder.encode(backUpId, "UTF-8");
			data += "&" + URLEncoder.encode("files", "UTF-8")+"="+URLEncoder.encode(jsonFiles.toString(), "UTF-8");

			//Thread wsCall = new WebServiceCall(data);
			WebServiceCall wsCall = new WebServiceCall(data);
			System.out.println("Starting Thread");
			//wsCall.start();
			System.out.println("Waiting for thread to die");
			//wsCall.join();
			//WebServiceCall castedThread = (WebServiceCall) wsCall;
			boolean threadStatus = wsCall.isThreadSucceeded();
			if (threadStatus) {
				System.out.println("Success");
				System.out.println("The response " + wsCall.getExecutionResponse());
				System.out.println("There is no error");
				JSONObject obj = wsCall.getExecutionResponse().getJSONObject("#data");
				if(obj.has("#error"))
				{
					returnMessage = obj.get("#message").toString();
				}
				else
				{
					System.out.println("@startBackupWS : The object value " + obj.toString());
					startFileTransferProcedure(obj);
				}
			} else {
				networkException = true;
				System.out.println("Failure");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Transferring files method
	 * NOT USED
	 * @author Prateek
	 */
	public void transfer1(String fileAbsolutePath, String fileName)
	{	

		try{
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			//httpclient.getParams().setParameter("sessid", Session.getSessionId());

			HttpPost httppost = new HttpPost("http://bliss-dev.com/mobiibook/backup_transfer_files");


			List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("sessid",Session.getSessionId()));
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			httppost.setEntity(formEntity);
			HttpResponse response1 = httpclient.execute(httppost);
			System.out.println("the response is " + response1.toString());
			HttpEntity resEntity = response1.getEntity();
			System.out.println(response1.getStatusLine());


//			HttpParams params = new BasicHttpParams();
//			String sessid = Session.getSessionId();
//			System.out.println("The session id is " + sessid);
//			params.setParameter("sessid", sessid);
//			httppost.
//			httppost.setParams(params);
			//httppost.getParams().setParameter("sessid", Session.getSessionId());
			//HttpParams params = new BasicHttpParams();
//			String sessid = Session.getSessionId();
//			System.out.println("The session id is " + sessid);
//			params.setParameter("sessid", sessid);
//			httppost.setParams(params);
			//File file = new File("c:/TRASH/zaba_1.jpg");
			//File file = new File(Environment.getExternalStorageDirectory(), "Khabar Nahi_Dostana.mkv");
//			File file = new File(fileAbsolutePath);
//			MultipartEntity mpEntity = new MultipartEntity();
//			ContentBody cbFile = new FileBody(file);
//			mpEntity.addPart("uploadedfile", cbFile);
//			httppost.setEntity(mpEntity);
//			System.out.println("executing request " + httppost.getRequestLine());
//			HttpResponse response = httpclient.execute(httppost);
//			HttpEntity resEntity = response.getEntity();

//			System.out.println(response.getStatusLine());
//			if (resEntity != null) {
//			System.out.println(EntityUtils.toString(resEntity));
//			}
//			if (resEntity != null) {
//			resEntity.consumeContent();
//			}

			httpclient.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}



//		try{
//		HttpClient httpclient = new DefaultHttpClient();
//		httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
//		//httpclient.getParams().setParameter("sessid", Session.getSessionId());

//		HttpPost httppost = new HttpPost("http://bliss-dev.com/mobiibook/backup_transfer_files");

////		List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
////		postParameters.add(new BasicNameValuePair("sessid",Session.getSessionId()));
////		UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
//		System.out.println("==== point 1.1 ====");
////		HttpParams params = new BasicHttpParams();
//		String sessid = Session.getSessionId();
////		System.out.println("The session id is " + sessid);
////		params.setParameter("sessid", sessid);
//		//httppost.setParams(params);
//		System.out.println("==== point 1.2 ====");
//		//httppost.setEntity(formEntity);
//		//httppost.addHeader("sessid11", Session.getSessionId());
//		//httppost.getParams().setParameter("sessid", Session.getSessionId());
//		//File file = new File("c:/TRASH/zaba_1.jpg");
//		//File file = new File(Environment.getExternalStorageDirectory(), "you.MPG");
//		File file = new File(fileAbsolutePath);
//		MultipartEntity mpEntity = new MultipartEntity();
//		ContentBody cbFile = new FileBody(file);
//		//mpEntity.addPart("sessid", new StringBody(sessid.toString(), "text/plain",Charset.forName( "UTF-8" )));
//		mpEntity.addPart("uploadedfile", cbFile);
//		System.out.println("==== point 2.1 ====");

//		httppost.setEntity(mpEntity);
//		System.out.println("==== point 2.2 ====");
//		System.out.println("executing request " + httppost.getRequestLine());
//		System.out.println("==== point 2.3 ====");
//		HttpResponse response = httpclient.execute(httppost);
//		System.out.println("==== point 2.4 ====");
//		HttpEntity resEntity = response.getEntity();
//		System.out.println("==== point 3.0 ====");
//		System.out.println(response.getStatusLine());
//		System.out.println("==== point 3.1 ====");
//		if (resEntity != null) {
//		System.out.println(EntityUtils.toString(resEntity));

//		//if (resEntity != null) {
//		resEntity.consumeContent();
//		}
//		System.out.println("==== point 3.2 ====");

//		httpclient.getConnectionManager().shutdown();
//		System.out.println("==== point 3.3 ====");
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		}
	}

	/**
	 * File transfer happens over here using HttpClient
	 * @param String fileAbsolutePath
	 * Path of the file
	 * @param String fileName
	 * Name of the file
	 * @author Prateek
	 */
	public void transfer(String fileAbsolutePath, String fileName)
	{
		try{
			HttpClient httpclient = new DefaultHttpClient();
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			//httppost = new HttpPost("http://bliss-dev.com/mobiibook/backup_transfer_files?sessid=" + Session.getSessionId() + "&backup_id=" + backUpId);
			httppost = new HttpPost("http://staging.mobiibook.com/backup_transfer_files?sessid=" + Session.getSessionId() + "&backup_id=" + backUpId);
			File file = new File(fileAbsolutePath);
			MultipartEntity mpEntity = new MultipartEntity();
			ContentBody cbFile = new FileBody(file);
			mpEntity.addPart("uploadedfile", cbFile);
			httppost.setEntity(mpEntity);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity resEntity = response.getEntity();
			System.out.println(response.getStatusLine());
			if (resEntity != null) {
				System.out.println(EntityUtils.toString(resEntity));
			}
			if (resEntity != null) {
				resEntity.consumeContent();
			}
			httpclient.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			System.out.println(e.getMessage() + "--" + e.getLocalizedMessage() + "--" ); 
			if(!httppost.isAborted())
				networkException = true;
			e.printStackTrace();
		}
	}
	
	/**
	 * Transferring files using multipart
	 * NOT USED
	 * @author Prateek
	 */
	public void transfer3(String fileAbsolutePath, String fileName)
	{
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;

		String pathToOurFile = fileAbsolutePath;
		//Hardcoding
		String urlServer = "http://bliss-dev.com/mobiibook/backup_transfer_files";
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		//String boundary =  "*****";
		String boundary = "----------V2ymHFg03ehbqgZCaKO6jy";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;

		try
		{
			FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );

			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs & Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Enable POST method
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
			outputStream = new DataOutputStream( connection.getOutputStream() );
			//outputStream.writeBytes(twoHyphens + boundary + lineEnd);

//			res.append("Content-Disposition: form-data; name=\"").append(key)
//			.append("\"\r\n").append("\r\n").append(value).append(
//			"\r\n").append("--").append(boundary)
//			.append("\r\n");
//			outputStream.writeBytes("Content-Disposition: form-data; name=\"sessid=\"" + Session.getSessionId() + "\"" + lineEnd);
//			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			//outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathToOurFile +"\"" + lineEnd);
			//outputStream.writeBytes(lineEnd);


			StringBuffer res = new StringBuffer("--").append(boundary).append("\r\n");
			String key = "sessid";
			String value = Session.getSessionId();

			res.append("Content-Disposition: form-data; name=\"").append(key)
			.append("\"\r\n").append("\r\n").append(value).append(
			"\r\n").append("--").append(boundary)
			.append("\r\n");
//			res.append("Content-Disposition: form-data; name=\"").append("file_info")
//			.append("\"; filename=\"").append(fileName).append("\"\r\n")
//			.append("Content-Type: ").append("image/jpeg").append("\r\n\r\n");

			res.append("Content-Disposition: form-data; name=\"").append("file_info")
			.append("\"; filename=\"").append(fileName).append("\"\r\n\r\n");
			//.append("Content-Type: ").append("image/jpeg").append("\r\n\r\n");



			outputStream.writeBytes(res.toString());
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0)
			{
				outputStream.write(buffer, 0, bufferSize);
				outputStream.flush();
				buffer = null;
				System.gc();
				buffer = new byte[bufferSize];
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = connection.getResponseCode();
			String serverResponseMessage = connection.getResponseMessage();

			System.out.println("Response code :" + serverResponseCode);
			System.out.println("Response Message : " + serverResponseMessage);

			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	/**
	 * Returns the value of the progress bar
	 * @return int
	 * Value of the percentage completion of porgress bar
	 * @author Prateek
	 */
	public int getProgess()
	{
		try{
			return (int) ((bytesTransferred * 100)/bytesToTransfer);
		}catch(Exception e)
		{
			return 0;
		}
	}

}
