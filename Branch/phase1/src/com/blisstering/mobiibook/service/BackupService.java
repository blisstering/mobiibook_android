package com.blisstering.mobiibook.service;

import java.text.DateFormat;
import java.util.Date;
import com.blisstering.mobiibook.backup.BackupContentsThread;
import com.blisstering.mobiibook.utils.Session;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/*
 * Service which runs in the background for the Backup
 */
public class BackupService extends Service{

	BackupContentsThread backupContentThread;
	String threadResponse = "";
	int progressValue;
	boolean isBackupProcedureOver = false;
	boolean networkException = false;
	String date,time,contents,contentDetails;

	public static final String BROADCAST_ACTION="com.blisstering.mobiibook.service.BackupServiceActivity";
	private Intent broadcast=new Intent(BROADCAST_ACTION);
	/**
	 * Binds the service
	 * @author Prateek
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		//arg0.getExtras();
		return binder;
	}

	/**
	 * Called when the service is created
	 * @author Prateek
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		date = DateFormat.getDateInstance().format(new Date()).toString();
		time = DateFormat.getTimeInstance().format(new Date()).toString();
		contents = "All";
		contentDetails = "Calendars,Contacts,Photos,Music,Videos";
		//Toast.makeText(this,"Service created", Toast.LENGTH_LONG).show();
	}

	/**
	 * Called when service is destroyed
	 * @author Prateek
	 */
	@Override
	public void onDestroy() {
		Session.setBackupService(false);
		if(backupContentThread!= null)
			backupContentThread.backupCancel();
		System.out.println("In destroy");
		super.onDestroy();
		//Toast.makeText(this,"Service destroyed", Toast.LENGTH_LONG).show();
	}

	/**
	 * Called when service is rebinded
	 * @author Prateek
	 */

	@Override
	public void onRebind(Intent intent) {

	}


	/**
	 * Called when service is unbinded
	 * @author Prateek
	 */
	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(this.getClass().getName(), "UNBIND");
		return true;
	}
//	public void onStart(int startId, Bundle arguments) {
//	super.onStart( startId, arguments );
//	Log.d( LOG_TAG, "onStart" );
//	serviceHandler = new Handler();
//	serviceHandler.postDelayed( new RunTask(),1000L );
//	}

	/**
	 * Backup start process starts from here. Called when the service starts
	 * @author Prateek
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		super.onCreate();
		Session.setBackupService(true);
		String bName = intent.getStringExtra("backupName");
		startBackup(bName);
		setProgress();
		return START_STICKY;
	}
	
	/**
	 * Thread for a backup process is started from here
	 * @param String bName
	 * Name of the backup
	 * @author Prateek
	 */
	public void startBackup(final String bName)
	{
		new Thread() {
			public void run() {
				backupContentThread = new BackupContentsThread(bName,"Manual");
				threadResponse = backupContentThread.startBackup();
				Log.d("THREADRESPONSE", threadResponse);
				networkException  = backupContentThread.isNetworkException();
				isBackupProcedureOver = true;
			}
		}.start();
	}
	
	/**
	 * Called when the service is started
	 * @author Prateek
	 */
	@Override
	public void onStart(Intent intent, int startId) {
		super.onCreate();
		Session.setBackupService(true);
		setProgress();
//		System.out.println("The service is started");
//		System.out.println("The argsss " + intent.getExtras());
		//super.onStart( startId, arguments );
		//Toast.makeText(this,"Service started", Toast.LENGTH_LONG).show();
	}

	/*
	 * Binder responsible for connecting to any activity and contains the function described in aidl files by which activity 
	 * communicates with the service
	 */
	
	private final com.blisstering.mobiibook.aidl.BackupService.Stub binder = 
		new com.blisstering.mobiibook.aidl.BackupService.Stub() {
//		public void startBackup(String bName)
//		{
////		backupName = bName;
////		backupContentThread = new BackupContentsThread(backupName,"Manual");
////		threadResponse = backupContentThread.startBackup();
////		isBackupProcedureOver = true;
//		}
		
		/**
		 * Cancels the backup
		 * @author Prateek
		 */
		public boolean cancelBackup()
		{
			if(backupContentThread != null)
			{
				backupContentThread.backupCancel();
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/**
		 * To initiate the resume process of the backup
		 * @author Prateek
		 */
		public void resumeBackup()
		{
			new Thread() {
				public void run() {
					isBackupProcedureOver = false;
					networkException = false;
					setProgress();
					threadResponse = backupContentThread.resumeBackup();
					networkException  = backupContentThread.isNetworkException();
					System.out.println("The backup needs to be resumed");
					isBackupProcedureOver = true;
				}
			}.start();
		}
		
		/**
		 * If backup process is running starts the send progress thread
		 * @author Prateek
		 */
		
		public void sendUpdates() throws RemoteException {
			// TODO Auto-generated method stub
			if(isBackupProcedureOver)
				setProgress();
		}
	};

	/**
	 * Returns the binder
	 * @author Prateek
	 */
	public IBinder getBinder() {
		return binder;
	}

	/**
	 * Starts the thread which broadcasts the information, received by an activity and shown on the screen
	 * @author Prateek
	 */
	public void setProgress()
	{
		new Thread() {
			public void run() {
				// TODO Auto-generated method stub
				progressValue = 0;
				while(true){
					try{
						if(backupContentThread != null)
						{
							progressValue = backupContentThread.getProgess();
							broadcast.putExtra("progress", progressValue);
							broadcast.putExtra("response", threadResponse);
							broadcast.putExtra("backupover", isBackupProcedureOver);
							broadcast.putExtra("isNetworkException", networkException);
							broadcast.putExtra("date", date);
							broadcast.putExtra("time", time);
							broadcast.putExtra("contents", contents);
							broadcast.putExtra("contentDetails", contentDetails);

							sendBroadcast(broadcast);
							if(isBackupProcedureOver || networkException)
								break;
							Thread.sleep(2000);
						}
					}
					catch(Throwable t){
					}
				}
			}
		}.start();
	}
}